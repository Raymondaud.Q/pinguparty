# Pingu Party

Implementation of the Pingu Party card game, originally by Reiner Knizia.

This software was developped as an assignment during the 2018/10 university year at the CERI, University of Avignon (France), by the following students:

* Iguichi Imad
* Robine Thomas
* Laperrière Gaelle
* Raymondaud Quentin

It can be used by a human to play against artificial agents, or against distant players ( artificial agents or human players ) through a network. 


## Organization
The source code is organized as follows... 

```

pinguparty
|
│ pinguGUI.sh 
| compiler.sh
| README.md
│
└─── class
|   | 
│   │   *.class
│   
└─── src
    |
    │   COLORS.java
    │   PassRef.java
    |
    └─── stats
    |   │ 
    |   │   LocalReadWriter.java
    |   │   Profile.java
    |   │   Stat.java
    |   |   Ranking.java
    |   |   PlayerRank.java
    |   |   Replay.java
    |   |   TempStat.java
    |
    └─── network
    |   │   
    |   │   Server.java
    |   │   Client.java
    |   |
    |   └─── dataobjects
    |       |   
    |       |   Color.java
    |       |   DataObjectParser.java
    |       |   EndGame.java
    |       |   Move.java
    |       |   NextTurn.java
    |       |   PlayerInformation.java
    |       |   ProfileData.java
    |       |   ProfileStatData.java
    |       |   Result.java
    |       |   RoundStart.java
    |       |   StatData.java 
    | 
    └─── engine
    |   │
    |   └─── agent
    |   |   |
    |   |   |   Agent.java
    |   |   |   AgentFactory.java
    |   |   |   Player.java
    |   |   |   Robot.java
    |   |   |   DumbRobot.java
    |   |   |   CarlosRobot
    |   |   |   HBasedRobot
    |   |   | 
    |   |   └─── brain
    |   |       |
    |   |       |   BackToTheRandomFutur
    |   |       |   ZombieBlock
    |   |       |
    |   |       └─── logic
    |   |           |
    |   |           |   PinguLogic
    |   |           |   Simulator 
    |   |   
    |   └─── data
    |   |   |
    |   |   |   prof.txt
    |   |   |   stats.txt
    |   |   |   replay.txt
    |   |   |   replayFiles.txt
    |   |   |
    |   |   └─── Pictures
    |   |   |
    |   |   └─── replays
    |   |   |
    |   |   └─── temp_stat
    |   | 
    |   └─── game
    |       | 
    |       |   Chrono.java
    |       |   Game.java
    |       |   Timer.java
    |       |   Color.java
    |       |   Move.java
    |       |   RankedMove.java
    |       |   BlockMove.java
    |
    └─── gui
        |
        |   ActionAIEdition.java
        |   ActionImage.java
        |   ActionJoinGame.java
        |   ActionLocalGame.java
        |   ActionLocalGame.java
        |   ActionMoreStatistics.java
        |   ActionNetworkGame.java
        |   ActionNewAI.java
        |   ActionNewPlayer.java
        |   ActionOption.java
        |   ActionPlayerEdition.java
        |   ActionProfiles.java
        |   ActionProfileSelection.java
        |   ActionStatistics.java
        |   CustomBorder.java
        |   DefTabCellRend.java
        |   DefTabMod.java
        |   LocalGame.java
        |   NetworkGame.java
        |   NetworkGameCLI.java
        |   View.java
        |   MenuView.java
        |   GoToAction.java
        |   ActionReplaying.java
        |   ActionReplay.java
        |   DrawGraph.java
        |   ActionAGraphicOption.java
        |   ActionBGrapicOption.java
        |   ActionGraphicOption.java
        |  
        └─── Background
        |   |
        |   |   B1.jpg
        |
        └─── Cards
            |
            |   *.jpg
           
     
 
```

## Installation
<p>
	Here is the procedure to install this software :

	1. Download this repository.
	2. Open downloaded directory and allow "/pinguparty/compiler.sh" and "/pinguparty/pinguGUI.sh" to be executed
	3. Execute "/pinguparty/pinguGUI.sh -c" to compile  and run source code for the first time.
</p>

You can also import and run it through Eclipse IDE




## Use
<p>
	<b> In order to play the game, you must : </b>

	0. Execute "pinguGUI.sh" ( If you haven't compile source code yet, please refer to ## Installation 3.).
	1. Create some profiles ( Possible AI classname are : Robot DumbRobot CarlosRobot HBasedRobot)
	2. Network Creating : Have minimum one agent, and enable DMZ feature on your network for your computer, port 11111 must be available.
	3. Network Join : Know the host Ip Address
	4. Local Game : Have some AI profile. 
</p>

The project wiki (put a hyperlink) contains detailed instructions regarding how to use the game.
CarlosRobot its the only agent that its level sensitive ( that you can change the difficulty when you create it). 

## Dependencies

<b> You must have installed a JDK >= 8 , TOPICS SOF :</b>

    StackOverFlow : JDK 7 to 8, install and update commands https://stackoverflow.com/questions/30177455/moving-from-jdk-1-7-to-jdk-1-8-on-ubuntu

<b> Debian (Ubuntu/Linux/...) apt command : </b>

    sudo add-apt-repository ppa:webupd8team/java
    sudo apt-get update
    sudo apt-get install oracle-java8-installer

<p>
	<b> The project relies on the following librarie : </b>
	
	* figlet (optional for pinguGUI.sh) : "PinguParty" Console display
	* JDK >=  8 
	* Others JDK pre-built libraries :
	
	    - Java Swing
		- ...
</p>

## References
During development, we use the following bibliographic resources:
* Forum WebSite <b>StackOverFlow</b> : Debugging, explaination, ... :+1:
* Lessons WebSite  <b>OpenClassroom</b> : Swing, Thread, ...
* Lessons WebSite  <b>JavaPractice</b> :  Swing, Reflexivity, UUID, ...
* Example/Tutorial WebSite  <b>TutorialPoint</b> : Same...



