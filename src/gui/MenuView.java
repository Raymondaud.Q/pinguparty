package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

import gui.View;
import gui.GoToAction;

/**
 * @author Quentin Raymondaud
 */

class MenuView extends JPanel
{
    // Class MenuView
    public static final String NAME = "PinguParty Home";
    public static Image background;

    @Override
	public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawImage(background, 0, 0, this);
        try
        {
            Thread.sleep(50);
        }
        catch (Exception e)
        {

        }

        repaint();
        revalidate();
    }

    /**
     * @author Quentin Raymondaud
     * MenuView Constructor
     * @param myMain Reference to the main panel
     */
    public MenuView(View myMain)
    {
        super(new FlowLayout(FlowLayout.CENTER, 3000, 30));
        setName(NAME);


        JButton local = new JButton(new GoToAction("Local Game", ActionLocalGame.NAME, myMain));
        JButton create = new JButton(new GoToAction("Create Network Game", ActionNetworkGame.NAME, myMain));
        JButton join = new JButton(new GoToAction("Join Network Game", ActionJoinGame.NAME, myMain));
        JButton profile = new JButton(new GoToAction("Profiles", ActionProfiles.NAME, myMain));
        JButton stat = new JButton(new GoToAction("Statistics", ActionStatistics.NAME, myMain));
        JButton opt = new JButton(new GoToAction("Options", ActionOptions.NAME, myMain));
        JButton replay = new JButton(new GoToAction("Replay", ActionReplay.NAME, myMain));
        JButton ex = new JButton("Exit");
        ex.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                int clickedButton = JOptionPane.showConfirmDialog(null, "Exit the Game ?", "Confirmation", JOptionPane.YES_NO_OPTION);
                if (clickedButton == JOptionPane.YES_OPTION)
                {
                    System.exit(0);
                }
            }
        });
        Font fieldFont = new Font("Arial", Font.PLAIN, 20);

        local.setPreferredSize(new Dimension(330,50));
        create.setPreferredSize(new Dimension(330,50));
        join.setPreferredSize(new Dimension(330,50));
        profile.setPreferredSize(new Dimension(330,50));
        opt.setPreferredSize(new Dimension(330,50));
        stat.setPreferredSize(new Dimension(330,50));
        replay.setPreferredSize(new Dimension(330,50));
        ex.setPreferredSize(new Dimension(330,50));

        local.setBorder(new OvalBorder());
        create.setBorder(new OvalBorder());
        join.setBorder(new OvalBorder());
        opt.setBorder(new OvalBorder());
        profile.setBorder(new OvalBorder());
        stat.setBorder(new OvalBorder());
        replay.setBorder(new OvalBorder());
        ex.setBorder(new OvalBorder());

        local.setFont(fieldFont);
        create.setFont(fieldFont);
        join.setFont(fieldFont);
        stat.setFont(fieldFont);
        opt.setFont(fieldFont);
        profile.setFont(fieldFont);
        replay.setFont(fieldFont);
        ex.setFont(fieldFont);

        setBorder(BorderFactory.createTitledBorder(null, NAME, TitledBorder.CENTER, TitledBorder.TOP, new Font("times new roman",Font.PLAIN,15), Color.white));
        try
       	{
       		background = ImageIO.read(new File("../src/gui/Background/B4.jpg"));
       	}
       	catch ( Exception e)
       	{;}
        add(local);
        add(create);
        add(join);
        add(stat);
        add(profile);
        add(stat);
        add(opt);
		add(replay);
        add(ex);
    }
}

