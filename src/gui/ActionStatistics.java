package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

import gui.View;
import stats.Profile;

/**
 * @author Gaelle Laperriere
 */
public class ActionStatistics extends JPanel
{
    //class Statistics

    /**
     * Panel's name
     */
    public static final String NAME = "Statistics";
    /**
     * Profiles's list
     */
    private Profile[] p=Profile.loadProfileFromFile();

    /**
     * @author Gaelle Laperriere
     * Statistics's Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionStatistics (View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));

        //columns for the statistics's table
        String [] col = {
            "Name",
            "ELO",
            "Points",
            "W/L/D",
	    "Played",
            "Time"
        };

        //data for the statistics's table
        Object [][] stats = new Object[Profile.getNbLine()][6];

        for(int i=0;i<Profile.getNbLine();i++)
        {
      	    Profile profile=p[i];
      	    stats[i][0]=profile.getName();
      	    String[] statsBis=profile.statsProfile();
      	    stats[i][1]=Double.parseDouble(statsBis[6]);
      	    stats[i][2]=Integer.parseInt(statsBis[5]);
      	    stats[i][3]=statsBis[1]+"/"+statsBis[2]+"/"+statsBis[3];
      	    stats[i][4]=Integer.parseInt(statsBis[0]);
      	    int t=Integer.parseInt(statsBis[7]);
      	    stats[i][5]=Integer.toString(t/(60*60))+"h"+Integer.toString(t/60)+"m"+Integer.toString(t%60)+"s";
        }

        DefTabMod mod= new DefTabMod(stats,col);
        JTable table=new JTable(mod);

        TableColumn column=null;
        for(int i=0; i<6;i++)
        {
            column=table.getColumnModel().getColumn(i);
            column.setPreferredWidth(110);
        }

        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setRowSelectionAllowed(true);
        table.setAutoCreateRowSorter(true);

        ////////////////////////////////

        //button to go back to Main Menu
        JButton rButton = new JButton(new GoToAction("Main Menu", MenuView.NAME, myMain));
	//button to view a graphic of statistics by selectig options
        JButton oButton = new JButton("Line Chart");
	oButton.addActionListener(new ActionListener()
        {
            @Override
	    public void actionPerformed(ActionEvent event)
            {
                myMain.showCardByHand(ActionGraphicOptions.NAME,0);
            }
        });

        ////////////////////////////////

        //button to see more about a user
        JButton plus = new JButton("Show More");
        plus.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                int r=table.convertRowIndexToModel(table.getSelectedRow());
                if(r!=-1)
                {
                    System.out.println("More about "+stats[r][0]);
                    myMain.showCardByHand(ActionMoreStatistics.NAME,r);
                }
            }
        });

        ////////////////////////////////

        JPanel pan = new JPanel();
        pan.setLayout(new BoxLayout(pan, BoxLayout.PAGE_AXIS));
	    if(Profile.getNbLine()>=1)
	    {
        	pan.add(plus);
		    pan.add(oButton);
	    }
        pan.add(rButton);
        add(new JScrollPane(table),BorderLayout.CENTER);

	add(pan);
    }
}
