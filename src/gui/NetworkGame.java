package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;

import engine.agent.Player;
import network.Server;
import stats.Profile;
import stats.Ranking;

/**
 * @author Raymondaud Quentin
 */

public class NetworkGame extends JPanel
{
	//class NetworkGame

	/**
	 * Game's server
	 */
	private Server  pinguNetwork ;

	/**
	 *  Panel's name
	 */
	public static final String name = "Pingu";

	/**
	 * current Player
	 */
	public  Player user ;

	/**
	 * Board's panel
	 */
	private JPanel board = new JPanel();

	/**
	 * Card's Panel
	 */
	private JPanel cards = new JPanel();

	/**
	 * Other players's Panel
	 */
	private JPanel players = new JPanel();

	/**
	 * Selected card
	 */
	public static String roundAc ="FINISHED";

	/**
	 * Selected place
	 */
	public static String roundPlace  = "FINISHED";

	/**
	 * Refresh inner panel or not
	 */
	public boolean flag = true;

	/**
	 * Main View reference
	 */
	protected View myMain ;

	/**
	 * Engine's Thread
	 */
	private SwingWorker engine;
	
	public static final ImageIcon yellowCard=new ImageIcon("../src/gui/Cards/yellow.jpg");
	public static final ImageIcon purpleCard=new ImageIcon("../src/gui/Cards/purple.jpg");
	public static final ImageIcon blueCard=new ImageIcon("../src/gui/Cards/blue.jpg");
	public static final ImageIcon greenCard=new ImageIcon("../src/gui/Cards/green.jpg");
	public static final ImageIcon redCard=new ImageIcon("../src/gui/Cards/red.jpg");
	public static final ImageIcon unknownCard=new ImageIcon("../src/gui/Cards/unknown.jpg");
	public static final ImageIcon usedCard=new ImageIcon("../src/gui/Cards/empty.jpg");
	public static final ImageIcon emptyCard=new ImageIcon("../src/gui/Cards/index.png");


	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		setPreferredSize(getPreferredSize());
		int delay = 5 ;//milliseconds
		ActionListener taskPerformer = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				if (flag )
				{
					playersInfo();
					userBoard();
					userCards();
					flag=false;
				}
			}
		};
		new javax.swing.Timer(delay, taskPerformer).start();
		repaint();
		revalidate();
	}

	/**
	 * Close the server and cancel the engine's thread
	 */
	public void close()
	{
		if ( engine != null )
			engine.cancel(true);
		if ( pinguNetwork != null)
			pinguNetwork.close();
	}

	/**
	 * The engine's Thread
	 */
	public void run()
	{
		NetworkGame t = this;
		engine = new SwingWorker<Void, Void>()
		{

			@Override
			protected Void doInBackground() throws Exception
			{
				while( pinguNetwork.getGame().getRanking() == null &&  ! isCancelled() )
				{
					flag=true;
					if ( pinguNetwork.play(t) != true)
                        break;
				}
				return null ;
			}
			@Override
			protected void done()
			{
				removeAll();
				add(new JButton(new GoToAction("Quit", MenuView.NAME, myMain)));
				if (pinguNetwork.getGame().getRanking() != null)
					rankTable();
			}
		};
		engine.execute();
		System.out.println(name + " exiting.");
	}

	/**
	 * Display the current cards's panel
	 */
	public void userCards()
	{
		String cardsS = user.getCards();
		ImageIcon [] cardI =new ImageIcon[cardsS.length()];
		JButton[] cardButtons = new JButton[cardsS.length()];
		remove(cards);
		cards = new JPanel();
		cards.setLayout(new BoxLayout( cards,BoxLayout.X_AXIS));
		cards.setBorder(BorderFactory.createEmptyBorder(20, 0, 20, 0));
		JPanel tmp = new JPanel();
		JLabel pName = new JLabel("You : "+user.getPseudo());
		if ( pinguNetwork.getGame().getPlayer(pinguNetwork.getGame().getPlayerIndex(),pinguNetwork.getGame().getNbPlayer()) == user )
			pName.setForeground(Color.orange);
		tmp.add(pName);

		for(int j=0;j<cardsS.length();j++)
		{

			if(cardsS.charAt(j) != ' ')
			{

				if(cardsS.charAt(j) == 'R')
				{
					cardI[j]=redCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="R";
							System.out.println(roundAc);
						}
					} );
				}
				else if(cardsS.charAt(j) == 'B')
				{
					cardI[j]=blueCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="B";
							System.out.println(roundAc);
						}
					} );
				}
				else if(cardsS.charAt(j) == 'Y')
				{
					cardI[j]=yellowCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="Y";
							System.out.println(roundAc);
						}
					} );
				}
				else if(cardsS.charAt(j) == 'P')
				{
					cardI[j]=purpleCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="P";
							System.out.println(roundAc);
						}
					} );

				}
				else if(cardsS.charAt(j) == 'G')
				{
					cardI[j]=greenCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="G";
							System.out.println(roundAc);
						}
					} );

				}

				cardButtons[j].setPreferredSize(new Dimension(30,35));
				tmp.add(cardButtons[j]);
			}


		}
		if ( user.getGR())
		  tmp.add(new JLabel(" Score : "+(user.getScore()+user.getNbC())));
		else
		  tmp.add(new JLabel(" Score : "+(user.getScore())));
		cards.add(tmp);
		add(cards);
	}

	/**
	 * Display the game's board
	 */
	public void userBoard()
	{

		String [][] boardS = pinguNetwork.getGame().getBoard();
		ImageIcon [][] boardI=new ImageIcon[8][8];
		JButton[][] boardButtons=new JButton[8][8];
		remove(board);
		board = new JPanel();
		board.setLayout(new BoxLayout( board,BoxLayout.Y_AXIS));
		int cmp = 0;
		for(int i=0; i<8;i++)
		{
			JPanel tmp = new JPanel();
			for(int j=0;j<8;j++)
			{
				if(boardS[i][j].equals("X") == false )
				{
					if(boardS[i][j].equals("E"))
					{
						final int num = cmp;
						boardI[i][j]=emptyCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);

						if(pinguNetwork.getGame().canPutCard(num,roundAc))
						{
							if(roundAc.equals("R"))
							{
								boardButtons[i][j].setBackground(Color.red);
							}
							else if(roundAc.equals("Y"))
							{
								boardButtons[i][j].setBackground(Color.yellow);
							}
							else if(roundAc.equals("P"))
							{
								boardButtons[i][j].setBackground(Color.pink);
							}
							else  if(roundAc.equals("G"))
							{
								boardButtons[i][j].setBackground(Color.green);
							}
							else if(roundAc.equals("B"))
							{
								boardButtons[i][j].setBackground(Color.cyan);
							}
							else
								boardButtons[i][j].setBackground(Color.orange);
						}

						boardButtons[i][j].addActionListener(new ActionListener()
						{
							@Override
							public void actionPerformed(ActionEvent e)
							{
								roundPlace=""+num;
								System.out.println(roundPlace);
							}
						} );
					}
					else if(boardS[i][j].equals("R"))
					{
						boardI[i][j]=redCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(boardS[i][j].equals("B"))
					{
						boardI[i][j]=blueCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(boardS[i][j].equals("Y"))
					{
						boardI[i][j]=yellowCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(boardS[i][j].equals("P"))
					{
						boardI[i][j]=purpleCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(boardS[i][j].equals("G"))
					{
						boardI[i][j]=greenCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}

					boardButtons[i][j].setPreferredSize(new Dimension(36,46));
					tmp.add(boardButtons[i][j]);
					cmp+=1;
				}
			}
			board.add(tmp);
		}
		add(board);
	}

	/**
	 * Diplay other players's informations
	 */
	public void playersInfo()
	{
		int nbPlayer =  pinguNetwork.getGame().getNbPlayer();;
		JButton[][] playerCards = new JButton[nbPlayer][];
		remove(players);
		players = new JPanel();
		players.setBorder(BorderFactory.createTitledBorder("Game Info"));
		players.setLayout(new BoxLayout( players,BoxLayout.Y_AXIS));
		for ( int i = 0 ; i < nbPlayer+1 ; i++)
		{
			JPanel tmp = new JPanel(new  FlowLayout(0,0,0));
			if ( i == nbPlayer )
			{
				int currentRound = pinguNetwork.getGame().getNbRound();

				JLabel roundCount = new JLabel(" Round N° " + currentRound  + " of " + nbPlayer );
				if (currentRound == nbPlayer)
					roundCount.setForeground(Color.red);
				tmp.add(roundCount);
				players.add(tmp);
			}
			else if ( i != nbPlayer && pinguNetwork.getGame().getPlayer(i,nbPlayer) != user )
			{

				String cardsS = pinguNetwork.getGame().getPlayer(i,nbPlayer).getCards();
				playerCards[i] = new JButton[cardsS.length()];
				JLabel pName = new JLabel(" "+pinguNetwork.getGame().getPlayer(i,nbPlayer).getPseudo()+" ");
				if ( pinguNetwork.getGame().getPlayerIndex() == i )
					pName.setForeground(Color.orange);
				tmp.add(pName);
				for(int j=0;j<cardsS.length();j++)
				{
					if(cardsS.charAt(j) != ' ')
					{
						playerCards[i][j]=new JButton(unknownCard);
						playerCards[i][j].setMargin(new Insets(0,0,0,0));
						playerCards[i][j].setPreferredSize(new Dimension(8,8));
						tmp.add(playerCards[i][j]);
					}
				}

				if ( pinguNetwork.getGame().getPlayer(i,nbPlayer).getGR() )
				  tmp.add(new JLabel(" Score : "+(pinguNetwork.getGame().getPlayer(i,nbPlayer).getScore()+pinguNetwork.getGame().getPlayer(i,nbPlayer).getNbC())));
				else
				  tmp.add(new JLabel(" Score : "+(pinguNetwork.getGame().getPlayer(i,nbPlayer).getScore())));
				players.add(tmp);
			}
		}
		add(players);
	}

	/**
	 * Display the ranktable of the game's end
	 */
	public void rankTable()
	{
		JPanel rank = new JPanel();
		Ranking endRank = pinguNetwork.getGame().getRanking();
		if ( endRank != null )
		{
			Object[][] tab = new Object [pinguNetwork.getGame().getNbPlayer()][6];
			String[] label = {"Pseudo", "Player Class", "Score", "Round Score", "Green Cards Played" , "Elo"};
			for ( int i = 0 ; i < pinguNetwork.getGame().getNbPlayer() ; i++ )
			{
				java.util.List<String> rankI = endRank.getPlayerRankI(i).rankToStrAL();
				for ( int j = 0 ; j < 6 ; j++ )
				{
					tab[i][j]= rankI.get(j);
				}
			}
			JTable table = new JTable(tab, label);
			JScrollPane scrollPane= new  JScrollPane(table);

			JLabel name ;
			if ( endRank.nbWinner() == 1 )
				name =  new JLabel (" Le gagnant est : " + endRank.getWinners());
			else
				name =  new JLabel (" Les gagnants sont : " + endRank.getWinners());

			//name.setForeground(Color.orange);
			rank.add(name);
			rank.add(scrollPane);
			add(rank);
		}
	}

	/**
	 * Create a new NetworkGame's Panel
	 * @param myMain Main panel
	 * @param pn     Game Server
	 * @param user   Current selected profile
	 */
	public NetworkGame (View myMain,Server pn, Profile user)
	{
		this.myMain = myMain;
		roundAc = "FINISHED";
		roundPlace = "FINISHED";
		setBorder(BorderFactory.createTitledBorder(name));

		JPanel buttonPan = new JPanel();
		buttonPan.setLayout(new BoxLayout( buttonPan,BoxLayout.X_AXIS));
		JButton quit = new JButton(" Quit ");
		quit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				int clickedButton = JOptionPane.showConfirmDialog(null, " You really want to leave this PinguParty Game ? ", "Confirmation", JOptionPane.YES_NO_OPTION);
				if ( clickedButton == JOptionPane.YES_OPTION) 
				{
					removeAll();
					close();
					myMain.showCard(MenuView.NAME);
				}
			}
		} );
		buttonPan.add(quit);
		JButton endRound = new JButton("Cannot Put Card");
		endRound.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				roundPlace="D";
				roundAc="D";
			}
		} );
		buttonPan.add(endRound);
		add(buttonPan);
		setLayout(new BoxLayout( this,BoxLayout.Y_AXIS));
		pinguNetwork = pn;
		this.user = pinguNetwork.getGame().getPlayerByProfile(user);
		run();
	}
}
