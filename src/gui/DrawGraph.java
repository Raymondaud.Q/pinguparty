package gui;

import java.util.ArrayList;
import java.util.Random;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Graphics;
import java.awt.Stroke;
import java.awt.BasicStroke;
import java.awt.RenderingHints;
import java.awt.Dimension;
import java.awt.FontMetrics;


/**
* @author Gaelle Laperriere
*/
public class DrawGraph extends JPanel
{
    protected ArrayList<Double> [] data;
    protected int nbLines;//nbProf or nb of stats selected
    protected int dS;//number of values (required)
    protected String title;

    protected int pWidth = 7;//point

    protected double max;//larger data
    protected double min;//smaler data

    protected double xS;//scale
    protected double yS;//scale

    protected List<Point> [] points;
    protected Stroke oldStroke;
    protected FontMetrics m;

    protected Color green=new Color(198, 255, 68);
    protected Color blue=new Color(68, 177, 255);
    protected Color red=new Color(255, 68, 68);
    protected Color purple=new Color(195, 68, 255);
    protected Color brown=new Color(209, 148, 102);
    protected Color orange=new Color(255, 177, 68);
    protected Color grey=new Color(142, 142, 142);

    protected boolean displayPoints=true;
    protected boolean displayMarks=true;

    protected int pad=1;


    //Constructor
    public DrawGraph(ArrayList<Double> [] data, int nbLines, int dS,String title, int pad)
    {
        this.data=data;
       	this.max=getMaxData();
      	//System.out.println(this.max);
      	this.min=getMinData();
      	//System.out.println(this.min);
      	this.nbLines=nbLines;
      	this.dS=dS;
      	this.title=title;
      	if(dS>=100)
      	{
      		this.displayPoints=false;
      		this.displayMarks=false;
      	}
      	this.pad=pad;
    }

    private double getMaxData()
    {
        double max=Double.MIN_VALUE;
      	for(ArrayList a : data)
      	{
          if(a!=null)
          {
        		for(Object o : a)
        		{
        			Double d=new Double(o.toString());
        		    	max=Math.max(max,d);
        		}
          }
      	}
        return max;
    }

    private double getMinData()
    {
        double min=Double.MAX_VALUE;
        for(ArrayList a : data)
      	{
          if(a!=null)
          {
            for(Object o : a)
        		{
        			Double d=new Double(o.toString());
        		    	min=Math.min(min,d);
        		}
          }
      	}
        return min;
    }

    //set the size of the JPanel
    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(800, 500);
    }

    //Paint with a padding of 35
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D canvas=(Graphics2D) g;

    	  //To have smooth lines (without pixels)
        canvas.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

      	//Basic setting
      	int h=getHeight();
      	int w=getWidth();

        yS=(h-105)/(max-min);//vertical scale : canvas size with padd for range
        xS=(w-105)/(dS-1);//horizontal scale : canvas size with padd for nb of data-1

      	//All the points
      	points=new ArrayList[nbLines];
      	for(int j=0;j<nbLines;j++)
      	{
      		//Point of one line
      		points[j]=new ArrayList<>();
      		for(int i=0;i<data[j].size();i++)
      		{
      			  double d=data[j].get(i);
    		    	int x=(int) (xS*i+70);//position x (pad+x)
    		    	int y=(int) (yS*(max-d)+35);//position y (pad+dist from top)
    		    	points[j].add(new Point(x, y));
      		}
      	}

	      ////////////////////////////////

        //Background
        canvas.setColor(Color.WHITE);
        canvas.fillRect(70,35,w-105,h-105);//pad left,pad top,canvas size

	      ////////////////////////////////

        //Marks, Labels and grid
      	m=canvas.getFontMetrics();
      	int mHeight=m.getHeight();
      	int lx=0;
      	int ly=0;
      	int lWidth=0;
      	String l="";

        //vertical lines 1 5 10
      	if(pad==1)
      	{
      		for(int i=0;i<dS;i++)
      		{
              int x=i*(w-105)/(dS-1)+70;//i*canvas size/number of data+padtop
              int y0=h-70;
              int y1=y0-7;//markSize=7

              //Grid
              canvas.setColor(new Color(237, 237, 237));
              canvas.drawLine(x,35,x,y0);//from top to bottom

              //Labels
              canvas.setColor(Color.BLACK);
              l=Integer.toString(i);

              lWidth=m.stringWidth(l);
              lx=x-lWidth/2+2;//position x of left label => horizontally centered
              ly=y0+mHeight+5;//position y of bottom label => 50 of margin

              canvas.drawString(l,lx,ly);

              //Marks
              if(displayMarks){canvas.drawLine(x,y1,x,y0);}//from top to bottom
      		}
      	}
      	else if(pad==5)
      	{
      		for(int i=0;i<dS;i=i+5)
      		{
      		  int x=i*(w-105)/(dS-1)+70;//i*canvas size/number of data+padtop
      			int y0=h-70;
      			int y1=y0-7;//markSize=7

      			//Grid
  		    	canvas.setColor(new Color(237, 237, 237));
  		    	canvas.drawLine(x,35,x,y0);//from top to bottom

			      //Labels
  		    	canvas.setColor(Color.BLACK);
  		    	l=Integer.toString(i);

            lWidth=m.stringWidth(l);
            lx=x-lWidth/2+2;//position x of left label => horizontally centered
            ly=y0+mHeight+5;//position y of bottom label => 50 of margin

            canvas.drawString(l,lx,ly);

            //Marks
            if(displayMarks){canvas.drawLine(x,y1,x,y0);}//from top to bottom
      		}
	      }
      	else
      	{
      		for(int i=0;i<dS;i=i+10)
      		{
      		    int x=i*(w-105)/(dS-1)+70;//i*canvas size/number of data+padtop
        			int y0=h-70;
        			int y1=y0-7;//markSize=7

			        //Grid
    		    	canvas.setColor(new Color(237, 237, 237));
    		    	canvas.drawLine(x,35,x,y0);//from top to bottom

			        //Labels
    		    	canvas.setColor(Color.BLACK);
    		    	l=Integer.toString(i);

              lWidth=m.stringWidth(l);
              lx=x-lWidth/2+2;//position x of left label => horizontally centered
              ly=y0+mHeight+5;//position y of bottom label => 50 of margin

              canvas.drawString(l,lx,ly);

              //Marks
              if(displayMarks){canvas.drawLine(x,y1,x,y0);}//from top to bottom
      		}
      	}
      	//label x
      	canvas.setColor(Color.BLACK);
      	l="Abs";
      	ly=ly-20;
      	lx=lx+15;
      	canvas.drawString(l,lx,ly);

      	//horizontal lines dS>=10
      	for(int i=0;i<11;i++)
      	{
      	    	int y=h-(i*(h-105)/10+70);//i*canvas size/scale+padleft
      	    	int x0=70;
      	    	int x1=x0+pWidth;

          		//Grid
    	        canvas.setColor(new Color(237, 237, 237));
    	        canvas.drawLine(70,y,w-35,y);//from left to right

	            //Labels
    	        canvas.setColor(Color.BLACK);
    	        l=Double.toString(min+(max-min)*(i*1.0/10));
              boolean precision=false;
              int afterPoint=0;
              String finalL="";
              char c=' ';
              for(int position=0;position<l.length();position++)
              {
                c=l.charAt(position);
                if(!precision)
                {
                  finalL+=c;
                }
                else if(precision && afterPoint<2)
                {
                  finalL+=c;
                  afterPoint+=1;
                }
                if(c=='.')
                {
                  precision=true;
                }
              }

              lWidth=m.stringWidth(finalL);
          		lx=x0-lWidth-5;//position x of left label => 5 of margin
          		ly=y+(mHeight/2)-3;//position y of bottom label => vertically centered

              canvas.drawString(finalL,lx,ly);

	            //Marks
    	        canvas.drawLine(x0,y,x1,y);//from left to right
        }

      	//label y
      	canvas.setColor(Color.BLACK);
      	l="Ord";
      	lx=lx+20;
      	ly=ly-20;
      	canvas.drawString(l,lx,ly);

      	//title
      	canvas.drawString(this.title,lx+250,ly);


      	////////////////////////////////

        //Axis
        canvas.drawLine(70,h-70,70,35);
        canvas.drawLine(70,h-70,w-35,h-70);

        ////////////////////////////////

        //Data
        oldStroke=canvas.getStroke();

      	for(int j=0;j<nbLines;j++)
      	{
      		int pS=points[j].size()-1;

      		//Lines
      		if(j==0)
      		{
      			canvas.setColor(green);
      		}
      		if(j==1)
      		{
      			canvas.setColor(blue);
      		}
      		if(j==2)
      		{
      			canvas.setColor(orange);
      		}
      		if(j==3)
      		{
      			canvas.setColor(purple);
      		}
      		if(j==4)
      		{
      			canvas.setColor(red);
      		}
      		if(j==5)
      		{
      			canvas.setColor(brown);
      		}
      		if(j==6)
      		{
      			canvas.setColor(grey);
      		}
      		canvas.setStroke(new BasicStroke(3f));//width of the line
      		for(int i=0;i<pS;i++)
      		{
      		    	int x1=points[j].get(i).x;
      		    	int y1=points[j].get(i).y;
      		    	int x2=points[j].get(i+1).x;
      		    	int y2=points[j].get(i+1).y;
      		    	canvas.drawLine(x1,y1,x2,y2);
      		}

      		//Points => after lines to see them (foreground)
      		if(displayPoints)
      		{
      			canvas.setStroke(oldStroke);
      			canvas.setColor(Color.BLACK);
      			for (int i=0;i<=pS;i++)
      			{
      			    	int x=points[j].get(i).x-pWidth/2;//adjust to center the point at the position
      			    	int y=points[j].get(i).y-pWidth/2;//adjust to center the point at the position
      			    	canvas.fillOval(x,y,pWidth,pWidth);
      			}
      		}
      	}
    }
}
