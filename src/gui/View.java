package gui;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class View extends JPanel
{
  // class View

	static final int PREF_W = 900;
	static final int PREF_H = 720;

	/**
	 * @author Quentin Raymondaud
	 * CardLayout that contains all views
	 */
	private CardLayout cardLayout = new CardLayout();

	/**
	 * @author Quentin Raymondaud
	 * View index to access to others
	 */
	private MenuView menuView = new MenuView(this);

	/**
	* @author Quentin Raymondaud
	* View to launch a local game
	*/
	private ActionLocalGame local = new ActionLocalGame(this);

	/**
	* @author Quentin Raymondaud
	* View to create a network game
	*/
	private ActionNetworkGame network = new ActionNetworkGame(this);

	/**
	* @author Quentin Raymondaud
	* View to launch and join a network game
	*/
	private ActionJoinGame join = new ActionJoinGame(this);

	/**
	 * @author Gaelle Laperriere
	 * View to show the global statistics
	 */
	private ActionStatistics stats = new ActionStatistics(this);

	/**
	 * @author Gaelle Laperriere
	 * View to changes the options
	 */
	private ActionOptions options = new ActionOptions(this);

	/**
	 * @author Gaelle Laperriere
	 * View to show the profiles
	 */
	private ActionProfiles profiles = new ActionProfiles(this);

	/**
	 * @author Gaelle Laperriere
	 * View to add a new player
	 */
	private ActionNewPlayer np = new ActionNewPlayer(this);

	/**
	 * @author Gaelle Laperriere
	 * View to add a new AI
	 */
	private ActionNewAI na = new ActionNewAI(this);

	/**
	 * @author Gaelle Laperriere
	 * View to edit an AI's profiles
	 */
	private ActionAIEdition ae = new ActionAIEdition(this);

	/**
	 * @author Gaelle Laperriere
	 * View to edit a player's profile
	 */
	private ActionPlayerEdition pe = new ActionPlayerEdition(this);

	/**
	 * @author Gaelle Laperriere
	 * View to change a user's image
	 */
	private ActionImage image = new ActionImage(this);

	/**
	 * @author Gaelle Laperriere
	 * View to show more statistics about a user
	 */
	private ActionMoreStatistics morestat = new ActionMoreStatistics(this);

	/**
	 * @author Gaelle Laperriere
	 * View to choose which graphic use
	 */
	private ActionGraphicOptions graphopt = new ActionGraphicOptions(this);

	/**
	 * @author Gaelle Laperriere
	 * View to see a statistic graphic
	 */
	private ActionGraphic graph = new ActionGraphic(this);

	/**
	 * @author Gaelle Laperriere
	 * View to set the options for type A statistic graphic
	 */
	private ActionAGraphicOptions agraphopt = new ActionAGraphicOptions(this);

	/**
	 * @author Gaelle Laperriere
	 * View to set the options for type B statistic graphic
	 */
	private ActionBGraphicOptions bgraphopt = new ActionBGraphicOptions(this);

	/**
	 * @author Gaelle Laperriere
	 * View to choose the game to replay
	 */
	private ActionReplay replay = new ActionReplay(this);

	/**
	 * @author Gaelle Laperriere
	 * View to replay the game
	 */
	private ActionReplaying replaying = new ActionReplaying(this);


	/**
	 * @author Gaelle Laperriere
	 * All the data to launch personalised panels
	 */
	protected int p=0;

	protected boolean elo=false;
	protected boolean time=false;
	protected boolean nb=false;
	protected boolean win=false;
	protected boolean lose=false;
	protected boolean draw=false;
	protected boolean score=false;

	protected int type=0;

	protected int nbProf=0;
	protected int p1=0;
	protected int p2=1;
	protected int p3=2;
	protected int p4=3;
	protected int p5=4;

	protected int dataC=0;
	protected int dataC2=0;

	protected boolean anTime=false;
	protected String fileName="";

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		repaint();
		revalidate();
	}
	/**
	 * @author Quentin Raymondaud
	 * Create a new View and add all inner panels
	 */
	public View()
	{
		/**
		 * @author Quentin Raymondaud
		 */
		setLayout(cardLayout);
		add(menuView, MenuView.NAME);
		add(local, ActionLocalGame.NAME);
		add(network, ActionNetworkGame.NAME);
		add(join, ActionJoinGame.NAME);

		/**
		 * @author Gaelle Laperriere
		 */
		add(stats, ActionStatistics.NAME);
		add(options, ActionOptions.NAME);
		add(profiles, ActionProfiles.NAME);
		add(np, ActionNewPlayer.NAME);
		add(na, ActionNewAI.NAME);
		add(pe, ActionPlayerEdition.NAME);
		add(ae, ActionAIEdition.NAME);
		add(image, ActionImage.NAME);
		add(morestat, ActionMoreStatistics.NAME);
		add(graphopt, ActionGraphicOptions.NAME);
		add(graph, ActionGraphic.NAME);
		add(agraphopt, ActionAGraphicOptions.NAME);
		add(bgraphopt, ActionBGraphicOptions.NAME);
		add(replay, ActionReplay.NAME);
		add(replaying, ActionReplaying.NAME);
	}

	@Override
	public Dimension getPreferredSize()
	{
		if (isPreferredSizeSet())
		{
			return super.getPreferredSize();
		}
		else
		{
			return new Dimension(PREF_W, PREF_H);
		}
	}

	/**
	 * @author Quentin Raymondaud
	 * [showCard description]
	 * Function called by GoToAction to show a precise Panel
	 * @param key String : name of view
	 */
	public void showCard(String key)
	{
		System.out.println(key);
		network.closeServer();
		join.close();
		if ( key.equals("Local Game"))
		{
			remove(local);
			local = new ActionLocalGame(this);
			add(local, ActionLocalGame.NAME);
		}

		if ( key.equals("Create Network Game"))
		{
			remove(network);
			network = new ActionNetworkGame(this);
			add(network, ActionNetworkGame.NAME);
		}
		if ( key.equals("Join Network Game"))
		{
			remove(join);
			join = new ActionJoinGame(this);
			add(join, ActionJoinGame.NAME);
		}

		cardLayout.show(this, key);
		// or swap by hand if you don't want to use CardLayout
		// but remember to revalidate and repaint whenever doing it by hand
	}

	/**
	 * @author Gaelle Laperriere
	 * [showCardByHand description]
	 * Function called by GoToAction to show a precise Panel by reloading it
	 * @param key String : name of view
	 * @param prof Int : profile's id if needed (for more infos or edition)
	 */
	public void showCardByHand(String key,int prof)
	{
		this.p=prof;
		if(key.equals("AI Edition"))
		{
			ae.removeAll();
			remove(ae);
			ActionAIEdition ae=new ActionAIEdition(this);
			add(ae, ActionAIEdition.NAME);
			ae.revalidate();
			ae.repaint();
		}
		else if(key.equals("Player Edition"))
		{
			pe.removeAll();
			remove(pe);
			ActionPlayerEdition pe=new ActionPlayerEdition(this);
			add(pe, ActionPlayerEdition.NAME);
			pe.revalidate();
			pe.repaint();
		}
		else if(key.equals("Profiles"))
		{
			profiles.removeAll();
			remove(profiles);
			ActionProfiles profiles=new ActionProfiles(this);
			add(profiles, ActionProfiles.NAME);
			profiles.revalidate();
			profiles.repaint();
		}
		else if(key.equals("Image"))
		{
			image.removeAll();
			remove(image);
			ActionImage image=new ActionImage(this);
			add(image, ActionImage.NAME);
			image.revalidate();
			image.repaint();
		}
		else if(key.equals("More Statistics"))
		{
			morestat.removeAll();
			remove(morestat);
			ActionMoreStatistics morestat=new ActionMoreStatistics(this);
			add(morestat, ActionMoreStatistics.NAME);
			morestat.revalidate();
			morestat.repaint();
		}
		else if(key.equals("Statistics"))
		{
			stats.removeAll();
			remove(stats);
			ActionStatistics stats=new ActionStatistics(this);
			add(stats, ActionStatistics.NAME);
			stats.revalidate();
			stats.repaint();
		}
		else if(key.equals("Line Chart Type Selection"))
		{
			graphopt.removeAll();
			remove(graphopt);
			ActionGraphicOptions graphopt=new ActionGraphicOptions(this);
			add(graphopt, ActionGraphicOptions.NAME);
			graphopt.revalidate();
			graphopt.repaint();
		}
		else if(key.equals("Line Chart"))
		{
			graph.removeAll();
			remove(graph);
			ActionGraphic graph=new ActionGraphic(this);
			add(graph, ActionGraphic.NAME);
			graph.revalidate();
			graph.repaint();
		}
		else if(key.equals("Line Chart Options (A)"))
		{
			agraphopt.removeAll();
			remove(agraphopt);
			ActionAGraphicOptions agraphopt=new ActionAGraphicOptions(this);
			add(agraphopt, ActionAGraphicOptions.NAME);
			agraphopt.revalidate();
			agraphopt.repaint();
		}
		else if(key.equals("Line Chart Options (B)"))
		{
			bgraphopt.removeAll();
			remove(bgraphopt);
			ActionBGraphicOptions bgraphopt=new ActionBGraphicOptions(this);
			add(bgraphopt, ActionBGraphicOptions.NAME);
			bgraphopt.revalidate();
			bgraphopt.repaint();
		}
		else if(key.equals("Replay"))
		{
			replay.removeAll();
			remove(replay);
			ActionReplay replay=new ActionReplay(this);
			add(replay, ActionReplay.NAME);
			replay.revalidate();
			replay.repaint();
		}
		else if(key.equals("Replaying"))
		{
			replaying.removeAll();
			remove(replaying);
			ActionReplaying replaying=new ActionReplaying(this);
			add(replaying, ActionReplaying.NAME);
			replaying.revalidate();
			replaying.repaint();
		}
		cardLayout.show(this, key);
	}

	/**
	 * @author Gaelle Laperriere
	 * Functions used to launch personalised panels with data changing
	 */

        ////////////////////////////////A sets

	//true or false + nb of profiles
	public void setDataForGraphicA(boolean elo, boolean time, boolean nb, boolean win, boolean lose, boolean draw, boolean score, int nbProf)
	{
		this.elo=elo;
		this.nb=nb;
		this.time=time;
		this.win=win;
		this.lose=lose;
		this.draw=draw;
		this.score=score;
		this.nbProf=nbProf;
	}

	//profiles + how many data
	public void setDataForGraphicATwo(int p1, int p2, int p3, int p4, int p5, int dataC)
	{
		this.p1=p1;
		this.p2=p2;
		this.p3=p3;
		this.p4=p4;
		this.p5=p5;
		this.dataC=dataC;
	}

	//scale
	public void setDataForGraphicAThree(int dataC2)
	{
		this.dataC2=dataC2;
		this.type=1;
	}

        ////////////////////////////////B sets

	//true or false
	public void setDataForGraphicB(boolean elo, boolean time, boolean nb, boolean win, boolean lose, boolean draw, boolean score)
	{
		this.elo=elo;
		this.time=time;
		this.nb=nb;
		this.win=win;
		this.lose=lose;
		this.draw=draw;
		this.score=score;
	}

	//how many data ?
	public void setDataForGraphicBTwo(int dataC)
	{
		this.dataC=dataC;
	}

	//scale
	public void setDataForGraphicBThree(int dataC2)
	{
		this.dataC2=dataC2;
		this.type=2;
	}

        ////////////////////////////////global gets

	//true or false
	public boolean[] getDataForGraphic()
	{
		boolean [] data= new boolean[7];
		if(!this.anTime)
		{
			data[0]=true;
		}
		else
		{
			data[0]=this.elo;
		}
		data[1]=this.time;
		data[2]=this.nb;
		data[3]=this.win;
		data[4]=this.lose;
		data[5]=this.draw;
		data[6]=this.score;
		return data;
	}

	//nb of profiles
	public int getNbProf()
	{
		return this.nbProf;
	}

	//which profiles
	public int[] getProfiles()
	{
		int [] profiles= new int[5];
		profiles[0]=this.p1;
		profiles[1]=this.p2;
		profiles[2]=this.p3;
		profiles[3]=this.p4;
		profiles[4]=this.p5;
		return profiles;
	}

	//how many data ?
	public int getDataC()
	{
		return this.dataC;
	}

	//scale
	public int getDataC2()
	{
		return this.dataC2;
	}

	//type of graphic
	public int getGraphType()
	{
		return this.type;//1=A,2=B
	}

	//when a single profile is required to launch the next pannel (edition, more stats, graphic A...)
	public int getProfile()
	{
	   	return this.p;
	}

        ////////////////////////////////global sets

	public void setAnTime()
	{
		this.anTime=true;
	}

	public void restartGraphicData()
	{
		this.elo=false;
		this.time=false;
		this.nb=false;
		this.win=false;
		this.lose=false;
		this.draw=false;
		this.score=false;

		this.type=0;

		this.nbProf=0;
		this.p1=0;
		this.p2=1;
		this.p3=2;
		this.p4=3;
		this.p5=4;
		this.dataC=0;
		this.dataC2=0;

		this.anTime=false;
		this.p=0;
	}

	public void setFileName(String fN)
	{
		this.fileName=fN;
	}

	public String getFileName()
	{
		return this.fileName;
	}
}
