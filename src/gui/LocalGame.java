package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.Timer;

import engine.agent.Player;
import engine.game.Game;
import stats.Replay;
import stats.Profile;
import stats.Ranking;

/**
* @author Imad Iguichi
* @author Thomas Robine
*/
public class LocalGame extends JPanel
{
	/**
	* Game
	*/
	private Game  pinguLocal;
	public static final String name = "Pingu";
	public static boolean startAll = false;
	/**
	* current Player
	*/
	public Player user ;

	/**
	* Board panel
	*/
	private JPanel board = new JPanel();

	/**
	* Card Panel
	*/
	private JPanel cards = new JPanel();

	/**
	* Other players Panel
	*/
	private JPanel players = new JPanel();

	/**
	* card selected
	*/
	public String roundAc ="FINISHED";

	/**
	* place selected
	*/
	public String roundPlace  = "FINISHED";

	/**
	* Refresh inner panel or not
	*/
	public boolean flag = true;

	/**
	* main View reference
	*/
	private View myMain ;
	
	private Replay replay;

	/**
	* Engine Thread
	*/
	private SwingWorker engine;
	private JButton quitButton;

	private Timer timer;
	private Timer timerUser;
	private Timer timerOther;
	private int gameTime;
	private JLabel timeGameUser = new JLabel("...");
	public JLabel timeGameOther =  new JLabel("...");
	public JLabel timeGame = new JLabel("...");

	public Player joueur;

	public static final ImageIcon yellowCard=new ImageIcon("../src/gui/Cards/yellow.jpg");
	public static final ImageIcon purpleCard=new ImageIcon("../src/gui/Cards/purple.jpg");
	public static final ImageIcon blueCard=new ImageIcon("../src/gui/Cards/blue.jpg");
	public static final ImageIcon greenCard=new ImageIcon("../src/gui/Cards/green.jpg");
	public static final ImageIcon redCard=new ImageIcon("../src/gui/Cards/red.jpg");
	public static final ImageIcon unknownCard=new ImageIcon("../src/gui/Cards/unknown.jpg");
	public static final ImageIcon usedCard=new ImageIcon("../src/gui/Cards/empty.jpg");
	public static final ImageIcon emptyCard=new ImageIcon("../src/gui/Cards/index.png");


	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		int delay = 5 ;//milliseconds
		ActionListener taskPerformer = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				if (flag )
				{
					playersInfo();
					userBoard();
					userCards();
					flag=false;
				}
			}
		};
		new javax.swing.Timer(delay, taskPerformer).start();
		revalidate();
		repaint();

	}

	/**
	* Engine Thread
	*/
	public void run()
	{
		LocalGame t = this;
		timer.start();
		engine = new SwingWorker<Void, Void>()
		{

			@Override
			protected Void doInBackground() throws Exception
			{
				while ( !startAll )
				{
					try {
						Thread.sleep(10);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				while( pinguLocal.getGame() && ! isCancelled() )
				{
					pinguLocal.play(t);
				}
				return null ;
			}
			@Override
			protected void done()
			{
				removeAll();
				
				add(quitButton);
				rankTable();
			}
		};
		engine.execute();


		System.out.println(name + " exiting.");
	}

	/**
	* Display the current cards panel
	*/
	public void userCards()
	{
		String cardsS = user.getCards();
		ImageIcon [] cardI =new ImageIcon[cardsS.length()];
		JButton[] cardButtons = new JButton[cardsS.length()];
		remove(cards);
		cards = new JPanel();
		cards.setLayout(new BoxLayout( cards,BoxLayout.X_AXIS));
		cards.setBorder(BorderFactory.createEmptyBorder(20, 0, 20, 0));
		JPanel tmp = new JPanel();
		JLabel pName = new JLabel("You : "+user.getPseudo());
		if ( pinguLocal.getPlayer(pinguLocal.getPlayerIndex(),pinguLocal.getNbPlayer()) == user ) {
			pName.setForeground(Color.orange);
			timerUser = new Timer(1,new ActionListener() 
			{
				@Override
				public void actionPerformed(ActionEvent e) 
				{
					displayGameTimeUser();
				}
			});
			timerUser.start();
		}
		tmp.add(timeGameUser);
		tmp.add(pName);

		for(int j=0;j<cardsS.length();j++)
		{

			if(cardsS.charAt(j) != ' ')
			{

				if(cardsS.charAt(j) == 'R')
				{
					cardI[j]=redCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="R";
							System.out.println(roundAc);
						}
					} );
				}
				else if(cardsS.charAt(j) == 'B')
				{
					cardI[j]=blueCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="B";
							System.out.println(roundAc);
						}
					} );
				}
				else if(cardsS.charAt(j) == 'Y')
				{
					cardI[j]=yellowCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="Y";
							System.out.println(roundAc);
						}
					} );
				}
				else if(cardsS.charAt(j) == 'P')
				{
					cardI[j]=purpleCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="P";
							System.out.println(roundAc);
						}
					} );

				}
				else if(cardsS.charAt(j) == 'G')
				{
					cardI[j]=greenCard;
					cardButtons[j]=new JButton(cardI[j]);
					cardButtons[j].addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							flag=true;
							roundAc="G";
							System.out.println(roundAc);
						}
					} );

				}

				cardButtons[j].setPreferredSize(new Dimension(30,35));
				tmp.add(cardButtons[j]);
			}


		}
		if ( user.getGR())
		  tmp.add(new JLabel(" Score : "+(user.getScore()+user.getNbC())));
		else
		  tmp.add(new JLabel(" Score : "+(user.getScore())));
		cards.add(tmp);
		add(cards);
	}

	/**
	* Displayer the game board
	*/
	public void userBoard()
	{

		String [][] boardS = pinguLocal.getBoard();
		ImageIcon [][] boardI=new ImageIcon[8][8];
		JButton[][] boardButtons=new JButton[8][8];
		remove(board);
		board = new JPanel();
		board.setLayout(new BoxLayout( board,BoxLayout.Y_AXIS));
		int cmp = 0;
		for(int i=0; i<8;i++)
		{
			JPanel tmp = new JPanel();
			for(int j=0;j<8;j++)
			{
				if(boardS[i][j].equals("X") == false )
				{
					if(boardS[i][j].equals("E"))
					{
						final int num = cmp;
						boardI[i][j]=emptyCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
						//boardButtons[i][j].setOpaque(true);
						//boardButtons[i][j].setBorderPainted(false);
						if(pinguLocal.canPutCard(num,roundAc))
						{
							if(roundAc.equals("R"))
							{
								boardButtons[i][j].setBackground(Color.red);
							}
							else if(roundAc.equals("Y"))
							{
								boardButtons[i][j].setBackground(Color.yellow);
							}
							else if(roundAc.equals("P"))
							{
								boardButtons[i][j].setBackground(Color.pink);
							}
							else  if(roundAc.equals("G"))
							{
								boardButtons[i][j].setBackground(Color.green);
							}
							else if(roundAc.equals("B"))
							{
								boardButtons[i][j].setBackground(Color.cyan);
							}
							else
								boardButtons[i][j].setBackground(Color.orange);
						}

						boardButtons[i][j].addActionListener(new ActionListener()
						{
							@Override
							public void actionPerformed(ActionEvent e)
							{
								roundPlace=""+num;
								System.out.println(roundPlace);
							}
						} );
					}
					else if(boardS[i][j].equals("R"))
					{
						boardI[i][j]=redCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(boardS[i][j].equals("B"))
					{
						boardI[i][j]=blueCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(boardS[i][j].equals("Y"))
					{
						boardI[i][j]=yellowCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(boardS[i][j].equals("P"))
					{
						boardI[i][j]=purpleCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(boardS[i][j].equals("G"))
					{
						boardI[i][j]=greenCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}

					boardButtons[i][j].setPreferredSize(new Dimension(36,46));
					tmp.add(boardButtons[i][j]);
					cmp+=1;
				}
			}
			board.add(tmp);
		}
		add(board);
	}

	/**
	* Diplay other players info
	*/
	public void playersInfo()
	{
		int nbPlayer =  pinguLocal.getNbPlayer();;
		JButton[][] playerCards = new JButton[nbPlayer][];
		remove(players);
		players = new JPanel();
		players.setBorder(BorderFactory.createTitledBorder("Game Info"));
		players.setLayout(new BoxLayout( players,BoxLayout.Y_AXIS));
		for ( int i = 0 ; i < nbPlayer+1 ; i++)
		{
			JPanel tmp = new JPanel(new  FlowLayout(0,0,0));
			if ( i == nbPlayer )
			{
				int currentRound = pinguLocal.getNbRound();

				JLabel roundCount = new JLabel(" Round N° " + currentRound  + " of " + nbPlayer );
				if (currentRound == nbPlayer)
					roundCount.setForeground(Color.red);
				tmp.add(roundCount);
				players.add(tmp);
			}
			else if ( i != nbPlayer && pinguLocal.getPlayer(i,nbPlayer) != user )
			{

				String cardsS = pinguLocal.getPlayer(i,nbPlayer).getCards();
				playerCards[i] = new JButton[cardsS.length()];
				JLabel pName = new JLabel(" "+pinguLocal.getPlayer(i,nbPlayer).getPseudo()+" ");
				if ( pinguLocal.getPlayerIndex() == i ) {
					pName.setForeground(Color.orange);
					joueur = pinguLocal.getPlayer(i,nbPlayer);
					timerOther = new Timer(1000,new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							displayGameTimeOther();
						}
					});
					timerOther.start();
				}
				tmp.add(pName);
				for(int j=0;j<cardsS.length();j++)
				{
					if(cardsS.charAt(j) != ' ')
					{
						playerCards[i][j]=new JButton(unknownCard);
						playerCards[i][j].setMargin(new Insets(0,0,0,0));
						playerCards[i][j].setPreferredSize(new Dimension(8,8));
						tmp.add(playerCards[i][j]);
					}
				}
				if ( pinguLocal.getPlayer(i,nbPlayer).getGR() ) {
				  tmp.add(new JLabel(" Score : "+(pinguLocal.getPlayer(i,nbPlayer).getScore()+pinguLocal.getPlayer(i,nbPlayer).getNbC())));
				}
				else {
				  tmp.add(new JLabel(" Score : "+(pinguLocal.getPlayer(i,nbPlayer).getScore())));
				}
				tmp.add(timeGameOther);
				players.add(tmp);
			}
		}
		add(players);
	}


	/**
	 * Display game's time
	 */
	 
	 public void displayGameTime() 
	 {
		timeGame.setText(String.valueOf(gameTime));
	 }

	public void displayGameTimeUser() 
	{
		if ( user.getTimeLeft() == -1 )
			timeGameUser.setText("...");
		else
			timeGameUser.setText(String.valueOf(user.getTimeLeft()));
	}

	public void displayGameTimeOther() 
	{
		if ( joueur.getTimeLeft() == -1 )
			timeGameOther.setText("...");
		else
			timeGameOther.setText(" "+String.valueOf(joueur.getTimeLeft()));
	}

	/**
	* Display the ranktable of end of game
	*/
	public void rankTable()
	{
		JPanel rank = new JPanel();
		if ( pinguLocal.getRanking() != null )
		{
			Ranking endRank = pinguLocal.getRanking();
			Object[][] tab = new Object [pinguLocal.getNbPlayer()][6];
			String[] label = {"Pseudo", "Player Class", "Score", "Round Score", "Green Cards Played" , "Elo"};
			String[] scores = new String[pinguLocal.getNbPlayer()+1];
			for ( int i = 0 ; i < pinguLocal.getNbPlayer() ; i++ )
			{
				java.util.List<String> rankI = endRank.getPlayerRankI(i).rankToStrAL();
				for ( int j = 0 ; j < 6 ; j++ )
				{
					tab[i][j]= rankI.get(j);
					scores[i] += rankI.get(j).toString()+";";
				}
			}
			scores[pinguLocal.getNbPlayer()] = Integer.toString(endRank.nbWinner())+";"+endRank.getWinners().toString();
			if (Replay.getReplayState()) {
				replay.setScores(scores);
			}
			JTable table = new JTable(tab, label);
			JScrollPane scrollPane= new  JScrollPane(table);

			JLabel name ;
			if ( endRank.nbWinner() == 1 )
				name =  new JLabel (" Le gagnant est : " + endRank.getWinners());
			else
				name =  new JLabel (" Les gagnants sont : " + endRank.getWinners());

			//name.setForeground(Color.orange);
			rank.add(name);
			rank.add(scrollPane);
			add(rank);
		}

	}

	/**
	* LocalGame constructor
	* @param myMain   Main View
	* @param p        Profile Array
	* @param nbPlayer Number of Player
	*/
	public LocalGame (View myMain,Profile [] p,int nbPlayer,int timerBlitz)
	{
		this.myMain = myMain;
		roundAc = "FINISHED";
		roundPlace = "FINISHED";
		setBorder(BorderFactory.createTitledBorder("Menu"));

		JPanel buttonPan = new JPanel();
		buttonPan.setLayout(new BoxLayout( buttonPan,BoxLayout.X_AXIS));
		quitButton = new JButton(" Quit ");
		quitButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if ( Game.getNbGame() <= 0 ) // 0 = 1 game 
				{
					int clickedButton = JOptionPane.showConfirmDialog(null, " You really want to leave this PinguParty Game ? ", "Confirmation", JOptionPane.YES_NO_OPTION);
					if ( clickedButton == JOptionPane.YES_OPTION) 
					{
						if ( engine != null )
							engine.cancel(true);
						myMain.showCard(MenuView.NAME);
					}
				}
				else
				{
					int clickedButton = JOptionPane.showConfirmDialog(null, "Close the game while there is some running games can alterate stats files", "Warning",JOptionPane.CANCEL_OPTION);
				}
			}
		} );
		buttonPan.add(quitButton);
		JButton endRound = new JButton("Cannot Put Card");
		endRound.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				roundPlace="D";
				roundAc="D";
			}
		} );
		buttonPan.add(endRound);
		add(buttonPan);

		timer = new Timer(1000,new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				gameTime++;
				displayGameTime();
			}
		});
		add(timeGame);
		if (Replay.getReplayState() ) 
		{
			replay = new Replay(p);
		}
		setLayout(new BoxLayout( this,BoxLayout.Y_AXIS));
		pinguLocal = new Game(nbPlayer,p,timerBlitz,replay);


		user = pinguLocal.getPlayerByProfile(p[0]);
		pinguLocal = pinguLocal.shuffle();
		run();

	}


}
