package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import gui.View;
import network.Server;
import network.dataobjects.DataObjectParser;
import stats.Profile;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;


public class ActionNetworkGame extends JPanel
{
	/**
	* Panel's name
	*/
	public static final String NAME = "Create Network Game";
	public static Image background;

	/**
	* Panel of profile's selection
	*/
	private JPanel selection = new JPanel();

	/**
	* Lobby's Panel
	*/
	private JPanel lobbyNet = new JPanel(); // Contains futur players for the game

	/**
	* Lobby players panel
	*/
	private JPanel player = new JPanel();

	/**
	* Local Profile number
	*/
	private int nbLocalProfile = Profile.nbLocalProfiles();

	/**
	* Local automatic agent Profile number
	*/
	private int nbLocalIAProfile = Profile.nbLocalIAProfiles();

	/**
	* Combox of number of player's selection
	*/
	private JComboBox nbPlayer = new JComboBox();

	/**
	* NetworkGame's Panel
	*/
	private NetworkGame game ;

	/**
	* Game's Server
	*/
	private Server pinguNetwork;

	/**
	* Number of players in the future Game
	*/
	private int num = 1;

	/**
	* Local Profiles
	*/
	private Profile [] p = Profile.loadLocalProfileFromFile();

	/**
	* Selected profile for the game
	*/
	private Profile [] selectedProfiles ;

	/**
	* User's profile
	*/
	private Profile user;

	/**
	* Lobby's thread
	*/
	private SwingWorker lobbyGUI;

	/**
	* Main panel
	*/
	private View myMain ;


	/*
	* Label for indication before create the room
	*/
	private JLabel address ;

	/*
	* localProfile Array
	*/
	private Profile [] joueur = new Profile[nbLocalProfile];

	/*
	* Local automatic agent profiles array
	*/
	private Profile [] computer = new Profile[nbLocalIAProfile];

	/*
	* Profile choice JComboBox
	*/
	private JComboBox [] profiles  = new JComboBox[num];


	/*
	* Public ip address
	*/
	private String hostIp = "";
	/**
	 * Timer for Blitz
	 */
	 protected int tt;
	 protected int timerBlitz;

	@Override
	public void paintComponent(Graphics g)
	{

		super.paintComponent(g);
		g.drawImage(background, 0, 0, this);
		int delay = 5 ;//milliseconds
		ActionListener taskPerformer = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				if ( game != null )
				{
					game.setPreferredSize(game.getPreferredSize());
				}

			}
		};
		new javax.swing.Timer(delay, taskPerformer).start();
		repaint();
		revalidate();
	}

	/**
	* Close connections and kill lobby's thread
	*/
	public void closeServer()
	{
		if ( lobbyGUI != null )
			lobbyGUI.cancel(true);
		if ( game != null)
			game.close();
		if ( pinguNetwork != null )
		{
			pinguNetwork.closeLobby();
		}
	}

	/**
	* Select number of local players on a network GAME
	* @param create JButton that creates the lobby's room
	*/
	public void selectionNumber(JButton create)
	{
		String [] choice = new String[0];

		if ( ( nbLocalProfile - nbLocalIAProfile >= 1 &&  nbLocalIAProfile >= 5 ) ||  nbLocalIAProfile > 5 )
			choice = new String[6] ;
		else if ( ( nbLocalIAProfile < 6 && ( nbLocalProfile - nbLocalIAProfile ) >= 1 ))
			choice = new String[nbLocalIAProfile+1] ;
		else if ( nbLocalIAProfile <= 6 )
			choice = new String[nbLocalIAProfile] ;
		else if ( nbLocalProfile >= 1 )
			choice = new String[1] ;

		for ( int i = 0 ; i < choice.length ; i ++ ) 
		{
			choice[i] = Integer.toString(i+1) ; 
		}

		nbPlayer = new JComboBox(choice);
		nbPlayer.setMaximumSize( nbPlayer.getPreferredSize() );

		nbPlayer.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if ( pinguNetwork == null)
				{
					System.out.println(num);
					num = Integer.parseInt((String)nbPlayer.getSelectedItem());
					selectedProfiles = new Profile[num];
					
					localPlayerChoice(create);
				}
			}
		});

		nbPlayer.setPreferredSize(new Dimension(330,50));
		nbPlayer.setBorder(new OvalBorder());
		add(nbPlayer);
	}

	/**
	* Player's selection's Panel
	*/
	public void localPlayerChoice(JButton create)
	{
		selection.removeAll();

		String [] namesE = new String[nbLocalIAProfile];
		String [] namesY = new String[nbLocalProfile];

		int you=0;
		int others=0;

		for(int index = 0 ; index < nbLocalProfile ;index++)
		{
			if ( p[index].getLevel()!=0)
			{
				computer[others] = p[index];
				namesE[others] = p[index].getName();
				others++;
			}
			joueur[you]=p[index];
			namesY[you]=" You : " + p[index].getName();
			you++;
		}

		profiles  = new JComboBox[num];
		for(int i=0; i < num ;i++)
		{
			Vector vector = new Vector();
			if ( i != 0)
			{
				for ( int j = 0 ; j < nbLocalIAProfile; j ++ )
				{
					vector.addElement(namesE[j]);
				}
			}
			else
			{
				for (int j = 0 ; j < nbLocalProfile ; j ++ )
				{
					vector.addElement(namesY[j]);
				}
			}

			profiles[i]= new JComboBox (vector);

			profiles[i].setPreferredSize(new Dimension(200,50));

			if ( i != 0 )
				profiles[i].setSelectedIndex(i-1);
			else
				profiles[i].setSelectedIndex(i);

			
			profiles[i].addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					JComboBox tmp = (JComboBox) e.getSource();
					int numP = tmp.getSelectedIndex();
					remove(create);
					if ( getProfileMeta(profiles,joueur,computer) )
						add(create);
					else
						JOptionPane.showConfirmDialog(null," You cannot create the game ! \n There are two identical profiles. "," Impossible ProfileMeta ",JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE);
				}
			});

			selection.add(profiles[i]);
		}
		if ( getProfileMeta(profiles,joueur,computer) )
			add(create);
		else
			JOptionPane.showConfirmDialog(null," You cannot create the game ! \n There are two identical profiles. "," Impossible ProfileMeta ",JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE);

	}

	/**
	* set a selected profile according to our choice ( and tell we if the choice is valid )
	* @param profiles  JComboBox
	* @param joueur   Profile Array
	* @param computer Profiles Array
	*/
	public boolean getProfileMeta(JComboBox [] profiles, Profile[] joueur , Profile[] computer)
	{
		for(int i=0;i<num;i++)
		{
			if ( i == 0)
			{
				selectedProfiles[i] = joueur[profiles[i].getSelectedIndex()];
				user = selectedProfiles[i];
			}
			else
				selectedProfiles[i] = computer[profiles[i].getSelectedIndex()];
		}
		for ( int j = 0 ; j < selectedProfiles.length ; j ++)
		{
			for ( int k = 0 ; k < selectedProfiles.length ; k ++)
			{
				if ( j != k && selectedProfiles[j] == selectedProfiles[k] )
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	* Lobby's thread
	*/
	public void run(JButton launchButton)
	{
		lobbyGUI = new SwingWorker<Void, Void>()
		{
			@Override
			protected Void doInBackground() throws Exception
			{
				try
				{
					int actualNb = pinguNetwork.getActualNb();
					player = new JPanel();
					player.setLayout(new BoxLayout(player, BoxLayout.Y_AXIS));
					player.setPreferredSize(new Dimension(300,400));
					generateList(launchButton);

					int i = 0;
					int index;
					while ( true && ! isCancelled() )												// Pour chaque clients
					{
						index = pinguNetwork.getActualNb(); // LOBBY DEVRAIT ETRE REFACTORE DANS SERVER
						if ( index < 6 )
						{
							pinguNetwork.acceptSocket();
							pinguNetwork.sendToAllLobby(pinguNetwork.getNetworkProfiles());
							generateList(launchButton);
							i +=1 ;
						}
						//sinon faudra lancer
					}
				}
				catch (Exception e)																	// Sinon on gère l'erreur
				{
					//e.printStackTrace();
					System.out.println(" => Nouveau client : connection ratée <=");
				}
				return null;
			}

			@Override
			public void done()
			{
				generateList(launchButton);
			}
		};
		lobbyGUI.execute();
		System.out.println("Lobby exited");
	}

	/**
	* Generate the list of connected players of the Server
	*/
	public void generateList(JButton launchButton)
	{
		player.removeAll();
		lobbyNet.removeAll();
		remove(lobbyNet);
		remove(player);

		int actualNb = pinguNetwork.getActualNb();
		JPanel player = new JPanel();
		player.setLayout(new FlowLayout(FlowLayout.CENTER, 5000, 5));
		java.util.List<Profile> prof = pinguNetwork.getProfiles();

		remove(address);
		address = new JLabel ( " Address : " + hostIp + ". DMZ must be activated and Port : 111111 available "  );
		player.add(address);
		
		for ( int j = 0 ; j < pinguNetwork.getActualNb(); j ++ )
		{
			if ( actualNb > 1)
				add(launchButton);
			else
			{
				remove(launchButton);
				revalidate();
				repaint();
			}

			JPanel playerI = new JPanel();
			playerI.setBorder(new OvalBorder());
			playerI.setMaximumSize(new Dimension(300,50));
			if ( user	== prof.get(j))
				playerI.add(new JLabel(" Host : " + prof.get(j).getName()));
			else
			{
				playerI.add(new JLabel(" Player : " + prof.get(j).getName()));
				JButton kickI = new JButton("Kick");
				final int indice = j;
				kickI.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						pinguNetwork.kickLobby(indice);
						generateList(launchButton);
					}
				});
				playerI.add(kickI);
			}
			player.add(playerI);
		}
		lobbyNet.add(player);

		add(lobbyNet);
	}

	/**
	* setLayout Y Axis to the current panel
	*/
	public void setLayout()
	{
		setLayout(new BoxLayout( this,BoxLayout.Y_AXIS));
	}

	/**
	* Generate a Selection and the Lobby's Panel for a network game
	* @param myMain Main view
	*/
	public ActionNetworkGame(View myMain)
	{
		try
       	{
       		background = ImageIO.read(new File("../src/gui/Background/B1.jpg"));
       	}
       	catch ( Exception e)
       	{;}
		setLayout(new FlowLayout(FlowLayout.CENTER, 3000, 10));
		setBorder(BorderFactory.createTitledBorder(NAME));
		
		JButton main = new JButton(new GoToAction("Main Menu", MenuView.NAME, myMain));
		main.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				closeServer();
			}
		});
		main.setPreferredSize(new Dimension(200,50));
		main.setBorder(new OvalBorder());
		add(main);
		
		JLabel annonce = new JLabel("Veuillez sélectionner une limite de temps si vous voulez jouer en mode Blitz");
		String[] times = {"Pas de limite de temps","5 secondes","10 secondes","15 secondes","20 secondes","25 secondes","30 secondes"};
		JComboBox time = new JComboBox(times);
		time.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e)
				{
					tt = time.getSelectedIndex();
					System.out.println(tt);
				}
		});	
		add(annonce);
		time.setPreferredSize(new Dimension(200,20));
		add(time);

		this.myMain = myMain;
		JButton create = new JButton("Create Room");
		create.setPreferredSize(new Dimension(200,50));
		create.setBorder(new OvalBorder());
		JButton go = new JButton("Start Game");
		go.setPreferredSize(new Dimension(200,50));
		go.setBorder(new OvalBorder());
		go.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (pinguNetwork.getActualNb() > 1 )
				{
					lobbyGUI.cancel(true);
					if (tt == 0) {
						timerBlitz = 0;
					}
					if (tt == 1) {
						timerBlitz = 5;
					}
					if (tt == 2) {
						timerBlitz = 10;
					}
					if (tt == 3) {
						timerBlitz = 15;
					}
					if (tt == 4) {
						timerBlitz = 20;
					}
					if (tt == 5) {
						timerBlitz = 25;
					}
					if (tt == 6 ) {
						timerBlitz = 30;
					}
					pinguNetwork.initGame(timerBlitz);
					game = new NetworkGame(myMain,pinguNetwork,user);					
					removeAll();
					add(game);
					time.setPreferredSize(new Dimension(200,20));
					revalidate();
					repaint();
				}
			}
		});

		create.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if ( nbLocalProfile >= num )
				{
					setLayout();
					selection.removeAll();
					remove(create);
					remove(nbPlayer);
					lobbyNet = new JPanel();
					lobbyNet.setLayout(new BoxLayout(lobbyNet, BoxLayout.Y_AXIS));
					lobbyNet.setBorder(BorderFactory.createTitledBorder("Player List"));
					selection.add(lobbyNet);
					pinguNetwork =  new Server(6,selectedProfiles);	
					repaint();
					revalidate();
					run(go);
					
				}
			}
		});
		selection.setLayout(new BoxLayout(selection, BoxLayout.Y_AXIS));
		selectionNumber(create);
		add(selection);

		//AFFICHAGE IP
		try
		{
			URL whatismyip = new URL("http://checkip.amazonaws.com");
			BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
			hostIp = in.readLine();
		}
		catch (Exception ex)
		{}
		address = new JLabel ( " Address : " + hostIp + " DMZ must be activated and Port : 11111 available"  );
		add(address);

	}
	
}
