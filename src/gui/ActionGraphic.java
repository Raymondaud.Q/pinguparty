package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JFrame;

import stats.Profile;
import stats.TempStat;
import gui.View;

import java.text.SimpleDateFormat;

/**
 * @author Gaelle Laperriere
 */
public class ActionGraphic extends JPanel
{
    //class for statistics in a graph

    /**
     * Panel's name
     */
    public static String NAME = "Line Chart";

    /**
     * Profiles's List
     */
    private Profile[] p;

    /**
     * @author Gaelle Laperriere
     * Graphic options Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionGraphic(View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));
        p=Profile.loadProfileFromFile();

        if(p.length!=0)
        {


          ////////////////////////////////

          //button to go back to global stats
          JButton rButton = new JButton(new GoToAction("Main Statistics", ActionStatistics.NAME, myMain));

          //button to go back to options
          JButton sButton = new JButton(new GoToAction("Type Selection", ActionGraphicOptions.NAME, myMain));

          ////////////////////////////////

          JPanel b = new JPanel();
          b.setLayout(new BoxLayout(b,BoxLayout.LINE_AXIS));
          b.add(rButton);
  		    b.add(sButton);
          add(b);

  	      ////////////////////////////////

      		int nbLines=0;
      		String title="Line Chart - ";
          String currentDate=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

      		int number=myMain.getDataC();//how many data ?
      		int scaleN=myMain.getDataC2();//which scale ?
      		boolean [] data = myMain.getDataForGraphic();

      		int realNumber=0;
          int date=0;

      		ArrayList<String> listLines=new ArrayList<String>();
      		ArrayList<Double> [] myList;

          ////////////////////////////////

          //set padding, number of data, scale for the title
      		String scale="round";
          boolean byGame=false;
      		int pad=1;

          if(number==2)
          {
            number=10;
          }
      		else if(number==1)
      		{
      			number=50;
      			pad=5;
      		}
          else
          {
            if(number!=0)
            {
              date=number;
            }
            number=100;
            pad=10;
          }

          if(scaleN==1)
          {
            scale="mean of games";
            byGame=true;
          }

          ////////////////////////////////

          //set the data

      		if(myMain.getGraphType()==2)//stats
      		{
      			Profile profile=p[myMain.getProfile()];
      			String name=profile.getName();
      			title+="Last "+Integer.toString(number)+" stats by "+scale+" for "+name;

      			TempStat[] tempStat;
      			if(byGame)
      			{
      				tempStat=TempStat.loadAllStatByGame(profile.getUID());
      			}
      			else
      			{
      				tempStat=TempStat.loadAllStat(profile.getUID());
      			}
      			realNumber=tempStat.length;
      			for(int i=0;i<7;i++)
      			{
      				if(data[i])
      				{
      					nbLines+=1;
      				}
      			}
      			myList= new ArrayList[nbLines];
      			int j=0;
      			for(int i=0;i<7;i++)
      			{
      				if(data[i])
      				{
      					myList[j]=new ArrayList<Double>();
      					int actual=0;

      					if(i==0)
      					{
      						listLines.add("ELO");
      						//for all data set add in j
      						for(TempStat tS : tempStat)
      						{
      							actual+=1;
      							if(actual>number)
      							{
      								break;
      							}
                    if(
                      tS.getDate().equals("null")
                      ||
                      (date==3 
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || 
                      (date==4 
                        && tS.getDate().charAt(3)==currentDate.charAt(3)
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      ||
                      (date==5
                        && tS.getDate().charAt(0)==currentDate.charAt(0)
                        && tS.getDate().charAt(1)==currentDate.charAt(1)
                        && tS.getDate().charAt(3)==currentDate.charAt(3) 
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || (date==0))
                    {
                      myList[j].add(Double.parseDouble(Float.toString(tS.getElo())));
                    }
      						}
      					}
      					else if(i==1)
      					{
      						listLines.add("Time");
      						//for all data set add in j
                  for(TempStat tS : tempStat)
                  {
                    actual+=1;
                    if(actual>number)
                    {
                      break;
                    }
                    if(
                      tS.getDate().equals("null")
                      ||
                      (date==3 
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || 
                      (date==4 
                        && tS.getDate().charAt(3)==currentDate.charAt(3)
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      ||
                      (date==5
                        && tS.getDate().charAt(0)==currentDate.charAt(0)
                        && tS.getDate().charAt(1)==currentDate.charAt(1)
                        && tS.getDate().charAt(3)==currentDate.charAt(3) 
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || (date==0))
                    {
                      myList[j].add(Double.parseDouble(Float.toString(tS.getTimeUsed())));
                    }
                  }

      					}
      					else if(i==2 && byGame)
      					{
      						listLines.add("Number of Games");
      						//for all data set add in j
      						for(TempStat tS : tempStat)
      						{
      							actual+=1;
      							if(actual>number)
      							{
      								break;
      							}
                    if(
                      tS.getDate().equals("null")
                      ||
                      (date==3 
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || 
                      (date==4 
                        && tS.getDate().charAt(3)==currentDate.charAt(3)
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      ||
                      (date==5
                        && tS.getDate().charAt(0)==currentDate.charAt(0)
                        && tS.getDate().charAt(1)==currentDate.charAt(1)
                        && tS.getDate().charAt(3)==currentDate.charAt(3) 
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || (date==0))
                    {
      							 myList[j].add(Double.parseDouble(Integer.toString(tS.getNb())));
                    }
      						}
      					}
      					else if(i==2)
      					{
      						listLines.add("Number of Rounds");
      						//for all data set add in j
      						for(TempStat tS : tempStat)
      						{
      							actual+=1;
      							if(actual>number)
      							{
      								break;
      							}
                    if(
                      tS.getDate().equals("null")
                      ||
                      (date==3 
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || 
                      (date==4 
                        && tS.getDate().charAt(3)==currentDate.charAt(3)
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      ||
                      (date==5
                        && tS.getDate().charAt(0)==currentDate.charAt(0)
                        && tS.getDate().charAt(1)==currentDate.charAt(1)
                        && tS.getDate().charAt(3)==currentDate.charAt(3) 
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || (date==0))
                    {
      							 myList[j].add(Double.parseDouble(Integer.toString(tS.getNb())));
                    }
      						}
      					}
      					else if(i==3)
      					{
      						listLines.add("Number of Victories");
      						//for all data set add in j
      						for(TempStat tS : tempStat)
      						{
      							actual+=1;
      							if(actual>number)
      							{
      								break;
      							}
                    if(
                      tS.getDate().equals("null")
                      ||
                      (date==3 
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || 
                      (date==4 
                        && tS.getDate().charAt(3)==currentDate.charAt(3)
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      ||
                      (date==5
                        && tS.getDate().charAt(0)==currentDate.charAt(0)
                        && tS.getDate().charAt(1)==currentDate.charAt(1)
                        && tS.getDate().charAt(3)==currentDate.charAt(3) 
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || (date==0))
                    {
      							 myList[j].add(Double.parseDouble(Integer.toString(tS.getWin())));
                    }
      						}
      					}
      					else if(i==4)
      					{
      						listLines.add("Number of Defeats");
      						//for all data set add in j
      						for(TempStat tS : tempStat)
      						{
      							actual+=1;
      							if(actual>number)
      							{
      								break;
      							}
                    if(
                      tS.getDate().equals("null")
                      ||
                      (date==3 
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || 
                      (date==4 
                        && tS.getDate().charAt(3)==currentDate.charAt(3)
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      ||
                      (date==5
                        && tS.getDate().charAt(0)==currentDate.charAt(0)
                        && tS.getDate().charAt(1)==currentDate.charAt(1)
                        && tS.getDate().charAt(3)==currentDate.charAt(3) 
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || (date==0))
                    {
      							 myList[j].add(Double.parseDouble(Integer.toString(tS.getLose())));
                    }
      						}
      					}
      					else if(i==5)
      					{
      						listLines.add("Number of Draws");
      						//for all data set add in j
      						for(TempStat tS : tempStat)
      						{
      							actual+=1;
      							if(actual>number)
      							{
      								break;
      							}
                    if(
                      tS.getDate().equals("null")
                      ||
                      (date==3 
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || 
                      (date==4 
                        && tS.getDate().charAt(3)==currentDate.charAt(3)
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      ||
                      (date==5
                        && tS.getDate().charAt(0)==currentDate.charAt(0)
                        && tS.getDate().charAt(1)==currentDate.charAt(1)
                        && tS.getDate().charAt(3)==currentDate.charAt(3) 
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || (date==0))
                    {
      							 myList[j].add(Double.parseDouble(Integer.toString(tS.getDraw())));
                    }
      						}
      					}
      					else
      					{
      						listLines.add("Score");
      						//for all data set add in j
      						for(TempStat tS : tempStat)
      						{
      							actual+=1;
      							if(actual>number)
      							{
      								break;
      							}
                    if(
                      tS.getDate().equals("null")
                      ||
                      (date==3 
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || 
                      (date==4 
                        && tS.getDate().charAt(3)==currentDate.charAt(3)
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      ||
                      (date==5
                        && tS.getDate().charAt(0)==currentDate.charAt(0)
                        && tS.getDate().charAt(1)==currentDate.charAt(1)
                        && tS.getDate().charAt(3)==currentDate.charAt(3) 
                        && tS.getDate().charAt(4)==currentDate.charAt(4)
                        && tS.getDate().charAt(6)==currentDate.charAt(8) 
                        && tS.getDate().charAt(7)==currentDate.charAt(9))
                      || (date==0))
                    {
      							 myList[j].add(Double.parseDouble(Integer.toString(tS.getCScore())));
                    }
      						}
      					}

      					j+=1;
      				}
      			}
      		}
      		else//profiles
      		{
      			String stat="";
      			for(int i=0;i<7;i++)
      			{
      				if(data[i])
      				{
      					if(i==0){stat="ELO data";}
      					else if(i==1){stat="Time data";}
      					else if(i==2 && byGame){stat="Games Played";}
      					else if(i==2){stat="Rounds Played";}
      					else if(i==3){stat="Victories";}
      					else if(i==4){stat="Defeats";}
      					else if(i==5){stat="Draws";}
      					else{stat="Score data";}
      				}
      			}
      			title+="Last "+Integer.toString(number)+" "+stat+" by "+scale;

      			nbLines=myMain.getNbProf()+1;
      			int[] names=myMain.getProfiles();
      			ArrayList<String> listProfiles=new ArrayList<String>();
      			myList= new ArrayList[nbLines];

      			for(int i=0;i<nbLines;i++)
      			{
      				if(names!=null && p!=null && names.length!=0 && p.length!=0)
      				{
                if(p[names[i]]!=null)
                {
        					myList[i]=new ArrayList<Double>();
        					Profile profile=p[names[i]];
        					listLines.add(profile.getName());
        					listProfiles.add(profile.getUID());

        					TempStat[] tempStat=null;
                  String[] statsBis=profile.statsProfile();
            	    if(Integer.parseInt(statsBis[5])!=0)
                  {
                    if(byGame)
          					{
          						tempStat=TempStat.loadAllStatByGame(profile.getUID());
          					}
          					else
          					{
          						tempStat=TempStat.loadAllStat(profile.getUID());
          					}
                  }
                  if(tempStat!=null)
                  {
                    realNumber=tempStat.length;
                  }
                  else
                  {
                    realNumber=0;
                    myList[i].add(0.0);
                  }
        					int actual=0;

        					//for all data set add in i
        					if(stat.equals("ELO data") && tempStat!=null)
        					{
        						for(TempStat tS : tempStat)
        						{
        							actual+=1;
        							if(actual>number)
        							{
        								break;
        							}
                      if(
                        tS.getDate().equals("null")
                        ||
                        (date==3 
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || 
                        (date==4 
                          && tS.getDate().charAt(3)==currentDate.charAt(3)
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        ||
                        (date==5
                          && tS.getDate().charAt(0)==currentDate.charAt(0)
                          && tS.getDate().charAt(1)==currentDate.charAt(1)
                          && tS.getDate().charAt(3)==currentDate.charAt(3) 
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || (date==0))
                      {
        							 myList[i].add(Double.parseDouble(Float.toString(tS.getElo())));
                      }
        						}
        					}
        					else if(stat.equals("Time data") && tempStat!=null)
        					{
        						for(TempStat tS : tempStat)
                    {
                      actual+=1;
                      if(actual>number)
                      {
                        break;
                      }
                      if(
                        tS.getDate().equals("null")
                        ||
                        (date==3 
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || 
                        (date==4 
                          && tS.getDate().charAt(3)==currentDate.charAt(3)
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        ||
                        (date==5
                          && tS.getDate().charAt(0)==currentDate.charAt(0)
                          && tS.getDate().charAt(1)==currentDate.charAt(1)
                          && tS.getDate().charAt(3)==currentDate.charAt(3) 
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || (date==0))
                      {
                        myList[i].add(Double.parseDouble(Float.toString(tS.getTimeUsed())));
                      }
                    }
        					}
        					else if((stat.equals("Games Played") || stat.equals("Rounds Played")) && tempStat!=null)
        					{
        						for(TempStat tS : tempStat)
        						{
        							actual+=1;
        							if(actual>number)
        							{
        								break;
        							}
                      if(
                        tS.getDate().equals("null")
                        ||
                        (date==3 
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || 
                        (date==4 
                          && tS.getDate().charAt(3)==currentDate.charAt(3)
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        ||
                        (date==5
                          && tS.getDate().charAt(0)==currentDate.charAt(0)
                          && tS.getDate().charAt(1)==currentDate.charAt(1)
                          && tS.getDate().charAt(3)==currentDate.charAt(3) 
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || (date==0))
                      {
        							 myList[i].add(Double.parseDouble(Float.toString(tS.getNb())));
                      }
        						}
        					}
        					else if(stat.equals("Victories") && tempStat!=null)
        					{
        						for(TempStat tS : tempStat)
        						{
        							actual+=1;
        							if(actual>number)
        							{
        								break;
        							}
                      if(
                        tS.getDate().equals("null")
                        ||
                        (date==3 
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || 
                        (date==4 
                          && tS.getDate().charAt(3)==currentDate.charAt(3)
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        ||
                        (date==5
                          && tS.getDate().charAt(0)==currentDate.charAt(0)
                          && tS.getDate().charAt(1)==currentDate.charAt(1)
                          && tS.getDate().charAt(3)==currentDate.charAt(3) 
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || (date==0))
                      {
        							 myList[i].add(Double.parseDouble(Float.toString(tS.getWin())));
                      }
        						}
        					}
        					else if(stat.equals("Defeats") && tempStat!=null)
        					{
        						for(TempStat tS : tempStat)
        						{
        							actual+=1;
        							if(actual>number)
        							{
        								break;
        							}
                      if(
                        tS.getDate().equals("null")
                        ||
                        (date==3 
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || 
                        (date==4 
                          && tS.getDate().charAt(3)==currentDate.charAt(3)
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        ||
                        (date==5
                          && tS.getDate().charAt(0)==currentDate.charAt(0)
                          && tS.getDate().charAt(1)==currentDate.charAt(1)
                          && tS.getDate().charAt(3)==currentDate.charAt(3) 
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || (date==0))
                      {
        							 myList[i].add(Double.parseDouble(Float.toString(tS.getLose())));
                      }
        						}
        					}
        					else if(stat.equals("Draws") && tempStat!=null)
        					{
        						for(TempStat tS : tempStat)
        						{
        							actual+=1;
        							if(actual>number)
        							{
        								break;
        							}
                      if(
                        tS.getDate().equals("null")
                        ||
                        (date==3 
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || 
                        (date==4 
                          && tS.getDate().charAt(3)==currentDate.charAt(3)
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        ||
                        (date==5
                          && tS.getDate().charAt(0)==currentDate.charAt(0)
                          && tS.getDate().charAt(1)==currentDate.charAt(1)
                          && tS.getDate().charAt(3)==currentDate.charAt(3) 
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || (date==0))
                      {
        							 myList[i].add(Double.parseDouble(Float.toString(tS.getDraw())));
                      }
        						}
        					}
        					else if(stat.equals("Score data") && tempStat!=null)
        					{
        						for(TempStat tS : tempStat)
        						{
        							actual+=1;
        							if(actual>number)
        							{
        								break;
        							}
                      if(
                        tS.getDate().equals("null")
                        ||
                        (date==3 
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || 
                        (date==4 
                          && tS.getDate().charAt(3)==currentDate.charAt(3)
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        ||
                        (date==5
                          && tS.getDate().charAt(0)==currentDate.charAt(0)
                          && tS.getDate().charAt(1)==currentDate.charAt(1)
                          && tS.getDate().charAt(3)==currentDate.charAt(3) 
                          && tS.getDate().charAt(4)==currentDate.charAt(4)
                          && tS.getDate().charAt(6)==currentDate.charAt(8) 
                          && tS.getDate().charAt(7)==currentDate.charAt(9))
                        || (date==0))
                      {
        							 myList[i].add(Double.parseDouble(Float.toString(tS.getCScore())));
                      }
        						}
        					}
                }
      				}
      			}
      		}

          ////////////////////////////////

          if(date==3)
          {
            title+=" this year";
          }
          else if(date==4)
          {
            title+=" this month";
          }
          else if(date==5)
          {
            title+=" today";
          }

      		JPanel textPanel = new JPanel();
      	  textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.PAGE_AXIS));
      		textPanel.add(new DrawGraph(myList,nbLines,number,title,pad));//ordinate and abscissa
      		if(nbLines>=1)
      		{
      			textPanel.add(new JLabel("Green  : "+listLines.get(0)));
      		}
      		if(nbLines>=2)
      		{
      			textPanel.add(new JLabel("Blue   : "+listLines.get(1)));
      		}
      		if(nbLines>=3)
      		{
      			textPanel.add(new JLabel("Orange : "+listLines.get(2)));
      		}
      		if(nbLines>=4)
      		{
      			textPanel.add(new JLabel("Purple : "+listLines.get(3)));
      		}
      		if(nbLines>=5)
      		{
      			textPanel.add(new JLabel("Red    : "+listLines.get(4)));
      		}
      		if(nbLines>=6)
      		{
      			textPanel.add(new JLabel("Brown  : "+listLines.get(5)));
      		}
      		if(nbLines>=7)
      		{
      			textPanel.add(new JLabel("Grey   : "+listLines.get(6)));
      		}
      		add(textPanel);
          }

        }
}
