package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.JOptionPane;

import gui.View;
import stats.Replay;
import stats.Profile;
import engine.game.Move;


/**
 * @author Gaelle Laperriere
 */
public class ActionReplay extends JPanel
{
    //class Options

    /**
     * Panel's name
     */
    public static final String NAME = "Replay";

    /**
     * @author Gaelle Laperriere
     * Options's Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionReplay(View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));

	    //columns for the statistics's table
        String [] col = {
            "Date",
            "Hour",
            "Nb Players"
        };

        //data for the statistics's table
	    String[] fileName= Replay.getFileNames();
        Object [][] data = new Object[fileName.length][3];
	    String savedData;

        for(int i=0;i<fileName.length;i++)
        {
			savedData="";
			for(int j=0; j<8; j++)
			{
				if(j==4 || j==6)
				{
					savedData+="/";
				}
				savedData+=fileName[i].charAt(j);
			}
			data[i][0]=savedData;
			savedData="";
			for(int j=9; j<15; j++)
			{
				if(j==11 || j==13)
				{
					savedData+=":";
				}
				savedData+=fileName[i].charAt(j);
			}
			data[i][1]=savedData;
			data[i][2]=Replay.getNbLine("../src/engine/data/replays/"+fileName[i]+"Profiles.txt");
        }

        DefTabMod mod= new DefTabMod(data,col);
        JTable table=new JTable(mod);

        TableColumn column=null;
        for(int i=0; i<3;i++)
        {
            column=table.getColumnModel().getColumn(i);
            column.setPreferredWidth(200);
        }

        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setRowSelectionAllowed(true);
        table.setAutoCreateRowSorter(true);

        ////////////////////////////////

        //button to go back to Main Menu
        JButton rButton = new JButton(new GoToAction("Main Menu", MenuView.NAME, myMain));

        ////////////////////////////////

        //button to replay the selected game
        JButton replayButton = new JButton("Replay it");
        replayButton.addActionListener(new ActionListener()
        {
            @Override
	        public void actionPerformed(ActionEvent event)
            {
                int r=table.convertRowIndexToModel(table.getSelectedRow());
                if(r!=-1)
                {
                    System.out.println("Replay of "+data[r][0]+" "+data[r][1]);
                    myMain.setFileName(fileName[r]);
                    myMain.showCardByHand(ActionReplaying.NAME,0);
                }
            }
        });
        
	    //button to delete a replay file
        JButton supprButton = new JButton("Delete");
        supprButton.addActionListener(new ActionListener()
        {
            @Override
	        public void actionPerformed(ActionEvent event)
            {
                int r=table.convertRowIndexToModel(table.getSelectedRow());
                if(r!=-1)
                {
                    int clickedButton = JOptionPane.showConfirmDialog(null, "Are you sure to delete this replay ?", "Confirmation", JOptionPane.YES_NO_OPTION);
                    if(clickedButton==JOptionPane.YES_OPTION)
                    {
                        Replay.deleteFiles(fileName[r]);
			            Replay.eraseFileName(fileName[r],"../src/engine/data/replayFiles.txt");
                        myMain.showCardByHand(ActionReplay.NAME,0);

                    }
                }
            }
        });
        
        //button to delete all
        JButton deleteAll = new JButton("Delete All");
        deleteAll.addActionListener(new ActionListener()
        {
            @Override
	        public void actionPerformed(ActionEvent event)
            {
                int clickedButton = JOptionPane.showConfirmDialog(null, " Really Nigga ?", "Delete all replay ", JOptionPane.YES_NO_OPTION);
                if(clickedButton==JOptionPane.YES_OPTION)
                {
                	for ( String i : fileName )
                	{
                		Replay.deleteFiles(i);
			            Replay.eraseFileName(i,"../src/engine/data/replayFiles.txt");
                	}
                    
                    myMain.showCardByHand(ActionReplay.NAME,0);

                }
            }
        });

        ////////////////////////////////

        JPanel pan = new JPanel();
        pan.setLayout(new BoxLayout(pan, BoxLayout.PAGE_AXIS));
    	if(fileName.length>=1)//if there are games 
    	{
            pan.add(replayButton);
    	    pan.add(supprButton);
    	    pan.add(deleteAll);
    	}
        pan.add(rButton);
        add(new JScrollPane(table),BorderLayout.CENTER);

	add(pan);
    }
}
