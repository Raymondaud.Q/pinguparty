package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import stats.Profile;

/**
 * @author Gaelle Laperriere
 */
public class ActionAIEdition extends JPanel
{
    //class AI's Edition

    /**
     * Panel's name
     */
    public static final String NAME = "AI Edition";

    /**
     * AI's id
     */
    protected String id;

    /**
     * AI's name
     */
    protected String name;

    /**
     * AI's level
     */
    protected int level;

    /**
     * AI's avatar
     */
    protected String url;

    /**
     * @author Gaelle Laperriere
     * AI's Edition's Constructor
     * @param myMain [description]
     */
    public ActionAIEdition(View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));
      	Profile[] profiles=Profile.loadProfileFromFile();
      	int indice= myMain.getProfile();
      	if ( indice <= profiles.length && profiles.length != indice )
      	{
          	this.id=profiles[indice].getUID();
          	this.name=profiles[indice].getName();
          	this.url=profiles[indice].getUrl();
      	}

        //Avatar
        JPanel image = new JPanel()
        {
            @Override
            public void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                try
                {
                    Image img = ImageIO.read (new File("../src/engine/data/Pictures/"+id+".img"));
                    g.drawImage(img, (this.getWidth()-70)/2, 0, 70, 70, this);
                }
                catch (IOException e)
                {
                    //e.printStackTrace();
                }
            }
        };

        image.setPreferredSize(new Dimension(70,70));

        //button to edit the avatar
        JButton iButton = new JButton("Edit");
        iButton.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                System.out.println("Edit Avatar");
		            myMain.showCardByHand(ActionImage.NAME,indice);
            }
        });

        iButton.setPreferredSize(new Dimension(100,25));
        JPanel button = new JPanel();
        button.add(iButton, BorderLayout.CENTER);

        JPanel i = new JPanel();
        GridLayout gl = new GridLayout(2,2);
        gl.setVgap(5);
        i.setLayout(gl);
        i.add(image);
        i.add(new JLabel(""));
        i.add(button);
        i.add(new JLabel(""));

        ////////////////////////////////

        //Name's edition
        JTextField t = new JTextField(name);
        JLabel tl = new JLabel("Name : ");

        t.setPreferredSize(new Dimension(150,25));

        JPanel u = new JPanel();
        u.setLayout(new BoxLayout(u,BoxLayout.LINE_AXIS));
        u.add(tl);
        u.add(t);
        
        ////////////////////////////////

        //button to confirm
        JButton goButton = new JButton("Confirm Modifications");
        goButton.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                if((!name.equals(t.getText()) && !Profile.alreadyExistsLocal(t.getText()) && t.getText()!=null) || name.equals(t.getText()))
                {
                    Profile.editProfile(id,t.getText(),url,1,"Robot");

                    System.out.println("Profiles");
                    myMain.showCardByHand(ActionProfiles.NAME,0);
                }
                else if(!name.equals(t.getText()) && (Profile.alreadyExistsLocal(t.getText()) || t.getText()==null))
                {
                    int clickedButton = JOptionPane.showConfirmDialog(null, "This name is already taken.", "Popup", JOptionPane.DEFAULT_OPTION);
                }
            }
        });

        //button to go back to profile
        JButton rButton = new JButton("Profiles");
	      rButton.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
		            myMain.showCardByHand(ActionProfiles.NAME,0);
	          }
        });

        ////////////////////////////////

        JPanel b1 = new JPanel();
        b1.add(goButton);
        JPanel b2 = new JPanel();
        b2.add(rButton);

        JPanel b = new JPanel();
        b.setLayout(new BoxLayout(b,BoxLayout.LINE_AXIS));
        b.add(b1);
        b.add(b2);

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        p.add(i);
        p.add(u);
        p.add(b);
        add(p);
    }
}
