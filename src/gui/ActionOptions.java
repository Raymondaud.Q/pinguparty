package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JLabel;

import gui.View;

import stats.Replay;


/**
 * @author Gaelle Laperriere
 */
public class ActionOptions extends JPanel
{
    //class Options

    /**
     * Panel's name
     */
    public static final String NAME = "Options";

    /**
     * @author Gaelle Laperriere
     * Options's Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionOptions(View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));

	JLabel saveGamesLabel= new JLabel("Game's record:");
	JCheckBox saveGames = new JCheckBox("");
	if(Replay.getReplayState())//read (think to rewrite the panel)
	{
		saveGames.setSelected(true);
	}

	JLabel nbSavedGamesLabel = new JLabel("Nb of games saved :");

	JRadioButton one = new JRadioButton("1");
	JRadioButton five = new JRadioButton("5");
	JRadioButton ten = new JRadioButton("10");
	JRadioButton twenty = new JRadioButton("20");
	JRadioButton thirty = new JRadioButton("30");
	JRadioButton fifty = new JRadioButton("50");
	
	ButtonGroup bg = new ButtonGroup();
	bg.add(one);
	bg.add(five);
	bg.add(ten);
	bg.add(twenty);
	bg.add(thirty);
	bg.add(fifty);

	int nbMaxReplayFiles=Replay.getNbMaxReplayFiles();

	if(nbMaxReplayFiles==1)
	{
		one.setSelected(true);
	}
	else if(nbMaxReplayFiles==5)
	{
		five.setSelected(true);
	}
	else if(nbMaxReplayFiles==10)
	{
		ten.setSelected(true);
	}
	else if(nbMaxReplayFiles==20)
	{
		twenty.setSelected(true);
	}
	else if(nbMaxReplayFiles==30)
	{
		thirty.setSelected(true);
	}
	else
	{
		fifty.setSelected(true);
	}

        /*JLabel lgl = new JLabel("Language :");
        JPanel lab = new JPanel();
        lab.add(lgl);

        String[] lgu = {"Select", "English", "French"};
        JComboBox lg = new JComboBox(lgu);
        lg.setPreferredSize(new Dimension(100,20));
        JPanel comb = new JPanel();
        comb.add(lg);

        JPanel l = new JPanel();
        l.setLayout(new BoxLayout(l, BoxLayout.LINE_AXIS));
        l.add(lab);
        l.add(comb);*/

        ////////////////////////////////

        /*JLabel cll = new JLabel("Color :");
        JPanel lab2 = new JPanel();
        lab2.add(cll);


        String[] colors = {"Select", "Blue", "White"};
        JComboBox cl = new JComboBox(colors);
        cl.setPreferredSize(new Dimension(100,20));
        JPanel comb2 = new JPanel();
        comb2.add(cl);

        JPanel c = new JPanel();
        c.setLayout(new BoxLayout(c, BoxLayout.LINE_AXIS));
        c.add(lab2);
        c.add(comb2);*/

        ////////////////////////////////

        //button to confirm
        JButton cButton = new JButton("Confirm Choices");
        cButton.addActionListener(new ActionListener()
        {
            @Override
	    public void actionPerformed(ActionEvent event)
            {
                /*if(cl.getSelectedItem().toString().equals("Blue"))
                {
                    setBackground(Color.BLUE);
                    System.out.println("Background : Blue");
                }
                if(cl.getSelectedItem().toString().equals("White"))
                {
                    setBackground(Color.WHITE);
                    System.out.println("Background : White");
                }
                if(lg.getSelectedItem().toString().equals("English"))
                {
                    removeAll();
                    //englishOptions();
                    frenchOptions();
                }
                if(lg.getSelectedItem().toString().equals("French"))
                {
                    removeAll();
                    frenchOptions();
                }*/
	
		if(saveGames.isSelected())
		{
			Replay.enableReplay();
		}
		else
		{
			Replay.disableReplay();
		}
		if(one.isSelected())
		{
			Replay.setNbMaxReplayFiles(1);
		}
		else if(five.isSelected())
		{
			Replay.setNbMaxReplayFiles(5);
		}
		else if(ten.isSelected())
		{
			Replay.setNbMaxReplayFiles(10);
		}
		else if(twenty.isSelected())
		{
			Replay.setNbMaxReplayFiles(20);
		}
		else if(thirty.isSelected())
		{
			Replay.setNbMaxReplayFiles(30);
		}
		else
		{
			Replay.setNbMaxReplayFiles(50);
		}
		myMain.showCardByHand(MenuView.NAME,0);
            }
        });

        //button to go back to Main Menu
        JButton rButton = new JButton(new GoToAction("Main Menu", MenuView.NAME, myMain));

        ////////////////////////////////

        /*JPanel b = new JPanel();
        b.setLayout(new BoxLayout(b,BoxLayout.LINE_AXIS));
        b.add(goButton);
        b.add(rButton);	

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        p.add(l);
        p.add(c);
        p.add(b);
        add(p);*/

	JPanel radioPanel = new JPanel();
	GridLayout gl = new GridLayout(3,2);
	radioPanel.setLayout(gl);
	radioPanel.add(one);
	radioPanel.add(five);
	radioPanel.add(ten);
	radioPanel.add(twenty);
	radioPanel.add(thirty);
	radioPanel.add(fifty);

	JPanel optionsPanel= new JPanel();
	GridLayout gl2 = new GridLayout(3,2);
	optionsPanel.setLayout(gl2);
	optionsPanel.add(saveGamesLabel);
	optionsPanel.add(saveGames);
	optionsPanel.add(nbSavedGamesLabel);
	optionsPanel.add(radioPanel);

	JPanel buttonsPanel=new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.LINE_AXIS));
	buttonsPanel.add(rButton);
	buttonsPanel.add(cButton);

	JPanel finalPanel=new JPanel();
	finalPanel.setLayout(new BoxLayout(finalPanel,BoxLayout.PAGE_AXIS));
	finalPanel.add(optionsPanel);
	finalPanel.add(buttonsPanel);
	
	add(finalPanel);
    }
}
