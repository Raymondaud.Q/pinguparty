package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import stats.Profile;
import gui.View;

/**
 * @author Gaelle Laperriere
 */
public class ActionGraphicOptions extends JPanel
{
    //class options to choose the type of graphic

    /**
     * Panel's name
     */
    public static String NAME = "Line Chart Type Selection";

    /**
     * @author Gaelle Laperriere
     * Graphic options Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionGraphicOptions (View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));

	JLabel label= new JLabel("Which type of line chart do you want to see ?");
	
        //button to select options for the A type
	JButton aButton = new JButton("One Stat for one Profile or more");
	aButton.addActionListener(new ActionListener()
	{
	    	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.restartGraphicData();
			myMain.showCardByHand(ActionAGraphicOptions.NAME,0);
		}
        });

        //button to select options for the B type
	JButton bButton = new JButton("One Profile for one Stat or more");
	bButton.addActionListener(new ActionListener()
	{
	    	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.restartGraphicData();
			myMain.showCardByHand(ActionBGraphicOptions.NAME,0);
		}
        });

        ////////////////////////////////  

        //button to go back to global stats
        JButton rButton = new JButton(new GoToAction("Cancel", ActionStatistics.NAME, myMain));

        ////////////////////////////////
	
	JPanel b = new JPanel();
        b.setLayout(new BoxLayout(b,BoxLayout.PAGE_AXIS));
        b.add(label);
        b.add(aButton);
	b.add(bButton);
        b.add(rButton);

	add(b);
    }
}
