package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Popup;
import javax.swing.JOptionPane;

import stats.Profile;
import gui.View;

/**
 * @author Gaelle Laperriere
 */
public class ActionAGraphicOptions extends JPanel
{
    //class options for type A graphic

    /**
     * Panel's name
     */
    public static String NAME = "Line Chart Options (A)";

    /**
     * Profiles's List
     */
    private Profile[] p;

    /**
     * @author Gaelle Laperriere
     * Graphic options Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionAGraphicOptions (View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));
        p=Profile.loadProfileFromFile();

      	////////////////////////////////

      	//n profiles => 1 stat

      	//players
      	JLabel profLabel = new JLabel("How many profiles do you want to see ?");
      	JComboBox players = new JComboBox();

      	for(int i=0;i<Profile.getNbLine() && i<5;i++)
      	{
      		players.addItem(Integer.toString(i+1));
      	}
        if(p.length!=0)
        {
      	   players.setSelectedIndex(myMain.getNbProf());
         }

      	//stat
      	JLabel statsLabel = new JLabel("Choose the statistic :");
      	JRadioButton elo = new JRadioButton("ELO");
      	JRadioButton nb = new JRadioButton("Number of Games/Rounds");
      	JRadioButton win = new JRadioButton("Number of Victories");
      	JRadioButton lose = new JRadioButton("Number of Defeats");
      	JRadioButton draw = new JRadioButton("Number of Draws");
      	JRadioButton time = new JRadioButton("Time");
      	JRadioButton score = new JRadioButton("Score");

        boolean [] data= myMain.getDataForGraphic();
      	if(data[0])
      	{
      		elo.setSelected(true);
      	}
      	if(data[1])
      	{
      		time.setSelected(true);
      	}
      	if(data[2])
      	{
      		nb.setSelected(true);
      	}
      	if(data[3])
      	{
      		win.setSelected(true);
      	}
      	if(data[4])
      	{
      		lose.setSelected(true);
      	}
      	if(data[5])
      	{
      		draw.setSelected(true);
      	}
      	if(data[6])
      	{
      		score.setSelected(true);
      	}

      	ButtonGroup bg = new ButtonGroup();
      	bg.add(elo);
      	bg.add(nb);
      	bg.add(win);
      	bg.add(lose);
      	bg.add(draw);
      	bg.add(time);
      	bg.add(score);

        ////////////////////////////////

	      players.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicA(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected(), players.getSelectedIndex());
          			myMain.setAnTime();
          			myMain.showCardByHand(ActionAGraphicOptions.NAME,players.getSelectedIndex());
          		}
        });

	      elo.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicA(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected(), players.getSelectedIndex());
          			myMain.setAnTime();
          			myMain.showCardByHand(ActionAGraphicOptions.NAME,players.getSelectedIndex());
          		}
        });

	      nb.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicA(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected(), players.getSelectedIndex());
          			myMain.setAnTime();
          			myMain.showCardByHand(ActionAGraphicOptions.NAME,players.getSelectedIndex());
          		}
        });

	      win.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicA(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected(), players.getSelectedIndex());
          			myMain.setAnTime();
          			myMain.showCardByHand(ActionAGraphicOptions.NAME,players.getSelectedIndex());
          		}
        });

	      lose.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicA(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected(), players.getSelectedIndex());
          			myMain.setAnTime();
          			myMain.showCardByHand(ActionAGraphicOptions.NAME,players.getSelectedIndex());
          		}
        });

	      draw.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicA(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected(), players.getSelectedIndex());
          			myMain.setAnTime();
          			myMain.showCardByHand(ActionAGraphicOptions.NAME,players.getSelectedIndex());
          		}
        });

	      time.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicA(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected(), players.getSelectedIndex());
          			myMain.setAnTime();
          			myMain.showCardByHand(ActionAGraphicOptions.NAME,players.getSelectedIndex());
          		}
        });

        score.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicA(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected(), players.getSelectedIndex());
          			myMain.setAnTime();
          			myMain.showCardByHand(ActionAGraphicOptions.NAME,players.getSelectedIndex());
          		}
        });

        //button to go back to global stats
        JButton rButton = new JButton(new GoToAction("Cancel", ActionStatistics.NAME, myMain));

        //button to see the graphic
        JButton nButton = new JButton("GO");
        nButton.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			int[] profiles=myMain.getProfiles();
          			int nbProf=players.getSelectedIndex();
          			boolean b=true;
          			for(int i=0;i<nbProf;i++)
          			{
          				for(int j=i+1;j<=nbProf;j++)
          				{
          					if(profiles[i]==profiles[j])
          					{
          						b=false;
          					}
          				}
          			}
          			if(b)
          			{
          				myMain.setDataForGraphicAThree(myMain.getDataC2());
          		        	myMain.showCardByHand(ActionGraphic.NAME,myMain.getProfile());
          			}
          			else
          			{
          				JOptionPane.showConfirmDialog(null, "Please choose different profiles.", "", JOptionPane.OK_CANCEL_OPTION);
          			}
          		}
        });

        ////////////////////////////////

      	JPanel radioPanel = new JPanel();
      	GridLayout gl = new GridLayout(4,2);
      	radioPanel.setLayout(gl);
      	radioPanel.add(elo);
      	radioPanel.add(win);
      	radioPanel.add(time);
      	radioPanel.add(lose);
      	radioPanel.add(nb);
      	radioPanel.add(draw);
      	radioPanel.add(score);

      	JPanel radioLabelPanel=new JPanel();
      	GridLayout gl2 = new GridLayout(2,1);
      	radioLabelPanel.setLayout(gl2);
      	radioLabelPanel.add(statsLabel);
      	radioLabelPanel.add(radioPanel);

      	JPanel chooseNbPanel=new JPanel();
      	chooseNbPanel.setLayout(new BoxLayout(chooseNbPanel, BoxLayout.PAGE_AXIS));
      	chooseNbPanel.add(profLabel);
      	chooseNbPanel.add(players);

      	add(radioLabelPanel);
      	add(chooseNbPanel);

      	JPanel otherPanel = new JPanel();
      	GridLayout gl3=new GridLayout(1,2);
      	otherPanel.setLayout(gl3);
      	otherPanel.add(two(myMain));
      	otherPanel.add(three(myMain));

      	JPanel buttonsPanel=new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.LINE_AXIS));
      	buttonsPanel.add(rButton);
      	buttonsPanel.add(nButton);

      	JPanel allPanel = new JPanel();
        allPanel.setLayout(new BoxLayout(allPanel, BoxLayout.PAGE_AXIS));
      	allPanel.add(otherPanel);
      	allPanel.add(buttonsPanel);

      	add(allPanel);
    }

    /**
     * @author Gaelle Laperriere
     * Graphic options part 2
     * @param myMain : Reference to the main panel
     */
    public JPanel two(View myMain)
    {
	      JPanel panel=new JPanel();

        p=Profile.loadProfileFromFile();
	      int nbDataTrue=0;

      	boolean [] data=myMain.getDataForGraphic();
      	int nbProf=myMain.getNbProf();
      	int stat=myMain.getDataC();//how many data for the stat
      	int [] profiles=myMain.getProfiles();

      	JComboBox comboStat = new JComboBox();//when there is no data : set it at 0
      	comboStat.addItem("100");
      	comboStat.addItem("50");
      	comboStat.addItem("10");
        comboStat.addItem("All this year");
        comboStat.addItem("All this month");
        comboStat.addItem("All this day");
      	comboStat.setSelectedIndex(stat);
      	JComboBox [] comboProf = new JComboBox[5];//maximum 5 profiles (set to more lisibility)
      	for(int i=0;i<5;i++)//creation of the 5 combobox even if they are empty
      	{
      		comboProf[i]=new JComboBox();
      		for(int j=0;j<Profile.getNbLine();j++)//put all the profiles in it
      		{
      			comboProf[i].addItem(p[j].getName());
      		}
      		if(profiles[i]<Profile.getNbLine())//set the selected item
      		{
      			comboProf[i].setSelectedIndex(profiles[i]);
      		}
      	}

        ////////////////////////////////

	      comboStat.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicATwo(comboProf[0].getSelectedIndex(), comboProf[1].getSelectedIndex(), comboProf[2].getSelectedIndex(), comboProf[3].getSelectedIndex(), comboProf[4].getSelectedIndex(), comboStat.getSelectedIndex());
          		  myMain.showCardByHand(ActionAGraphicOptions.NAME,myMain.getProfile());
          		}
        });

      	for(int i=0;i<5;i++)
      	{
      		comboProf[i].addActionListener(new ActionListener()
      		{
      		    	@Override
      			public void actionPerformed(ActionEvent event)
      			{
      				myMain.setDataForGraphicATwo(comboProf[0].getSelectedIndex(), comboProf[1].getSelectedIndex(), comboProf[2].getSelectedIndex(), comboProf[3].getSelectedIndex(), comboProf[4].getSelectedIndex(), comboStat.getSelectedIndex());
      				myMain.showCardByHand(ActionAGraphicOptions.NAME,myMain.getProfile());
      			}
      		});
      	}

        ////////////////////////////////

      	JPanel b = new JPanel();
        b.setLayout(new GridLayout(4+nbProf,1));
      	if(data[0])
      	{
      		b.add(new JLabel("How many rounds for the ELO data ?"));
      	}
      	else if(data[1])
      	{
      		b.add(new JLabel("How many rounds for the time data ?"));
      	}
      	else if(data[2])
      	{
      		b.add(new JLabel("How many rounds for the number of games played ?"));
      	}
      	else if(data[3])
      	{
      		b.add(new JLabel("How many rounds for the number of victories ?"));
      	}
      	else if(data[4])
      	{
      		b.add(new JLabel("How many rounds for the number of defeats ?"));
      	}
      	else if(data[5])
      	{
      		b.add(new JLabel("How many rounds for the number of draws ?"));
      	}
      	else
      	{
      		b.add(new JLabel("How many rounds for the score data ?"));
      	}
      	b.add(comboStat);

      	b.add(new JLabel("Choose the profile(s) :"));
      	for(int i=0;i<nbProf+1;i++)
      	{
      		b.add(comboProf[i]);
      	}

      	JPanel b2=new JPanel();
      	b2.setLayout(new BoxLayout(b2, BoxLayout.PAGE_AXIS));

      	panel.add(b);
      	panel.add(b2);

      	return panel;
    }

    /**
     * @author Gaelle Laperriere
     * Graphic options part 3
     * @param myMain : Reference to the main panel
     */
    public JPanel three(View myMain)
    {
	      JPanel panel=new JPanel();

	      boolean [] data=myMain.getDataForGraphic();//true if used
      	int stat=myMain.getDataC();//how many data for the stat

      	JComboBox comboStat = new JComboBox();
      	for(int i=0;i<7;i++)//for all data
      	{
      		if(data[i])
      		{
      			comboStat.addItem("All rounds");
      			comboStat.addItem("Mean by games");
      		}
      	}
        myMain.setDataForGraphicAThree(comboStat.getSelectedIndex());

        ////////////////////////////////

	      comboStat.addActionListener(new ActionListener()
        {
            	@Override
          		public void actionPerformed(ActionEvent event)
          		{
          			myMain.setDataForGraphicAThree(comboStat.getSelectedIndex());
          		}
        });

        ////////////////////////////////

	      JPanel b = new JPanel();
        b.setLayout(new GridLayout(2,1));

      	//scale to choose if necessary => no need how many rounds/games registered, put it at 0 like in other games
      	if(data[0])
      	{
      		b.add(new JLabel("Scale for the ELO :"));
      	}
      	else if(data[1])
      	{
      		b.add(new JLabel("Scale for the Time in game :"));
      	}
      	else if(data[2])
      	{
      		b.add(new JLabel("Scale for the Number of Games :"));
      	}
      	else if(data[3])
      	{
      		b.add(new JLabel("Scale for the Number of Victories :"));
      	}
      	else if(data[4])
      	{
      		b.add(new JLabel("Scale for the Number of Defeats :"));
      	}
      	else if(data[5])
      	{
      		b.add(new JLabel("Scale for the Number of Draws :"));
      	}
      	else
      	{
      		b.add(new JLabel("Scale for the Score :"));
      	}
      	b.add(comboStat);

      	JPanel b2=new JPanel();
      	b2.setLayout(new BoxLayout(b2, BoxLayout.PAGE_AXIS));

      	panel.add(b);
      	panel.add(b2);
      	return panel;
    }
}
