package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.JCheckBox;

import engine.agent.Player;
import engine.game.Game;
import stats.Profile;
import stats.Ranking;
import gui.View;
import stats.Replay;


/**
 * @author Gaelle Laperriere
 * @author Thomas Robine
 */
public class ActionReplaying extends JPanel
{
    //class Options

	public static final ImageIcon yellowCard=new ImageIcon("../src/gui/Cards/yellow.jpg");
	public static final ImageIcon purpleCard=new ImageIcon("../src/gui/Cards/purple.jpg");
	public static final ImageIcon blueCard=new ImageIcon("../src/gui/Cards/blue.jpg");
	public static final ImageIcon greenCard=new ImageIcon("../src/gui/Cards/green.jpg");
	public static final ImageIcon redCard=new ImageIcon("../src/gui/Cards/red.jpg");
	public static final ImageIcon unknownCard=new ImageIcon("../src/gui/Cards/unknown.jpg");
	public static final ImageIcon usedCard=new ImageIcon("../src/gui/Cards/empty.jpg");
	public static final ImageIcon emptyCard=new ImageIcon("../src/gui/Cards/index.png");

	
    /**
     * Panel's name
     */
    public static final String NAME = "Replaying";
    
    public boolean flag = false ;
    public Replay replay;
    public Profile[] profiles;
    public int nbPlayers;
    public String[] decks;
    public int nbRound = 0;
	public Profile currentProfile;
	public Profile userProfile;
	public Profile nextProfile;
	public String[] ranks;
	public JPanel players = new JPanel();
	public JPanel cards = new JPanel();
	public JPanel control = new JPanel();
	public JPanel board = new JPanel();
	public JPanel button = new JPanel();
	public String userDeck;
	public String[][] partyBoard;
	public boolean cond = false;
	public String[][] actions;
	public int cpt_pass = 0;
	public int cpt = 0;
	public boolean pass = false;
	public boolean end = false;
	public JCheckBox fast = new JCheckBox("Fast");
	public JButton next = new JButton("Next");
	Profile [] profs ;
	
	@Override
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		int delay = 1 ;//milliseconds
		ActionListener taskPerformer = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				if (flag && !end)
				{
					otherInfos();
					userBoard();
					userCards();
					if (cpt < actions.length) 
					{
						demo(cpt);
						cpt++;
					}
					revalidate();
					repaint();
				}
				else if (flag && end) 
				{
					rankTable();
					flag = false;
					revalidate();
					repaint();
				}
			}
		};
		new javax.swing.Timer(delay, taskPerformer).start();

	}
	
	public void otherInfos() {
		
		JButton[][] playerCards = new JButton[nbPlayers][];
		remove(players);
		players = new JPanel();
		players.setBorder(BorderFactory.createTitledBorder("Game Info"));
		players.setLayout(new BoxLayout( players,BoxLayout.Y_AXIS));
		JPanel tmp = new JPanel(new  FlowLayout(0,0,0));
		Profile p ;
		for ( int i = 0 ; i < nbPlayers ; i++)
		{
			tmp = new JPanel(new  FlowLayout(0,0,0));
			p = profs[i];
			if ( ! p.getUID().equals( userProfile.getUID() ) )
			{
				String cardsS = decks[i+nbRound*nbPlayers];
				playerCards[i] = new JButton[cardsS.length()];
				JLabel pName = new JLabel(p.getName());
				if ( p.getUID().equals( currentProfile.getUID() )) 
				{
					pName.setForeground(Color.orange);
				}

				tmp.add(pName);

				for(int j=0;j<cardsS.length();j++)
				{
					if(cardsS.charAt(j) != ' ')
					{
						playerCards[i][j]=new JButton(unknownCard);
						playerCards[i][j].setMargin(new Insets(0,0,0,0));
						playerCards[i][j].setPreferredSize(new Dimension(8,8));
						tmp.add(playerCards[i][j]);
					}
				}
				tmp.add(new JLabel(" Score : "+cardsS.length()));
				players.add(tmp);
			}
		}
		JLabel roundCount = new JLabel(" Round N° " + (nbRound+1)  + " of " + nbPlayers );
		tmp.add(roundCount);
		players.add(tmp);
		add(players);
	}
	
	public void userCards() 
	{
		
		String cardsS = decks[0+nbRound*nbPlayers];
		ImageIcon [] cardI =new ImageIcon[cardsS.length()];
		JButton[] cardButtons = new JButton[cardsS.length()];
		remove(cards);
		cards = new JPanel();
		cards.setLayout(new BoxLayout( cards,BoxLayout.X_AXIS));
		cards.setBorder(BorderFactory.createEmptyBorder(20, 0, 20, 0));
		JPanel tmp = new JPanel();
		JLabel pName = new JLabel("You : "+profiles[0].getName());
		if ( currentProfile == userProfile ) {
			pName.setForeground(Color.orange);
		}
		tmp.add(pName);

		for(int j=0;j<cardsS.length();j++)
		{
			if(cardsS.charAt(j) != ' ')
			{
				if(cardsS.charAt(j) == 'R')
				{
					cardI[j]=redCard;
					cardButtons[j]=new JButton(cardI[j]);
				}
				else if(cardsS.charAt(j) == 'B')
				{
					cardI[j]=blueCard;
					cardButtons[j]=new JButton(cardI[j]);
				}
				else if(cardsS.charAt(j) == 'Y')
				{
					cardI[j]=yellowCard;
					cardButtons[j]=new JButton(cardI[j]);
				}
				else if(cardsS.charAt(j) == 'P')
				{
					cardI[j]=purpleCard;
					cardButtons[j]=new JButton(cardI[j]);
				}
				else if(cardsS.charAt(j) == 'G')
				{
					cardI[j]=greenCard;
					cardButtons[j]=new JButton(cardI[j]);
				}
				cardButtons[j].setPreferredSize(new Dimension(30,35));
				tmp.add(cardButtons[j]);
			}
		}
		tmp.add(new JLabel(" Score : "+decks[0+nbRound*nbPlayers].length()));
		cards.add(tmp);
		add(cards);
	}
	
	public void initBoard() 
	{
		
		if ( nbPlayers == 2)										//Initialisation du plateau pour 2 joueurs (Pas d'étage 8)
		{
			for (int i = 0 ; i < 8 ; i++ )
			{
				for (int y = 0 ; y < 8 ; y++ )
				{
					if ( i < 7 && y < i+1 )
						partyBoard[i][y]="E";
					else
						partyBoard[i][y]="X";
				}
			}
		}

		else 													//Initialisation du plateau pour les autres nombre de joueurs
		{
			for (int i = 0 ; i < 8 ; i++ )
			{
				for (int y = 0 ; y < 8 ; y++ )
				{
					if ( y < i+1 )
						partyBoard[i][y]="E";
					else
						partyBoard[i][y]="X";
				}
			}
		}
	}
	
	public void userBoard() 
	{
		ImageIcon [][] boardI=new ImageIcon[8][8];
		JButton[][] boardButtons=new JButton[8][8];
		remove(board);
		board = new JPanel();
		board.setLayout(new BoxLayout( board,BoxLayout.Y_AXIS));
		for(int i=0; i<8;i++)
		{
			JPanel tmp = new JPanel();
			for(int j=0;j<8;j++)
			{
				if(partyBoard[i][j].equals("X") == false )
				{
					if(partyBoard[i][j].equals("E"))
					{
						boardI[i][j]=emptyCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
						else if(partyBoard[i][j].equals("R"))
					{
						boardI[i][j]=redCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(partyBoard[i][j].equals("B"))
					{
						boardI[i][j]=blueCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(partyBoard[i][j].equals("Y"))
					{
						boardI[i][j]=yellowCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(partyBoard[i][j].equals("P"))
					{
						boardI[i][j]=purpleCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}
					else if(partyBoard[i][j].equals("G"))
					{
						boardI[i][j]=greenCard;
						boardButtons[i][j]=new JButton(boardI[i][j]);
					}

					boardButtons[i][j].setPreferredSize(new Dimension(36,46));
					tmp.add(boardButtons[i][j]);
				}
			}
			board.add(tmp);
		}
		add(board);
	}

	public boolean insert(int place, String value) 
	{// Pose une carte en fonction des regles du jeu
		
		int cmp = 0;
		int x = 0;
		int y = 0;

		for (int i = 0 ; i < 8 ; i++ )
		{
			for (int j = 0 ; j < 8 ; j++ )
			{
				String pb = partyBoard[i][j];
				if ( pb.charAt(0) != 'X')
				{
					if ( cmp == place)
					{
						x = i;
						y = j;
					}
					cmp += 1;
				}
			}
		}

		if ( partyBoard[x][y] == "E" && ( value.charAt(0) == 'R' || value.charAt(0) == 'G' || value.charAt(0) == 'P' || value.charAt(0) == 'Y' || value.charAt(0) == 'B') )
		{
			if ( nbPlayers == 2 && x == 6 )
			{
				partyBoard[x][y] = value;
				return true;
			}
			else if ( nbPlayers > 2 && x == 7 )
			{
				partyBoard[x][y] = value;
				return true;
			}

			else if (  x < 7 )
			{
				if ( ( partyBoard[x+1][y].charAt(0) == value.charAt(0) && partyBoard[x+1][y+1].charAt(0) != 'E') || ( partyBoard[x+1][y+1].charAt(0) == value.charAt(0) &&  partyBoard[x+1][y].charAt(0) != 'E'  ) )
				{
					partyBoard[x][y] = value;
					return true;
				}
			}
			return false ;
		}
		return false;
	}
	
	public int index() 
	{
		for (int i = 0; i < profiles.length; i++) 
		{
			if (profiles[i] == currentProfile)
			{
				return i;
			}
		}
		return 7;
	}
				
	public void nextOrder() 
	{
		currentProfile = nextProfile;
		for (int i = 0; i < profiles.length; i++) 
		{
			if (profiles[i] == currentProfile) 
			{
				if (i == profiles.length - 1) 
				{
					nextProfile = profiles[0];
				}
				else 
				{
					nextProfile = profiles[i+1];
				}
			}
		}
	}
				
	public void demo(int cpt) 
	{
		String place = actions[cpt][0];
		String value = actions[cpt][1];
		String countdown = actions[cpt][2];
		String id = actions[cpt][3];
		if (!place.equals("Pass")) 
		{
			insert(Integer.parseInt(place),value);
			int ind = index();
			for (int g = 0; g < decks[ind+nbRound*nbPlayers].length(); g++) 
			{
				if (decks[ind+nbRound*nbPlayers].charAt(g) == value.charAt(0)) 
				{
					if(g==0)
					{
						decks[ind+nbRound*nbPlayers] =decks[ind+nbRound*nbPlayers].substring(1,decks[ind+nbRound*nbPlayers].length());
						break;

					}
					else if(g==decks[ind+nbRound*nbPlayers].length())
					{
						decks[ind+nbRound*nbPlayers] = decks[ind+nbRound*nbPlayers].substring(0,g-1);
						break;

					}
					else
					{
						decks[ind+nbRound*nbPlayers] = decks[ind+nbRound*nbPlayers].substring(0,g-1) + decks[ind+nbRound*nbPlayers].substring(g+1,decks[ind+nbRound*nbPlayers].length());
						break;

					}
				}
			}
		}
		else 
		{
			cpt_pass++;
			pass = true;
		}
		if (cpt_pass == nbPlayers && nbRound < nbPlayers -1) 
		{
			if (nbRound < nbPlayers - 1) 
				nbRound++;
			cpt_pass = 0;
			pass = false;
			partyBoard = new String[8][8];
			initBoard();
		}
		else if (cpt_pass == nbPlayers && nbRound == nbPlayers - 1) 
		{
			end = true;
			flag = true;
		}
	}
	
	public void firstOrder() 
	{
		if ( actions != null )
		{
			String id = actions[0][3];
			for (int i = 0; i < profiles.length; i++) 
			{
				if (profiles[i].getUID().equals(id)) 
				{
					currentProfile = profiles[i];
					if (i == profiles.length - 1) 
					{
						nextProfile = profiles[0];
					}
					else 
					{
						nextProfile = profiles[i+1];
					}
				}
			}
		}
	}
		
	public void rankTable() 
	{
		remove(players);
		remove(cards);
		remove(board);
		remove(button);
		JPanel rank = new JPanel();
		String[] label = {"Pseudo", "Player Class", "Score", "Round Score", "Green Cards Played" , "Elo"};
		String[][] tab = new String[ranks.length - 1][6];
		for (int i = 0; i < ranks.length - 1; i++) {
			int cpt = 0;
			for (int j = 0; j < ranks[i].length(); j++) {
				if (ranks[i].charAt(j) == ';') {
					cpt += 1;
					tab[i][cpt-1] = tab[i][cpt-1].substring(4,tab[i][cpt-1].length());
				}
				else if (j > 3) {
					tab[i][cpt] += ranks[i].charAt(j);
				}
			}
		}
		JTable table = new JTable(tab, label);
		JScrollPane scrollPane= new  JScrollPane(table);
		JLabel name ;
		String win = "";
		for (int i = 0; i < ranks[ranks.length - 1].length(); i++) {
			if (i > 1) {
				win += ranks[ranks.length - 1].charAt(i);
			}
		}
		if ( ranks[ranks.length - 1].charAt(0) ==  '1' )
			name =  new JLabel (" Le gagnant est : " + win);
		else
			name =  new JLabel (" Les gagnants sont : " + win);

		rank.add(name);
		rank.add(scrollPane);
		add(rank);

	}
	
	  /**
     * @author Gaelle Laperriere
     * Options's Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionReplaying(View myMain) {
	    setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));
        setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
        ////////////////////////////////
	
        //button to go back to Main Menu
        JButton rButton = new JButton(new GoToAction("Main Menu", MenuView.NAME, myMain));
        fast.setSelected(false);
        fast.addActionListener(new ActionListener() 
        {
            @Override
			public void actionPerformed(ActionEvent event) 
            {
				if (fast.isSelected()) 
				{
					button.remove(next);
					flag = true;
				}
				else {
					button.add(next);
					flag = false;
				}
			}
		});
		next.addActionListener(new ActionListener() 
		{
            @Override
			public void actionPerformed(ActionEvent event) 
            {
				if (cpt < actions.length && !flag) 
				{
					demo(cpt);
					nextOrder();
					otherInfos();
					userBoard();
					userCards();
					cpt++;
					revalidate();
					repaint();
				}
			}
		});
        ////////////////////////////////
		control.add(rButton);
		button.add(fast);
		button.add(next);
		add(control);
		add(button);
		if(myMain.getFileName() !=null)
		{
			flag = false ;
			nbRound = 0;
			cond = false;
			cpt_pass = 0;
			cpt = 0;
			pass = false;
			end = false;
			replay = new Replay(myMain.getFileName());
			profiles = replay.getProfiles();
			profs = profiles.clone();
			nbPlayers = replay.getNbPlayers();
			decks = replay.getDecks();
			userProfile = profiles[0];
			actions = replay.getAllActions();
			firstOrder();
			partyBoard = new String[8][8];
			initBoard();
			ranks = replay.getScores();
			otherInfos();
			userBoard();
			userCards();
			profs = replay.getProfiles();
		}
    }
}
