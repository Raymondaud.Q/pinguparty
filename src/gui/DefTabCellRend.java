package gui;

import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author Gaelle Laperriere
 */
public class DefTabCellRend extends DefaultTableCellRenderer
{
    /**
     * [getTableCellRendererComponent description]
     * @param  tab
     * @param  val
     * @param  selec
     * @param  focus
     * @param  row
     * @param  col  
     * @return the desired component (JPanel)
     */
    @Override
	public Component getTableCellRendererComponent(JTable tab, Object val, boolean selec, boolean focus, int row, int col)
    {
        if(val instanceof JPanel)
        {
            return (JPanel) val;
        }
        else
            return this;
    }
}
