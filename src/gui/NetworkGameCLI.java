package gui;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;

import engine.agent.Player;
import network.Client;
import stats.Ranking;


/**
 * @author Raymondaud Quentin
 */

public class NetworkGameCLI  extends JPanel
{
	//Class NetworkGameCLI
	/**
	 * main View's reference
	 */
	protected View myMain ;

	/**
	 * Client
	 */
	private Client cli;

	/**
	 * Current Player
	 */
	public Player user ;

	public String userUID;

	/**
	 * Board's panel
	 */
	private JPanel board = new JPanel();

	/**
	 * Card's Panel
	 */
	private JPanel cards = new JPanel();

	/**
	 * Other players's Panel
	 */
	private JPanel players = new JPanel();

	/**
	 * Engine's thread
	 */
	private SwingWorker engine;

	/**
	 * Selected card
	 */
	public static String roundAc ="FINISHED";

	/**
	 * Selected place
	 */
	public static String roundPlace  = "FINISHED";

	/**
	 * Refresh inner panel or not
	 */
	public boolean flag = true;

	public static final ImageIcon yellowCard=new ImageIcon("../src/gui/Cards/yellow.jpg");
	public static final ImageIcon purpleCard=new ImageIcon("../src/gui/Cards/purple.jpg");
	public static final ImageIcon blueCard=new ImageIcon("../src/gui/Cards/blue.jpg");
	public static final ImageIcon greenCard=new ImageIcon("../src/gui/Cards/green.jpg");
	public static final ImageIcon redCard=new ImageIcon("../src/gui/Cards/red.jpg");
	public static final ImageIcon unknownCard=new ImageIcon("../src/gui/Cards/unknown.jpg");
	public static final ImageIcon usedCard=new ImageIcon("../src/gui/Cards/empty.jpg");
	public static final ImageIcon emptyCard=new ImageIcon("../src/gui/Cards/index.png");

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		int delay = 5 ;//milliseconds
		ActionListener taskPerformer = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				if (flag  )
				{
					playersInfo();
					userBoard();
					userCards();
					flag=false;
				}
			}
		};
		new javax.swing.Timer(delay, taskPerformer).start();
		repaint();
		revalidate();
	}

	/**
	 * Close connection between cli and server
	 */
	public void close()
	{
		if ( engine != null)
			engine.cancel(true);
		if (cli != null)
			cli.close();
	}


	/**
	 * Engine's thread
	 * Need to relocate
	 */
	public void run()
	{
		NetworkGameCLI t = this;
		engine = new SwingWorker<Void, Void>()
		{

			@Override
			protected Void doInBackground() throws Exception
			{

				while( cli.getCliPlayer() != null && cli.getCliGame() != null  && ! isCancelled() )
				{
					flag=true;
					if ( cli.play(t) != true)
                        break;
				}
				return null ;
			}
			@Override
			protected void done()
			{
				removeAll();
				add(new JButton(new GoToAction("Quit", MenuView.NAME, myMain)));
				rankTable();
			}
		};
		engine.execute();
		System.out.println("Lobby exiting. \n Start Game");
	}


	/**
	 * Cards of current user's Panel
	 */
	public void userCards()
	{
		if (cli.getCliGame() != null )
		{
			
			user = cli.getCliPlayer();
			if ( user != null )
			{
				userUID = user.getProfile().getUID();
				String cardsS = user.getCards();
				ImageIcon [] cardI =new ImageIcon[cardsS.length()];
				JButton[] cardButtons = new JButton[cardsS.length()];
				remove(cards);
				cards = new JPanel();
				cards.setLayout(new BoxLayout( cards,BoxLayout.X_AXIS));
				cards.setBorder(BorderFactory.createEmptyBorder(20, 0, 20, 0));
				JPanel tmp = new JPanel();
				JLabel pName = new JLabel("You : "+user.getPseudo());
				if ( cli.currentPlayerUID != null && user != null && cli.currentPlayerUID.equals(user.getProfile().getUID()) )
					pName.setForeground(Color.orange);
				tmp.add(pName);

				for(int j=0;j<cardsS.length();j++)
				{

					if(cardsS.charAt(j) != ' ')
					{

						if(cardsS.charAt(j) == 'R')
						{
							cardI[j]=redCard;
							cardButtons[j]=new JButton(cardI[j]);
							cardButtons[j].addActionListener(new ActionListener()
							{
								@Override
								public void actionPerformed(ActionEvent e)
								{
									roundAc="R";
									flag=true;
									System.out.println(roundAc);
								}
							} );
						}
						else if(cardsS.charAt(j) == 'B')
						{
							cardI[j]=blueCard;
							cardButtons[j]=new JButton(cardI[j]);
							cardButtons[j].addActionListener(new ActionListener()
							{
								@Override
								public void actionPerformed(ActionEvent e)
								{
									roundAc="B";
									flag=true;
									System.out.println(roundAc);
								}
							} );
						}
						else if(cardsS.charAt(j) == 'Y')
						{
							cardI[j]=yellowCard;
							cardButtons[j]=new JButton(cardI[j]);
							cardButtons[j].addActionListener(new ActionListener()
							{
								@Override
								public void actionPerformed(ActionEvent e)
								{
									roundAc="Y";
									flag=true;
									System.out.println(roundAc);
								}
							} );
						}
						else if(cardsS.charAt(j) == 'P')
						{
							cardI[j]=purpleCard;
							cardButtons[j]=new JButton(cardI[j]);
							cardButtons[j].addActionListener(new ActionListener()
							{
								@Override
								public void actionPerformed(ActionEvent e)
								{
									roundAc="P";
									flag=true;
									System.out.println(roundAc);
								}
							} );

						}
						else if(cardsS.charAt(j) == 'G')
						{
							cardI[j]=greenCard;
							cardButtons[j]=new JButton(cardI[j]);
							cardButtons[j].addActionListener(new ActionListener()
							{
								@Override
								public void actionPerformed(ActionEvent e)
								{
									roundAc="G";
									flag=true;
									System.out.println(roundAc);
								}
							} );

						}

						cardButtons[j].setPreferredSize(new Dimension(30,35));
						tmp.add(cardButtons[j]);
					}


				}
				if ( user.getGR())
				  tmp.add(new JLabel(" Score : "+(user.getScore()+user.getNbC())));
				else
				  tmp.add(new JLabel(" Score : "+(user.getScore())));
				cards.add(tmp);

				add(cards);
			}
		}
	}

	/**
	 * Game's board's panel
	 */
	public void userBoard()
	{
		if ( cli.getCliGame() != null )
		{
			String [][] boardS = cli.getCliGame().getBoard();
			ImageIcon [][] boardI=new ImageIcon[8][8];
			JButton[][] boardButtons=new JButton[8][8];
			remove(board);
			board = new JPanel();
			board.setLayout(new BoxLayout( board,BoxLayout.Y_AXIS));
			int cmp = 0;
			for(int i=0; i<8;i++)
			{
				JPanel tmp = new JPanel();
				for(int j=0;j<8;j++)
				{
					if(boardS[i][j].equals("X") == false )
					{
						if(boardS[i][j].equals("E"))
						{
							final int num = cmp;
							boardI[i][j]=emptyCard;
							boardButtons[i][j]=new JButton(boardI[i][j]);
							if(cli.getCliGame().canPutCard(num,roundAc))
							{
								if(roundAc.equals("R"))
								{
									boardButtons[i][j].setBackground(Color.red);
								}
								else if(roundAc.equals("Y"))
								{
									boardButtons[i][j].setBackground(Color.yellow);
								}
								else if(roundAc.equals("P"))
								{
									boardButtons[i][j].setBackground(Color.pink);
								}
								else  if(roundAc.equals("G"))
								{
									boardButtons[i][j].setBackground(Color.green);
								}
								else if(roundAc.equals("B"))
								{
									boardButtons[i][j].setBackground(Color.cyan);
								}
								else
									boardButtons[i][j].setBackground(Color.orange);
							}

							boardButtons[i][j].addActionListener(new ActionListener()
							{
								@Override
								public void actionPerformed(ActionEvent e)
								{
									roundPlace=""+num;
									System.out.println(roundPlace);
								}
							} );
						}
						else if(boardS[i][j].equals("R"))
						{
							boardI[i][j]=redCard;
							boardButtons[i][j]=new JButton(boardI[i][j]);
						}
						else if(boardS[i][j].equals("B"))
						{
							boardI[i][j]=blueCard;
							boardButtons[i][j]=new JButton(boardI[i][j]);
						}
						else if(boardS[i][j].equals("Y"))
						{
							boardI[i][j]=yellowCard;
							boardButtons[i][j]=new JButton(boardI[i][j]);
						}
						else if(boardS[i][j].equals("P"))
						{
							boardI[i][j]=purpleCard;
							boardButtons[i][j]=new JButton(boardI[i][j]);
						}
						else if(boardS[i][j].equals("G"))
						{
							boardI[i][j]=greenCard;
							boardButtons[i][j]=new JButton(boardI[i][j]);
						}

						boardButtons[i][j].setPreferredSize(new Dimension(36,46));
						tmp.add(boardButtons[i][j]);
						cmp+=1;
					}
				}
				board.add(tmp);
			}
			add(board);
		}
	}

	/**
	 * Other players's Panel
	 */
	public void playersInfo()
	{
		if (cli.getCliGame() != null)
		{
			int nbPlayer =  cli.getCliGame().getNbPlayer();
			user = cli.getCliPlayer();
			if (user  != null )
			{
				userUID = user.getProfile().getUID();
				JButton[][] playerCards = new JButton[nbPlayer][];
				remove(players);
				players = new JPanel();
				players.setBorder(BorderFactory.createTitledBorder("Game Info"));
				players.setLayout(new BoxLayout( players,BoxLayout.Y_AXIS));
				for ( int i = 0 ; i < nbPlayer+1 ; i++)
				{
					JPanel tmp = new JPanel(new  FlowLayout(0,0,0));
					if ( i == nbPlayer )
					{
						int currentRound = cli.getRoundNumber();

						JLabel roundCount = new JLabel(" Round N° " + currentRound  + " of " + nbPlayer );
						if (currentRound == nbPlayer)
							roundCount.setForeground(Color.red);
						tmp.add(roundCount);
						players.add(tmp);
					}
					else if (  i != nbPlayer && ! cli.getCliGame().getPlayer(i,nbPlayer).getProfile().getUID().equals(user.getProfile().getUID()) )
					{

						String cardsS = cli.getCliGame().getPlayer(i,nbPlayer).getCards();
						playerCards[i] = new JButton[cardsS.length()];
						JLabel pName = new JLabel(" "+cli.getCliGame().getPlayer(i,nbPlayer).getPseudo()+" ");
						if (cli.currentPlayerUID != null && cli.currentPlayerUID.equals(cli.getCliGame().getPlayer(i,nbPlayer).getProfile().getUID()) )
							pName.setForeground(Color.orange);
						tmp.add(pName);
						for(int j=0;j<cardsS.length();j++)
						{
							if(cardsS.charAt(j) != ' ')
							{
								playerCards[i][j]=new JButton(unknownCard);
								playerCards[i][j].setMargin(new Insets(0,0,0,0));
								playerCards[i][j].setPreferredSize(new Dimension(8,8));
								tmp.add(playerCards[i][j]);
							}
						}

						if ( cli.getCliGame().getPlayer(i,nbPlayer).getGR() )
						  tmp.add(new JLabel(" Score : "+(cli.getCliGame().getPlayer(i,nbPlayer).getScore()+cli.getCliGame().getPlayer(i,nbPlayer).getNbC())));
						else
						  tmp.add(new JLabel(" Score : "+(cli.getCliGame().getPlayer(i,nbPlayer).getScore())));
						players.add(tmp);
					}
				}
				add(players);
			}
		}
	}

	/**
	 * Rank of game's end's panel
	 */
	public void rankTable()
	{
	
		JPanel rank = new JPanel();
		
		Ranking endRank = cli.endRank;
		if ( endRank != null)
		{
			Object[][] tab = new Object [endRank.getSize()][6];
			String[] label = {"Pseudo", "Player Class", "Score", "Round Score", "Green Cards Played" , "Elo"};
			for ( int i = 0 ; i < endRank.getSize() ; i++ )
			{
				java.util.List<String> rankI = endRank.getPlayerRankI(i).rankToStrAL();
				for ( int j = 0 ; j < rankI.size() ; j++ )
				{
					tab[i][j]= rankI.get(j);
				}
			}
			JTable table = new JTable(tab, label);
			JScrollPane scrollPane= new  JScrollPane(table);

			JLabel name ;
			if ( endRank.nbWinner() == 1 )
				name =  new JLabel (" Le gagnant est : " + endRank.getWinners());
			else
				name =  new JLabel (" Les gagnants sont : " + endRank.getWinners());

			//name.setForeground(Color.orange);
			rank.add(name);
			rank.add(scrollPane);
			add(rank);
		}
	}

	/**
	 * Constructor NetworkGameCLI
	 * @param myMain Main View
	 * @param cli    Client
	 * @param user   Current Player
	 */
	public NetworkGameCLI (View myMain, Client cli )
	{
		this.cli = cli ;
		JPanel button = new JPanel();
		setBorder(BorderFactory.createTitledBorder("Client Pingu Party"));
		JPanel buttonPan = new JPanel();
		buttonPan.setLayout(new BoxLayout( buttonPan,BoxLayout.X_AXIS));
		JButton quit = new JButton(new GoToAction("Quit", MenuView.NAME, myMain));
		quit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if ( engine != null )
				{
					roundAc = roundPlace = "Q";
					engine.cancel(true);
				}
			}
		} );
		buttonPan.add(quit);
		JButton endRound = new JButton("Cannot Put Card");
		endRound.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				roundAc = roundPlace = "D";
			}
		} );
		buttonPan.add(endRound);
		add(buttonPan);
		this.myMain = myMain;
		setLayout(new BoxLayout( this,BoxLayout.Y_AXIS));
		run();
	}
}
