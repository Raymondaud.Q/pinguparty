package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.View;
import engine.agent.AgentFactory;
import engine.agent.Player;
import stats.Profile;

/**
 * @author Gaelle Laperriere
 */
public class ActionNewAI extends JPanel
{
    //class New AI

    /**
     * Panel's name
     */
    public static final String NAME = "New AI";

    /**
     * Profiles's list
     */
    protected Profile[] p;

    /**
     * @author Gaelle Laperriere
     * New AI's Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionNewAI(View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));

        p= Profile.loadProfileFromFile();

        //Avatar

        JLabel indic = new JLabel("Valid classnames are HBasedRobot, CarlosRobot, DumbRobot, Robot, ...");
        
        JTextField t1 = new JTextField("");
        JLabel tl1 = new JLabel("Avatar : ");

        t1.setPreferredSize(new Dimension(300,25));

        JPanel im = new JPanel();
        im.add(tl1);
        im.setLayout(new BoxLayout(im,BoxLayout.LINE_AXIS));
        im.add(t1);
        ////////////////////////////////

        //Name
        JTextField t = new JTextField("");
        JLabel tl = new JLabel("Name : ");
        

        t.setPreferredSize(new Dimension(150,25));

        JPanel u = new JPanel();
        u.setLayout(new BoxLayout(u,BoxLayout.LINE_AXIS));
        u.add(tl);
        u.add(t);

        ////////////////////////////////

        //Class Name
        JTextField cn = new JTextField(" ");
        JLabel cnl = new JLabel("Class Name : ");
        JTextField lvl = new JTextField("Difficulty : 1 to 200");
        cn.setPreferredSize(new Dimension(150,25));

        JPanel cnp = new JPanel();
        cnp.setLayout(new BoxLayout(cnp,BoxLayout.LINE_AXIS));
        cnp.add(cnl);
        cnp.add(cn);
        cnp.add(lvl);

        ////////////////////////////////

        //button to create a new AI
        JButton goButton = new JButton("Create");
        goButton.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                boolean b = false;
                Player test = AgentFactory.testRobot(cn.getText());
                int level = 1;
                if(!Profile.alreadyExistsLocal(t.getText()) && !t.getText().equals("") && test != null && t1.getText().equals(""))
                {
					String className=cn.getText();
					if ( className.indexOf("CarlosRobot") != -1 )
					{
						int tmpLevel = Integer.parseInt(lvl.getText());
						if ( tmpLevel <= 200 && tmpLevel >= 1 )
							level = tmpLevel;
						else
							level = 20;
						
					}
					else
						level = 1;
					
					String name=t.getText();
					Profile.addProfile(name,"",level,1,className);
					String id=Profile.getId(name);
					b=true;
					System.out.println("Profiles");
					myMain.showCardByHand(ActionProfiles.NAME,0);
                }
                else if(!Profile.alreadyExistsLocal(t.getText()) && !t.getText().equals("") && test != null && ! t1.getText().equals(""))
                {
                    String name=t.getText();
                    String className=cn.getText();
                    String image=t1.getText();
                    try
                    {
                        URL url = new URL(image);
                        String fileName ="test.img";
                        int length = 0;

                        InputStream i = url.openStream();
                        OutputStream o = new FileOutputStream(fileName);
                        byte[] buffer = new byte[2048];

                        while ((length = i.read(buffer))!=-1)
                        {
                            o.write(buffer,0,length);
                        }

                        i.close();
                        o.close();

			            File img = new File("test.img");
                        img.delete();

                        Profile.addProfile(name,image,1,1,className);
                        String id=Profile.getId(name);

                        url = new URL(image);
                        fileName ="../src/engine/data/Pictures/"+id+".img";
                        length = 0;

                        i = url.openStream();
                        o = new FileOutputStream(fileName);
                        buffer = new byte[2048];

                        while ((length = i.read(buffer))!=-1)
                        {
                            o.write(buffer,0,length);
                        }

                        i.close();
                        o.close();

                        b=true;

        		        System.out.println("Profiles");
        		        myMain.showCardByHand(ActionProfiles.NAME,0);
                    }
                    catch(Exception e)
                    {
			                  //e.printStackTrace();
                        //System.out.println("Exception "+ e.getMessage());
                        int clickedButton = JOptionPane.showConfirmDialog(null, "Please use a proper url.", "Popup", JOptionPane.DEFAULT_OPTION);
                    }
                }
                else
                {
                    int clickedButton = JOptionPane.showConfirmDialog(null, "Please enter another name or choose a possible class name (ex:Robot).", "Popup", JOptionPane.DEFAULT_OPTION);
                }
            }
        });

        //button to go back to profiles
        JButton rButton = new JButton("Profiles");
	      rButton.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
		            myMain.showCardByHand(ActionProfiles.NAME,0);
	          }
        });

        ////////////////////////////////

        JPanel b1 = new JPanel();
        b1.add(goButton);
        JPanel b2 = new JPanel();
        b2.add(rButton);

        JPanel b = new JPanel();
        b.setLayout(new BoxLayout(b,BoxLayout.LINE_AXIS));
        b.add(b1);
        b.add(b2);

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        p.add(im);
        p.add(u);
        p.add(cnp);
        p.add(b);
        p.add(indic);
        add(p);
        
    }
}
