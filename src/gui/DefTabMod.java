package gui;

import javax.swing.table.AbstractTableModel;

/**
 * @author Gaelle Laperriere
 */
public class DefTabMod extends AbstractTableModel
{
    /**
     * data of the table
     */
    private Object [][] data;

    /**
     * columns of the table
     */
    private String[] title;

    /**
     * Constructor
     * @param data  : Our data
     * @param title : Our columns
     */
    public DefTabMod(Object[][] data, String [] title)
    {
        this.data=data;
        this.title=title;
    }

    @Override
    public int getColumnCount()
    {
      return this.title.length;
    }

    @Override
    public Class getColumnClass(int col)
    {
        return getValueAt(0, col).getClass();
    }

    @Override
    public int getRowCount()
    {
      return this.data.length;
    }

    @Override
    public Object getValueAt(int row, int col)
    {
      return this.data[row][col];
    }

    @Override
    public boolean isCellEditable(int row, int col)
    {
        return false;
    }

    /**
     * Column names
     * @param  col id of the column
     * @return the column's name
     */
    @Override
	public String getColumnName(int col)
    {
        return this.title[col];
    }
}
