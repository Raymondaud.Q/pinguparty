package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

import gui.View;
import stats.Profile;

/**
 * @author Gaelle Laperriere
 */
public class ActionProfiles extends JPanel
{
    //class Profiles

    /**
     * Panel's name
     */
    public static final String NAME = "Profiles";

    /**
     * Profiles's list
     */
    private Profile[] p;

    /**
     * Global panel
     */
    protected JPanel pan;

    /**
     * Profiles's table
     */
    protected JTable table;

    /**
     * Profiles's panel
     */
    protected JPanel l=new JPanel();

    /**
     * @author Gaelle Laperriere
     * Statistics's Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionProfiles(View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));

	     p=Profile.loadProfileFromFile();

        //columns for the profiles's table
        String [] col = {
            "Avatar",
            "Name",
            "Class Name",
            "Local"
        };

        //data for the profiles's table
        Object[][] infos = new Object[Profile.getNbLine()][4];

        for(int i=0;i<Profile.getNbLine();i++)
        {
            String[] infosp=p[i].infoProfile();
            infos[i][1]=infosp[0];
            infos[i][2]=p[i].getClassName();
            if(p[i].getLocal()==1)
            {
                infos[i][3]="yes";
            }
            else
            {
                infos[i][3]="no";
            }

            JPanel image = new JPanel();
      	    ImageIcon icon=new ImageIcon("../src/engine/data/Pictures/"+p[i].getUID()+".img");
      	    JButton bimage = new JButton(icon);
        	  bimage.setPreferredSize(new Dimension(50,50));
      	    image.add(bimage);
            infos[i][0]=image;
        }
        DefTabMod mod= new DefTabMod(infos,col);
        table=new JTable(mod);
        table.setDefaultRenderer(JPanel.class, new DefTabCellRend());

        TableColumn column=null;
        for(int i=0; i<4;i++)
        {
            column=table.getColumnModel().getColumn(i);
            column.setPreferredWidth(100);
        }
        table.setRowHeight(60);

        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setRowSelectionAllowed(true);
        table.setAutoCreateRowSorter(true);

        ////////////////////////////////

        //button to edit a profile
        JButton edit = new JButton("Edit");
        edit.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                int r=table.convertRowIndexToModel(table.getSelectedRow());
                if(p[r].getLocal()==0)
                {
                    int clickedButton = JOptionPane.showConfirmDialog(null, "You can't edit distant profiles", "Popup", JOptionPane.DEFAULT_OPTION);
                }
                else if(r!=-1)
                {
                    System.out.print("Editing "+infos[r][1]);
                    if(p[r].getAI())
                    {
                    	System.out.println(" (AI)"+Integer.toString(r));
                      myMain.showCardByHand(ActionAIEdition.NAME,r);
                			generateTable();
                		  revalidate();
                		  repaint();
                    }
                    else
                    {
                    	System.out.println(" (Player)");
                        myMain.showCardByHand(ActionPlayerEdition.NAME,r);
			                  generateTable();
		                    revalidate();
		                    repaint();
                    }
                }
            }
        });

        //button to delete a profile
        JButton suppr = new JButton("Delete");
        suppr.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                int r=table.convertRowIndexToModel(table.getSelectedRow());
                if(r!=-1)
                {
                    int clickedButton = JOptionPane.showConfirmDialog(null, "Are you sure to delete this profile ?", "Confirmation", JOptionPane.YES_NO_OPTION);
                    if ( clickedButton == JOptionPane.YES_OPTION)
                    {
                        System.out.println("Deleting "+infos[r][1]);
                        File img = new File("../src/engine/data/Pictures/"+p[r].getUID()+".img");
                        img.delete();
                        p[r].deleteProfile(true);
			                  /*generateTable();
            		        revalidate();
            		        repaint();*/
                        myMain.showCardByHand(ActionProfiles.NAME,0);

                    }
                    else
                    {
                        System.out.println("Profiles");
                    }
                }
            }
        });
        
        JButton deleteAll = new JButton("Delete All");
        deleteAll.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                int clickedButton = JOptionPane.showConfirmDialog(null, "Really Nigga ?", " Delete All Profiles ?", JOptionPane.YES_NO_OPTION);
                if ( clickedButton == JOptionPane.YES_OPTION)
                {
                	for (Profile pr : p ) 
                	{
                		File img = new File("../src/engine/data/Pictures/"+pr.getUID()+".img");
                        img.delete();
                        pr.deleteProfile(true);
                	}
                    myMain.showCardByHand(ActionProfiles.NAME,0);
                   
                }
               
            }
        });

        ////////////////////////////////

        //button to add a new player
        JButton addPl = new JButton(new GoToAction("New Player", ActionNewPlayer.NAME, myMain));

        //button to add a new AI
        JButton addAI = new JButton(new GoToAction("New AI", ActionNewAI.NAME, myMain));

        //button to go back to Main Menu
        JButton rButton = new JButton(new GoToAction("Main Menu", MenuView.NAME, myMain));

        ////////////////////////////////

        l.setLayout(new BoxLayout(l, BoxLayout.PAGE_AXIS));
        if(Profile.getNbLine()!=0)
	    {
		    l.add(edit);
        	l.add(suppr);
        	l.add(deleteAll);
	    }
        l.add(addPl);
        l.add(addAI);
        l.add(rButton);

	      pan=new JPanel();
	      pan.add(new JScrollPane(table),BorderLayout.CENTER);
        pan.add(l);

        add(pan);
    }

    /**
     * @author Gaelle Laperriere
     * Generate the profiles's table after a modification
     */
    public void generateTable()
    {
    	removeAll();
  		pan.removeAll();
  		remove(pan);
  		JPanel pan=new JPanel();

  		table.removeAll();
  		remove(table);
  		p=Profile.loadProfileFromFile();

      //columns for the profiles's table
  		String [] col = {
  		    "Avatar",
  		    "Name",
  		    "Class Name",
  		    "Local"
  		};

      //data for the profiles's table
  		Object[][] infos = new Object[Profile.getNbLine()][4];

  		for(int i=0;i<Profile.getNbLine();i++)
  		{
  		    String[] infosp=p[i].infoProfile();
  		    infos[i][1]=infosp[0];
            infos[i][2]=p[i].getClassName();
  		    if(p[i].getLocal()==1)
  		    {
  		        infos[i][3]="yes";
  		    }
  		    else
  		    {
  		        infos[i][3]="no";
  		    }

  		    final int zz=i;

  		    JPanel image = new JPanel()
  		    {
  		        @Override
  		        public void paintComponent(Graphics g)
  		        {
  		            super.paintComponent(g);

  		            ////////////////////////////////////////TOO SLOW =>mettre imageIcon

  		            /*try
  		            {
  		            }
  		            catch (IOException e)
  		            {
  		                //e.printStackTrace();
  		            }*/
  		        }
  		    };
  		    //image.setPreferredSize(new Dimension(20,20));
  		    infos[i][0]=image;
  		}
  		DefTabMod mod= new DefTabMod(infos,col);
  		JTable table=new JTable(mod);
  		table.setDefaultRenderer(JPanel.class, new DefTabCellRend());

  		TableColumn column=null;
  		for(int i=0; i<4;i++)
  		{
  		    column=table.getColumnModel().getColumn(i);
  		    column.setPreferredWidth(100);
  		}
  		for(int i=0; i<Profile.getNbLine();i++)
  		{
  		    table.setRowHeight(i, 40);
  		}

  		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  		table.setRowSelectionAllowed(true);
  		table.setAutoCreateRowSorter(true);

  		pan.add(new JScrollPane(table),BorderLayout.CENTER);
  		pan.add(l);
  		add(pan);
    }
}
