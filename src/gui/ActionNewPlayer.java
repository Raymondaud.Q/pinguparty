package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.View;
import stats.Profile;

/**
 * @author Gaelle Laperriere
 */
public class ActionNewPlayer extends JPanel
{
    //class New Player

    /**
     * Panel's name
     */
    public static final String NAME = "New Player";

    /**
     * Profiles's list
     */
    protected Profile[] p;

    /**
     * @author Gaelle Laperriere
     * New Player's Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionNewPlayer(View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));

        p=Profile.loadProfileFromFile();

        //Avatar
        JTextField t1 = new JTextField("");
        JLabel tl1 = new JLabel("Avatar : ");

        t1.setPreferredSize(new Dimension(300,25));

        JPanel im = new JPanel();
        im.setLayout(new BoxLayout(im,BoxLayout.LINE_AXIS));
        im.add(tl1);
        im.add(t1);

        ////////////////////////////////

        //Name
        JTextField t = new JTextField("");
        JLabel tl = new JLabel("Name : ");

        t.setPreferredSize(new Dimension(150,25));

        JPanel u = new JPanel();
        u.setLayout(new BoxLayout(u,BoxLayout.LINE_AXIS));
        u.add(tl);
        u.add(t);

        ////////////////////////////////

        //button to create the new profile
        JButton goButton = new JButton("Create");
        goButton.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                if(!Profile.alreadyExistsLocal(t.getText()) && !t.getText().equals("") && t1.getText().equals(""))
                {
                    boolean b = false;
                    String name=t.getText();

                    Profile.addProfile(name,"",0,1,"Player");
                    String id=Profile.getId(name);

                    b=true;

                    System.out.println("Profiles");
                    myMain.showCardByHand(ActionProfiles.NAME,0);
                }
                else if(!Profile.alreadyExistsLocal(t.getText()) && !t.getText().equals("") && !t1.getText().equals(""))
                {
                    System.out.println("j"+t1.getText()+"j");
                    boolean b = false;
                    String name=t.getText();
                    String image=t1.getText();
                    //load image
                    try
                    {
                        URL url = new URL(image);
                        String fileName = "test.img";
                        int length = 0;

                        InputStream i = url.openStream();
                        OutputStream o = new FileOutputStream(fileName);
                        byte[] buffer = new byte[2048];

                        while ((length = i.read(buffer))!=-1)
                        {
                            o.write(buffer,0,length);
                        }

                        i.close();
                        o.close();

			            File img = new File("test.img");
                        img.delete();

			            Profile.addProfile(name,image,0,1,"Player");
                        String id=Profile.getId(name);

                        url = new URL(image);
                        fileName = "../src/engine/data/Pictures/"+id+".img";
                        length = 0;

                        i = url.openStream();
                        o = new FileOutputStream(fileName);
                        buffer = new byte[2048];

                        while ((length = i.read(buffer))!=-1)
                        {
                            o.write(buffer,0,length);
                        }

                        i.close();
                        o.close();

                        b=true;

		                System.out.println("Profiles");
		                myMain.showCardByHand(ActionProfiles.NAME,0);
                    }
                    catch(Exception e)
                    {
			                  //e.printStackTrace();
                        //System.out.println("Exception "+ e.getMessage());
                        int clickedButton = JOptionPane.showConfirmDialog(null, "Please use a proper url.", "Popup", JOptionPane.DEFAULT_OPTION);
                    }
                }
                else
                {
                    int clickedButton = JOptionPane.showConfirmDialog(null, "Please enter another name.", "Popup", JOptionPane.DEFAULT_OPTION);
                }
            }
        });

        //button to go baack to profiles
        JButton rButton = new JButton("Profiles");
	      rButton.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
		            myMain.showCardByHand(ActionProfiles.NAME,0);
	          }
        });

        ////////////////////////////////

        JPanel b1 = new JPanel();
        b1.add(goButton);
        JPanel b2 = new JPanel();
        b2.add(rButton);

        JPanel b = new JPanel();
        b.setLayout(new BoxLayout(b,BoxLayout.LINE_AXIS));
        b.add(b1);
        b.add(b2);

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        p.add(im);
        p.add(u);
        p.add(b);
        add(p);
    }
}
