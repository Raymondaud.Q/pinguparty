package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Popup;
import javax.swing.JOptionPane;

import stats.Profile;
import gui.View;

/**
 * @author Gaelle Laperriere
 */
public class ActionBGraphicOptions extends JPanel
{
    //class options for type B graphic

    /**
     * Panel's name
     */
    public static String NAME = "Line Chart Options (B)";

    /**
     * Profiles's List
     */
    private Profile[] p;

    /**
     * @author Gaelle Laperriere
     * Graphic options Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionBGraphicOptions (View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));
        p=Profile.loadProfileFromFile();

        ////////////////////////////////

	//1 profile => n stats

	//player
	JLabel profLabel = new JLabel("Choose the player :  ");
	JComboBox players = new JComboBox();

	for(int i=0;i<Profile.getNbLine();i++)
	{
		Profile profile=p[i];
		players.addItem(profile.getName());
	}
  if(p.length!=0)
  {
    players.setSelectedIndex(myMain.getProfile());
  }

	//stats
	boolean [] data=myMain.getDataForGraphic();
	JLabel statsLabel = new JLabel("Choose the statistics :");
	JCheckBox elo = new JCheckBox("ELO");
	JCheckBox nb = new JCheckBox("Number of Games/Rounds");
	JCheckBox win = new JCheckBox("Number of Victories");
	JCheckBox lose = new JCheckBox("Number of Defeats");
	JCheckBox draw = new JCheckBox("Number of Draws");
	JCheckBox time = new JCheckBox("Time");
	JCheckBox score = new JCheckBox("Score");
	if(data[0])
	{
		elo.setSelected(true);
	}
	if(data[1])
	{
		time.setSelected(true);
	}
	if(data[2])
	{
		nb.setSelected(true);
	}
	if(data[3])
	{
		win.setSelected(true);
	}
	if(data[4])
	{
		lose.setSelected(true);
	}
	if(data[5])
	{
		draw.setSelected(true);
	}
	if(data[6])
	{
		score.setSelected(true);
	}


        ////////////////////////////////

	elo.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setAnTime();
			myMain.setDataForGraphicB(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected());
			myMain.showCardByHand(ActionBGraphicOptions.NAME,players.getSelectedIndex());
		}
        });

	nb.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setAnTime();
			myMain.setDataForGraphicB(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected());
			myMain.showCardByHand(ActionBGraphicOptions.NAME,players.getSelectedIndex());
		}
        });

	win.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setAnTime();
			myMain.setDataForGraphicB(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected());
			myMain.showCardByHand(ActionBGraphicOptions.NAME,players.getSelectedIndex());
		}
        });

	lose.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setAnTime();
			myMain.setDataForGraphicB(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected());
			myMain.showCardByHand(ActionBGraphicOptions.NAME,players.getSelectedIndex());
		}
        });

	draw.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setAnTime();
			myMain.setDataForGraphicB(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected());
			myMain.showCardByHand(ActionBGraphicOptions.NAME,players.getSelectedIndex());
		}
        });

	score.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setAnTime();
			myMain.setDataForGraphicB(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected());
			myMain.showCardByHand(ActionBGraphicOptions.NAME,players.getSelectedIndex());
		}
        });

	time.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setAnTime();
			myMain.setDataForGraphicB(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected());
			myMain.showCardByHand(ActionBGraphicOptions.NAME,players.getSelectedIndex());
		}
        });

	players.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setAnTime();
			myMain.setDataForGraphicB(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected());
			myMain.showCardByHand(ActionBGraphicOptions.NAME,players.getSelectedIndex());
		}
        });


        //button to go back to global stats
        JButton rButton = new JButton(new GoToAction("Cancel", ActionStatistics.NAME, myMain));

        //button to see the graphic
        JButton nButton = new JButton("GO");
	nButton.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
		        if(elo.isSelected() || nb.isSelected() || win.isSelected() || lose.isSelected() || draw.isSelected() || time.isSelected() || score.isSelected())
			{
				myMain.setDataForGraphicB(elo.isSelected(), time.isSelected(), nb.isSelected(), win.isSelected(), lose.isSelected(), draw.isSelected(),score.isSelected());

				myMain.setDataForGraphicBThree(myMain.getDataC2());
				myMain.showCardByHand(ActionGraphic.NAME,players.getSelectedIndex());
			}
			else
			{
				JOptionPane.showConfirmDialog(null, "Please choose at least one statistic.", "", JOptionPane.OK_CANCEL_OPTION);
			}
		}
        });

        ////////////////////////////////

	JPanel radioPanel = new JPanel();
	GridLayout gl = new GridLayout(5,2);
	radioPanel.setLayout(gl);
	radioPanel.add(elo);
	radioPanel.add(win);
	radioPanel.add(time);
	radioPanel.add(lose);
	radioPanel.add(nb);
	radioPanel.add(draw);
	radioPanel.add(score);

	JPanel choosePlayerPanel = new JPanel();
        choosePlayerPanel.setLayout(new BoxLayout(choosePlayerPanel, BoxLayout.LINE_AXIS));
	choosePlayerPanel.add(profLabel);
	choosePlayerPanel.add(players);

	JPanel chooseStatPanel=new JPanel();
	chooseStatPanel.setLayout(new BoxLayout(chooseStatPanel, BoxLayout.LINE_AXIS));
	chooseStatPanel.add(statsLabel);
	chooseStatPanel.add(radioPanel);


	JPanel buttonsPanel=new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.LINE_AXIS));
	buttonsPanel.add(new JLabel(" "));
	buttonsPanel.add(rButton);
	buttonsPanel.add(nButton);

	JPanel allPanel = new JPanel();
        allPanel.setLayout(new BoxLayout(allPanel, BoxLayout.PAGE_AXIS));
	allPanel.add(chooseStatPanel);
	allPanel.add(choosePlayerPanel);
	allPanel.add(two(myMain));
	allPanel.add(three(myMain));
	allPanel.add(buttonsPanel);

	add(allPanel);
    }

    /**
     * @author Gaelle Laperriere
     * Graphic options part 2
     * @param myMain : Reference to the main panel
     */
    public JPanel two(View myMain)
    {
	boolean [] data=myMain.getDataForGraphic();

	JComboBox combo = new JComboBox();
	combo.addItem("100");
	combo.addItem("50");
	combo.addItem("10");
	combo.addItem("All this year");
	combo.addItem("All this month");
	combo.addItem("All this day");
	combo.setSelectedIndex(myMain.getDataC());

        ////////////////////////////////

	combo.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setDataForGraphicBTwo(combo.getSelectedIndex());
		        myMain.showCardByHand(ActionBGraphicOptions.NAME,myMain.getProfile());
		}
        });

        ////////////////////////////////

	JPanel panel=new JPanel();

	if(data[0] || data[1] || data[2] || data[3] || data[4] || data[5] || data[6])
	{
		panel.add(new JLabel("How many rounds do you want to see for the data ?"));
		panel.add(combo);
	}

	return panel;
    }

    /**
     * @author Gaelle Laperriere
     * Graphic options part 3
     * @param myMain : Reference to the main panel
     */
    public JPanel three(View myMain)
    {
	boolean [] data=myMain.getDataForGraphic();
	int time=myMain.getDataC();

	JComboBox combo = new JComboBox();
	combo.addItem("All rounds");
	combo.addItem("Mean by games");
  myMain.setDataForGraphicBThree(combo.getSelectedIndex());

        ////////////////////////////////

	combo.addActionListener(new ActionListener()
        {
            	@Override
		public void actionPerformed(ActionEvent event)
		{
			myMain.setDataForGraphicBThree(combo.getSelectedIndex());
		}
        });

        ////////////////////////////////

	JPanel panel=new JPanel();

	if(data[0] || data[1] || data[2] || data[3] || data[4] || data[5] || data[6])
	{
		panel.add(new JLabel("Choose the scale for the data :"));
		panel.add(combo);
	}

	return panel;
    }

}
