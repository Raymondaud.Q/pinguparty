package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import engine.game.Game;

import javax.swing.JLabel;

import stats.Profile;

/**
* @author Imad Iguichi
* @author Raymondaud Quentin
* @author Thomas Robine
*/

public class ActionLocalGame extends JPanel
{
	/**
	* Panel's name
	*/
	public static final String NAME = "Local Game";
	public static Image background;
	public JLabel runningGame = new JLabel("...");

	/**
	* Panel of profile's selection
	*/
	protected JPanel selection = new JPanel();

	/**
	* Game's Panel
	*/
	protected LocalGame game ;

	/**
	* Local Profile number
	*/
	protected int nbLocalProfile = Profile.nbLocalProfiles();

	/**
	* Local automatic agent Profile number
	*/
	protected int nbLocalIAProfile = Profile.nbLocalIAProfiles();

	/**
	* Game's player number
	*/
	protected int num = 2;

	/**
	* Local profile Array
	*/
	protected Profile [] p = Profile.loadLocalProfileFromFile();

	/**
	* Selected player's profile array
	*/
	protected Profile [] selectedProfiles ;

	/**
	* Local players profileArray
	*/
	protected Profile [] players = new Profile[nbLocalProfile];

	/**
	* Local automatic agent Profile array
	*/
	protected Profile [] computer = new Profile[nbLocalIAProfile];

	/**
	* Profile's selection combobox
	*/
	protected JComboBox [] profiles  = new JComboBox[num];

	/**
	 * Timer for Blitz
	 */
	 protected int tt;
	 protected int timerBlitz;

	@Override
	public void paintComponent(Graphics g)
	{

		super.paintComponent(g);
		g.drawImage(background, 0, 0, this);
		int delay = 2 ;//milliseconds
		ActionListener taskPerformer = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				if ( game != null )
				{
					game.setPreferredSize(getSize());
				}
				runningGame.setText(" Running Game = " + String.valueOf(Game.getNbGame()+1));
				repaint();
				revalidate();
			}
		};
		new javax.swing.Timer(delay, taskPerformer).start();
	}


	/**
	* Selection number of player
	*/
	public void selectionNumber(JButton go)
	{
		String [] choice = new String[0];

		if (( nbLocalProfile - nbLocalIAProfile >= 1 &&  nbLocalIAProfile >= 5 )||  nbLocalIAProfile > 5)
			choice = new String[5];
		else if (  nbLocalIAProfile <= 5 &&  nbLocalProfile >= 2 )
		{
			if ( nbLocalIAProfile >= 2  && nbLocalProfile-nbLocalIAProfile < 1 )  
				choice = new String[nbLocalIAProfile-1] ;
			else
				choice = new String[nbLocalIAProfile] ;
		}

		for ( int i = 0 ; i < choice.length ; i ++ ) 
		{
			choice[i] = Integer.toString(i+2) ; 
		}
		JComboBox nbPlayer = new JComboBox(choice);
		nbPlayer.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				num =  Integer.parseInt((String)nbPlayer.getSelectedItem());
				selectedProfiles = new Profile[num];
				System.out.println("Nombre : " + num );
				lobby(go);
			}
		});
		nbPlayer.setPreferredSize(new Dimension(330,50));
		nbPlayer.setBorder(new OvalBorder());
		add(nbPlayer);
	}

	/**
	* Local lobby panel
	*/
	public void lobby(JButton go)
	{
		
		selection.removeAll();

		String [] namesE = new String[nbLocalIAProfile];
		String [] namesY = new String[nbLocalProfile];

		int you=0;
		int others=0;
		for(int index = 0 ; index < nbLocalProfile ;index++)
		{	
			if ( p[index].getLevel()!=0)
			{
				computer[others] = p[index];
				namesE[others]="Ennemy : " + p[index].getName();
				others++;
			}
			players[you]=p[index];
			namesY[you]="You : " + p[index].getName();
			you++;
		}

		profiles  = new JComboBox[num];
		
		for(int i=0; i < num ;i++)
		{
			Vector vector = new Vector();
			if ( i != 0)
			{
				for ( int j = 0 ; j < nbLocalIAProfile; j ++ )
				{
					vector.addElement(i+" "+namesE[j]);
				}
			}
			else
			{
				for (int j = 0 ; j < nbLocalProfile ; j ++ )
				{
					vector.addElement(i+" "+namesY[j]);
				}
			}
			profiles[i]= new JComboBox (vector);
			profiles[i].setPreferredSize(new Dimension(200,50));
			if ( i != 0 )
				profiles[i].setSelectedIndex(i-1);
			else
				profiles[i].setSelectedIndex(i);

			profiles[i].setPreferredSize(new Dimension(200,40));
			profiles[i].setBorder(new OvalBorder());

			profiles[i].addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					JComboBox tmp = (JComboBox) e.getSource();
					int numP = tmp.getSelectedIndex();
					System.out.println( p[numP].getName() + " selectionné !\n");
					getProfileMeta(profiles,players,computer);
					remove(go);
					if ( getProfileMeta(profiles,players,computer) )
						add(go);
					else
						JOptionPane.showConfirmDialog(null," You cannot create the game ! \n There are two identical profiles. "," Impossible ProfileMeta ",JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE);
				}
			});
			selection.add(profiles[i]);
		}
		if ( getProfileMeta(profiles,players,computer) )
			add(go);
		else
			JOptionPane.showConfirmDialog(null," You cannot create the game ! \n There are two identical profiles. "," Impossible ProfileMeta ",JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE);
				
	}

	/**
	* Get Selected profiles
	* @param profiles  JComboBox array profiles
	* @param players	Profile array
	* @param computer  Profile array
	*/
	public boolean getProfileMeta(JComboBox [] profiles, Profile[] players , Profile[] computer)
	{
		for(int i=0;i<num;i++)
		{
			if ( i == 0)
			  selectedProfiles[i] = players[profiles[i].getSelectedIndex()];
			else
			  selectedProfiles[i] = computer[profiles[i].getSelectedIndex()];
			System.out.println(selectedProfiles[i].getName());
		}

		for ( int j = 0 ; j < selectedProfiles.length ; j ++)
		{
			for ( int k = 0 ; k < selectedProfiles.length ; k ++)
			{
				if ( j != k && selectedProfiles[j] == selectedProfiles[k] )
				{
					System.out.println("C'est la fin : " + selectedProfiles[j].toString() + j + " VS  "+ k + selectedProfiles[k].toString() );
					return false;
				}
			}
		}

		return true;
	}

	/**
	* Action local game constructor
	* @param myMain Main view
	*/
	public ActionLocalGame(View myMain)
	{
		try
       	{
       		background = ImageIO.read(new File("../src/gui/Background/B1.jpg"));
       	}
       	catch ( Exception e)
       	{;}
		LocalGame.startAll = false;
		
	
		setLayout(new FlowLayout(FlowLayout.CENTER, 3000, 10));
		setName(NAME);
		setBorder(BorderFactory.createTitledBorder(NAME));
		JButton main  =new JButton(new GoToAction("Main Menu", MenuView.NAME, myMain));
		main.setPreferredSize(new Dimension(330,50));
		main.setBorder(new OvalBorder());
		add(main);
		
		JLabel instruc = new JLabel("Repeat");
		JTextField nb = new JTextField("1");
		JLabel instrucEnd = new JLabel("time(s)");
		add(instruc);
		add(nb);
		add(instrucEnd);
		
		JLabel annonce = new JLabel("Veuillez sélectionner une limite de temps si vous voulez jouer en mode Blitz");
		add(annonce);
		String[] times = {"Pas de limite de temps","5 secondes","10 secondes","15 secondes","20 secondes","25 secondes","30 secondes"};
		JComboBox time = new JComboBox(times);
		time.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e)
				{
					tt = time.getSelectedIndex();
					System.out.println(tt);
				}
		});	
		add(time);
		JButton go = new JButton("Play");
		go.setPreferredSize(new Dimension(200,40));
		go.setBorder(new OvalBorder());
		go.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if ( selectedProfiles != null && getProfileMeta(profiles,players,computer) )
				{
					if ( nbLocalProfile >=  num )
					{
						int n=1;
						removeAll();
						try
						{
							n = Integer.parseInt(nb.getText());
							if (n != 1)
								add(runningGame);
						}
						catch ( Exception ee)
						{}
						
						
							
						int i = 0;
						while ( i  < n )
						{
							if (tt == 1) {
								timerBlitz = 5;
							}
							if (tt == 2) {
								timerBlitz = 10;
							}
							if (tt == 3) {
								timerBlitz = 15;
							}
							if (tt == 4) {
								timerBlitz = 20;
							}
							if (tt == 5) {
								timerBlitz = 25;
							}
							if (tt == 6 ) {
								timerBlitz = 30;
							}
							game = new LocalGame(myMain,selectedProfiles,num,timerBlitz);
							add(game);
							repaint();
							revalidate();
							i+=1;
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
						LocalGame.startAll = true;
					}
				}
				else 
				{
					JOptionPane.showConfirmDialog(null," This action could not succeed. There are two identical profiles. "," Impossible ProfileMeta ",JOptionPane.DEFAULT_OPTION,JOptionPane.PLAIN_MESSAGE);
				}
			}
		});
		selection.setLayout(new BoxLayout(selection, BoxLayout.Y_AXIS));
		selectionNumber(go);
		add(selection);
	}
}
