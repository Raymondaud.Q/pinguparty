package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import stats.Profile;

/**
 * @author Gaelle Laperriere
 */
public class ActionImage extends JPanel
{
    //class Image

    /**
     * Panel's name
     */
    public static final String NAME = "Image";

    /**
     * User's id
     */
    protected String id;

    /**
     * User's name
     */
    protected String name;

    /**
     * User's level, 0 if not an AI
     */
    protected int level;

    /**
     * User className
     */
    protected String className;

    /**
     * Old user's avatar
     */
    protected String url;

    /**
     * Profile edited
     */
    protected boolean bool=false;

    /**
     * @author Gaelle Laperriere
     * Image's Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionImage(View myMain)
    {
        setName(NAME);
        setBorder(BorderFactory.createTitledBorder(NAME));
      	Profile[] profiles=Profile.loadProfileFromFile();
      	int indice=myMain.getProfile();
      	if ( indice <= profiles.length && profiles.length != indice )
      	{
          	this.id=profiles[indice].getUID();
          	this.name=profiles[indice].getName();
          	this.level=profiles[indice].getLevel();
          	this.url=profiles[indice].getUrl();
          	this.className=profiles[indice].getClassName();
      	}

        //Avatar
      	JTextField t = new JTextField(this.url);
        JLabel tl = new JLabel("Avatar : ");

        t.setPreferredSize(new Dimension(300,25));

        JPanel u = new JPanel();
        u.setLayout(new BoxLayout(u,BoxLayout.LINE_AXIS));
        u.add(tl);
        u.add(t);

        ////////////////////////////////

        //button to confirm
        JButton goButton = new JButton("Confirm Modifications");
        goButton.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                if(t.getText()!=null)
                {
                    String image=t.getText();
                    boolean b=false;
                    //load image
                    try
                    {
                        URL url = new URL(image);
                        String fileName = "../src/engine/data/Pictures/"+id+".img";
                        int length = 0;

                        InputStream i = url.openStream();
                        OutputStream o = new FileOutputStream(fileName, bool);
                        byte[] buffer = new byte[2048];

                        while ((length = i.read(buffer))!=-1)
                        {
                            o.write(buffer,0,length);
                        }

                        i.close();
                        o.close();

                        Profile.editProfile(id,name,image,level,className);

                        b=true;
            		        System.out.println("Profile");

                  			if(className.equals("Player"))
                  			{
                  			    myMain.showCardByHand(ActionPlayerEdition.NAME,indice);
                  			}
                  			else
                  			{
                  			    myMain.showCardByHand(ActionAIEdition.NAME,indice);
                  			}
                    }
                    catch(Exception e)
                    {
			                  //e.printStackTrace();
                        //System.out.println("Exception "+ e.getMessage());
                        int clickedButton = JOptionPane.showConfirmDialog(null, "Please use a proper url.", "Popup", JOptionPane.DEFAULT_OPTION);

                    }
                }
            }
        });

        //button to go back to Main Menu
        JButton rButton = new JButton("Main Edition");
	      rButton.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
            		if(className.equals("Player"))
            		{
            		    myMain.showCardByHand(ActionPlayerEdition.NAME,indice);
            		}
            		else
            		{
            		    myMain.showCardByHand(ActionAIEdition.NAME,indice);
            		}
	          }
        });

        ////////////////////////////////

        JPanel b1 = new JPanel();
        b1.add(goButton);
        JPanel b2 = new JPanel();
        b2.add(rButton);

        JPanel b = new JPanel();
        b.setLayout(new BoxLayout(b,BoxLayout.LINE_AXIS));
        b.add(b1);
        b.add(b2);

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        p.add(u);
        p.add(b);
        add(p);
    }
}
