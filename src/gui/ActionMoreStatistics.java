package gui;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import stats.Profile;
import gui.View;

/**
 * @author Gaelle Laperriere
 */
public class ActionMoreStatistics extends JPanel
{
    //class More Stats about a user

    /**
     * Panel's name
     */
    public static String NAME = "More Statistics";

    /**
     * Profiles's List
     */
    private Profile[] p;

    /**
     * @author Gaelle Laperriere
     * More Stats Constructor
     * @param myMain : Reference to the main panel
     */
    public ActionMoreStatistics (View myMain)
    {
        setName(NAME);
        p=Profile.loadProfileFromFile();
	    int indice=myMain.getProfile();

        JLabel label2;
        JTextField statFields;

        JPanel panGen = new JPanel();
        if ( indice <= p.length && p.length != indice )
        {
    	    String name=p[indice].getName();
            setBorder(BorderFactory.createTitledBorder(NAME+" of "+name));
            GridLayout gl = new GridLayout(10,2);
            panGen.setLayout(gl);
    
            //columns
            String[] title =
            {
                "Games Played",
                "Games Won",
                "Games Lost",
                "Games Draw",
                "Games Left",
                "Points",
                "ELO",
    	          "Med Time",
                "Time"
            };
    
            //data
    	      String [] ss = new String[9];
            String [] ssinit = p[indice].statsProfile();
    	      int t=Integer.parseInt(ssinit[7]);
    
            for ( int j = 0 ; j < 9 ; j ++)
            {
        	    if(j<8)
        	    {
        		  ss[j]=ssinit[j];
        	    }
              label2 = new JLabel(title[j]);
              if(j<5 && j>0)
              {
                  int v=Integer.parseInt(ss[j]);
                  if(v!=0)
                  {
                      ss[j]=ss[j]+" : "+Integer.toString((v*100)/Integer.parseInt(ss[0]))+"%";
                  }
                  else
                  {
                      ss[j]="0 : 0%";
                  }
              }
        	    else if(j==7)
        	    {
                    if(Integer.parseInt(ss[0])!=0)
                    {
                        int sev=t/Integer.parseInt(ss[0]);
                        ss[7]=Integer.toString(sev/(60*60))+"h"+Integer.toString(sev/60)+"m"+Integer.toString(sev%60)+"s";
                    }
                    else
                    {
                        ss[7]="0h0m0s";
                    }
            	}
        	    else if(j==8)
        	    {
        		    ss[8 ]=Integer.toString(t/(60*60))+"h"+Integer.toString(t/60)+"m"+Integer.toString(t%60)+"s";
        	    }
                statFields = new JTextField(ss[j]);
                statFields.setPreferredSize(new Dimension( 200, 24 ));
                statFields.setEditable(false);
                panGen.add(label2);
                panGen.add(statFields);
            }
        }

        ////////////////////////////////

        //button to go back to global stats
        JButton rButton = new JButton(new GoToAction("Main Statistics", ActionStatistics.NAME, myMain));

        ////////////////////////////////

        JPanel b = new JPanel();
        b.setLayout(new BoxLayout(b,BoxLayout.PAGE_AXIS));
        b.add(panGen);
        b.add(rButton);
        add(b);
    }
}
