package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import engine.agent.Player;
import network.Client;
import stats.Profile;

public class ActionJoinGame extends JPanel
{
	public static final String NAME = "Join Network Game";

	/**
	* Main view
	*/
	private View myMain;

	/**
	* Profile's selection's panel
	*/
	protected JPanel selection = new JPanel();

	/**
	* Lobby's Panel
	*/
	protected JPanel lobbyNet = new JPanel(); // Contains futur players of the game

	/**
	*
	*/
	protected JPanel player = new JPanel();

	/**
	* List of the profiles that can be selected
	*/
	protected java.util.List<Profile> prof ;

	/**
	* Selected profile
	*/
	public static Profile profile ;

	/**
	* Current player
	*/
	Player user ;

	/**
	* Client
	*/
	Client cli;

	/**
	* Lobby's thread
	*/
	SwingWorker lobbyGUI;

	/**
	* NetworkGameCLI's Panel
	*/
	NetworkGameCLI game;

	/**
	* Profile's selection's JComboBox
	*/
	JComboBox profiles;

	public static Image background;


	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(background, 0, 0, this);
		int delay = 5 ;//milliseconds
		ActionListener taskPerformer = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				if ( game != null )
				{
					game.setPreferredSize(game.getPreferredSize());
				}

			}
		};
		new javax.swing.Timer(delay, taskPerformer).start();
		repaint();
		revalidate();
	}

	/**
	* Return a JCombox of Profile
	* @return JCombox
	*/
	public JComboBox profile()
	{
		int nbLocalProfile = Profile.nbLocalProfiles();
		Profile [] p = Profile.loadLocalProfileFromFile();
		String [] names = new String[nbLocalProfile];
		String [] ids = new String[nbLocalProfile];
		int [] indices = new int[nbLocalProfile];
		
		for(int i=0; i < nbLocalProfile ; i++)
		{
			names[i]=p[i].getName();
			ids[i]=ids[i];
			indices[i]=i;
		}


		profiles = new JComboBox(names);
		if (nbLocalProfile > 0)
		{
			profiles.setPreferredSize(new Dimension(100,20));
			profiles.addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					int index = profiles.getSelectedIndex();
					profile = p[index];
				}
			});
			int index = profiles.getSelectedIndex();
			profile = p[index];
			profiles.setPreferredSize(new Dimension(200,40));
		}
		return profiles;
	}

	/**
	* Close the connection and kill the lobby's Thread
	*/
	public void close()
	{
		if ( game != null )
			game.close();
		if ( lobbyGUI != null )
			lobbyGUI.cancel(true);
	}

	/**
	* Lobby's Thread
	*/
	public void run()
	{
		lobbyGUI = new SwingWorker<Void, Void>()
		{

			@Override
			protected Void doInBackground() throws Exception
			{
				try
				{
					while ( cli.lobbyGUI() && ! isCancelled() )
					{
						prof = cli.getProfiles();
						generateList();
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				return null;
			}
			@Override
			public void done()
			{
				cancel(true);
				game = new NetworkGameCLI(myMain,cli);
				removeAll();
				add(game);
				revalidate();
				repaint();
			}
		};
		lobbyGUI.execute();
		System.out.println("Lobby exited");
	}

	/**
	* Generate a panel that contains all the players of the distant game
	*/
	public void generateList()
	{
		lobbyNet.setBorder(BorderFactory.createTitledBorder("Player List"));
		player.removeAll();
		lobbyNet.removeAll();
		remove(lobbyNet);
		remove(player);
		JPanel player = new JPanel();
		player.setLayout(new BoxLayout(player, BoxLayout.Y_AXIS));
		player.removeAll();
		for ( int j = 0 ; j < prof.size() ; j ++ )
		{
			JPanel playerI = new JPanel();
			playerI.setBorder(new OvalBorder());
			playerI.setMaximumSize(new Dimension(300,50));
			if ( profile.getUID().equals(prof.get(j).getUID()) )
				playerI.add(new JLabel(" You : " + prof.get(j).getName()));
			else
			{
				playerI.add(new JLabel(" Ennemy : " + prof.get(j).getName()));
			}
			player.add(playerI);
		}
		lobbyNet.add(player);
		add(lobbyNet);
	}

	/**
	* Setlayout Y Axis on current panel
	*/
	public void setLayout()
	{
		setLayout(new BoxLayout( this,BoxLayout.Y_AXIS));
	}


	/**
	* Create an ActionJoinGame's Panel
	* @param myMain Main View
	*/
	public ActionJoinGame(View myMain)
	{
		super(new FlowLayout(FlowLayout.CENTER, 3000, 30));
		try
       	{
       		background = ImageIO.read(new File("../src/gui/Background/B1.jpg"));
       	}
       	catch ( Exception e)
       	{;}
		JPanel button = new JPanel();
		setName(NAME);
		setBorder(BorderFactory.createTitledBorder(NAME));
		JButton main =new JButton(new GoToAction("Main Menu", MenuView.NAME, myMain));
		main.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (cli != null)
					cli.close();
				if (lobbyGUI != null )
					lobbyGUI.cancel(true);
				if ( game != null )
					game.close();
			}
		});
		main.setPreferredSize(new Dimension(200,40));
		main.setBorder(new OvalBorder());
		add(main);

		this.myMain = myMain;
		Font fieldFont = new Font("Arial", Font.PLAIN, 20);
		JTextField textField = new JTextField("192.168.1...");
		textField.setFont(fieldFont);
		textField.setBackground(Color.white);
		textField.setForeground(Color.gray.brighter());
		textField.setColumns(10);
		textField.setBorder(new OvalBorder());

		JButton create = new JButton("Join Room");
		create.setPreferredSize(new Dimension(200,40));
		create.setBorder(new OvalBorder());
		create.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String ip = textField.getText();
				cli = new Client(ip,profile);
				run();
				remove(textField);
				remove(create);
				remove(profiles);
				setLayout();
			}
		});
		add(profile());
		add(textField);
		add(create);
	}
}
