package stats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import network.dataobjects.Result;
import network.dataobjects.StatData;

/**
 * @author Raymondaud Quentin
 */
public class PlayerRank implements Serializable
{
	private String pseudo;
	private String playerClass;
	private int score;
	private int nbWinRound;
	private int greenCardsPlayed;
	private double ELO;

	/**
	 * @param ps String pseudo
	 * @param plC String player ClassName
	 * @param sc Int Score
	 * @param nbWR Int win round number
	 * @param gCP Int Green cards played number
	 * @param elo ELO
	 */
	public PlayerRank(String ps , String plC , int sc , int nbWR , int gCP, double elo)
	{
		pseudo = ps;
		playerClass = plC;
		score = sc;
		nbWinRound = nbWR;
		greenCardsPlayed = gCP;
		ELO = elo;
	}
	
	public PlayerRank( Result r, StatData s, String name)
	{
		pseudo = name;
		playerClass = "RemotePlayer";
		score = r.score ;
		nbWinRound = -1 ; //NULL
		greenCardsPlayed = r.greenCardsPlayed ;
		ELO = s.elo;
	}

	public String getPseudo()
	{
		return pseudo;
	}

	public String getPlayerClass()
	{
		return playerClass;
	}

	public String getScore()
	{
		return Integer.toString(score);
	}

	public String getNbWinRound()
	{
		return Integer.toString(nbWinRound);
	}

	public String getGreenCardsPlayed()
	{
		return 	Integer.toString(greenCardsPlayed);
	}
	
	public double getElo()
	{
		return ELO;
	}

	@Override
	public String toString()
	{
		return (" | Pseudo : "+pseudo+" | Score : "+score+ " | roundScore : " + nbWinRound +" | Green Cards Played : "+ greenCardsPlayed + " | " + playerClass + " | " );
	}
	
	/**
	 * @return String that contains PlayerRank informations
	 */
	public String rankToString()
	{
		return (" | Pseudo : "+pseudo+" | Score : "+score+ " | roundScore : " + nbWinRound +" | Green Cards Played : "+ greenCardsPlayed + " | " + playerClass + " | " );
	}


	/**
	 * @return List<String> String PlayerRank
	 */
	public List<String> rankToStrAL()
	{
		List<String> p = new ArrayList<String>();
		p.add(0,pseudo);
		p.add(1,playerClass);
		p.add(2,Integer.toString(score));
		p.add(3,Integer.toString(nbWinRound));
		p.add(4,Integer.toString(greenCardsPlayed));
		p.add(5,Double.toString(ELO));

		return p;
	}
}