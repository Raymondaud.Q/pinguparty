package stats;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URL;
import java.util.UUID;

import network.dataobjects.ProfileData;
import network.dataobjects.ProfileStatData;
import network.dataobjects.StatData;


/**
 * @author Thomas Robine
 * @author Quentin Raymondaud
 */
public class Profile extends LocalReadWriter implements Serializable
{
	String profileUID ;				// ID unique du profil String suuid = UUID.randomUUID().toString();
	String name ;					// pseudo du profil
	String picUrl ;					// Lien de la photo du profil
	int level ;  					// 0 = joueur , 1,2,3 = Niveau de l'IA
	int local ;						// 0 = Distant, 1 = Local
	Stat stats ;					// Statisques du profil "profileUID"
	String className ;				// Nom de la classe définissant le comportement du futur joueur 


	private static String path = "../src/engine/data/prof.txt" ;

	
	public Profile()
	{
		profileUID=name=picUrl="";
		level=local=0;
	}


	/**
	 * Create a profile without a specific ID
	 * @param name Str
	 * @param url Str
	 * @param lvl int
	 * @param local int
	 * @param cN Str
	 */
	public Profile( String name, String url, int lvl, int local , String  cN)
	{
		profileUID = UUID.randomUUID().toString();
		this.name = name ;
		picUrl = url ;
		level = lvl ;
		this.local = local;
		className = cN;
		stats = new Stat(profileUID);
		stats.insertStat();
		insertProfile();

	}

	/**
	 * Create a Profile with a specific id
	 * @param id Str
	 * @param name Str
	 * @param url Str
	 * @param lvl int
	 * @param local int
	 * @param cN Str
	 */
	public Profile( String id, String name, String url, int lvl, int local , String cN)
	{
		profileUID = id;
		this.name = name ;
		picUrl = url ;
		level = lvl ;
		this.local = local;
		className = cN;
		stats = new Stat();
		stats = stats.loadStat(profileUID);
	}
	
	public Profile ( ProfileStatData newbie )
	{
		profileUID = newbie.profile.id;
		name = newbie.profile.nickname;
		picUrl = "";
		level = 0 ;//newbie.profile.isHuman;
		local = 0 ;
		className = "Player";
		stats = new Stat(newbie.stat);
		
	}
	
	public ProfileStatData toProfileStatData()
	{
		ProfileData prof = new ProfileData(profileUID, true, true, name , picUrl);
		StatData stat = new StatData( profileUID, stats.win, stats.lost, 0, stats.scoreTotal, stats.time, stats.elo);
		return new ProfileStatData(prof,stat);
	}

	public static int getNbLine()
	{
		return getNbLine(path);
	}

	public String getName()
	{
		return name;
	}
	public void setLocal(int set)
	{
		local = set;
	}

	public String getUID()
	{
		return profileUID;
	}

	public String getUrl()
	{
		return picUrl;
	}

	public int getLocal()
	{
		return local;
	}

	public int getLevel()
	{
		return level;
	}

	public Stat getStat()
	{
		return stats;
	}

	public String getClassName()
	{
		return className;
	}

	public void setClassName(String cn)
	{
		className = cn;
	}
	
	static public Profile addProfile( String name, String url, int lvl, int local, String cN)	// Crée et insere un profil local 
	{
		Profile p = new Profile(name,url,lvl,local,cN);
		return p;
	}

	public boolean insertProfile()															// Insere un profil
	{
		PrintWriter fileToWrite ;
		try
		{
			fileToWrite =  new PrintWriter(new BufferedWriter(new FileWriter(path,true)));
			fileToWrite.append(profileUID+";"+name.replaceAll(";","")+";"+picUrl+";"+level+";"+local+";"+className+";\n");
			fileToWrite.close();
			return true;
		}
		catch (NullPointerException a)
		{
			System.out.println("Erreur : pointeur null");
			return false;
		}
		catch (IOException a)
		{
			a.printStackTrace();
			System.out.println("Problème d'IO");
			return false;
		}
	}

	public static void editProfile( String id, String name, String url, int lvl, String cN)		// Edit un profil local
	{
		Profile p = new Profile(id,name,url,lvl,1,cN);
		p.editProfile();

	}

	public void editProfile()
	{
		deleteProfile(false);
		insertProfile();
		stats.insertStat();
	}

	public boolean deleteProfile(boolean deleteTMP)															// Suppression d'un profil
	{
		if ( stats != null)
			stats.deleteStat();
		if (deleteTMP)
			TempStat.delete(profileUID);
		return eraseID(profileUID,path);
	}



	public Profile loadProfile (String id)														// Chargement d'un profil
	{
		id =  id.replaceAll(";","");
		return loadProfile(id,path);
	}



	public boolean getAI()  														// return true si IA
	{
		if ( level != 0)
			return true;
		else 
			return false;
	}

	static public boolean getLocal( String id ) 										// return true si local
	{
		Profile p = new Profile();
		p.loadProfile(id);
		if ( p.getLocal() > 0)
			return true;
		else 
			return false ;
	}


	/**
	 * @param Profile Name
	 * @return True if exists already false otherwise
	 */
	public static boolean alreadyExistsLocal ( String name )  										// return true si existe déjà
	{
		String id = Profile.getId(name);
		return alreadyExists(id,path);
	}


	/**
	 * @param prof Profile Array 
	 * @param nbProfile Array max index
	 * @return String GUID Array
	 */
	public static String [] getProfileID(Profile [] prof, int nbProfile) 				// return ids
	{
		String [] pUID = new String [nbProfile];
		for (int i = 0 ; i < nbProfile ; i ++)
		{
			pUID[i] = prof[i].getUID();
		}
		return pUID;
	}

	/**
	 * @param prof Profile Array 
	 * @param nbProfile Array max index
	 * @return String Name Array
	 */
	public static String [] getNames(Profile [] prof, int nbProfile) 							// return Noms
	{
		String [] names = new String [nbProfile];
		for (int i = 0 ; i < nbProfile ; i ++)
		{
			names[i] = prof[i].getName();
		}
		return names;
	}

	/**
	 * @return String Profile to string Array
	 */
	public String [] infoProfile()  												// return (nom, url, lvl)
	{
		String [] info = new String[4];
		info[0]=name;
		info[1]=picUrl;
		info[2]=String.valueOf(level);
		info[3]=className;
		return info;
	}

	
	public static String [][] getStrStats () // return id nom elo total des points
	{
		return getStrStats(path);
	}

	public String [] statsProfile() 											// return (ELO, Nb parties jouées, gagnées, nulles, perdues, total pts marqués, temps joué)
	{
		if ( stats != null)
			return stats.getStrStat();
		else
		{
			deleteProfile(true);
			return null;
		}
	}

	
	/**
	 * @param name String 
	 * @return ID of local profile called Name
	 */
	public static String getId(String name)
	{
		String id = getId(name,path,true);
		return id;
	}

	
	/**
	 * @param name String
	 * @return ID of remoted profile called name
	 */
	public static String getIdDist(String name)
	{
		String id = getId(name,path,false);
		return id;
	}



	/**
	 * @return Integer : number of local profiles
	 */
	public static int nbLocalProfiles()														// Retourne le nombre de profils locaux
	{
		Profile [] p = loadProfileFromFile(path);
		int nbLocal = 0;
		for ( int index = 0; index < p.length ; index ++ )
		{
			if (p[index].getLocal() == 1)
				nbLocal +=1;
		}
		return nbLocal;
	}
	
	/**
	 * @return Integer : number of local IA profiles
	 */
	public static int nbLocalIAProfiles()														// Retourne le nombre de profils locaux
	{
		Profile [] p = loadProfileFromFile();
		int nbLocalIA = 0;
		for ( int index = 0; index < p.length ; index ++ )
		{
			if (p[index].getLocal() == 1 && p[index].getLevel() != 0 )
				nbLocalIA +=1;
		}
		return nbLocalIA;
	}

	/**
	 * @return All Profile Array
	 */
	public static Profile[] loadProfileFromFile()
	{
		return loadProfileFromFile(path);
	}

	/**
	 * @return Local Profile Array
	 */
	public static Profile[] loadLocalProfileFromFile()
	{
		Profile [] p = loadProfileFromFile(path);
		int nb  = nbLocalProfiles();

		Profile [] localProfiles = new Profile[nb];

		int j = 0 ;
		for ( int index = 0; index < p.length ; index ++ )
		{
			if (p[index].getLocal() == 1)
			{
				localProfiles[j]=p[index];
				j+=1;
			}
		}
		return localProfiles;
	}

	/**
	 *  Display a profile and Stats
	 */
	public void printProf()
	{
		String [] s = stats.getStrStat();
		System.out.println(profileUID+"|"+name+"|"+picUrl+"|"+level+"|"+local+"|"+className+s[0]+"|"+s[1]+"|"+s[2]+"|"+s[3]+"|"+s[4]);
	}

	@Override
	public String toString()
	{
		//String [] s = stats.getStrStat();
		//return (profileUID+"|"+name+"|"+picUrl+"|"+level+"|"+local+"|"+className+s[0]+"|"+s[1]+"|"+s[2]+"|"+s[3]+"|"+s[4]);
		return name;
	}


}
