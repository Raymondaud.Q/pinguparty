package stats;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.*;

public class TempStat extends LocalReadWriter implements Serializable
{

	private String id;
	private int roundPlayedNumber;
	private int won;
	private int lost;
	private int draw;
	private int score;
	private int cumulScore;
	private int totalScore;
	private float elo;
	private String dateH;
	private float timeUsed;
	private String path = "../src/engine/data/temp_stat/";
	public static final String GENERAL_PATH = "../src/engine/data/temp_stat/";

	public TempStat( String uid , int rP , int win , int lose , int eq , int sc , int csc, int tsc ,float e, float time, String date) 
	{
		id = uid;
		roundPlayedNumber = rP;
		won = win ;
		lost = lose ;
		draw = eq;
		score = sc;
		cumulScore = csc ;
		totalScore = tsc;
		elo = e;
		timeUsed = time;
		dateH = date;
		path += id + ".txt";
	}

	public TempStat(String uid) // Crée ou charge un fichier de stat tempo
	{
		try
		{
			id  = uid;
			path += id + ".txt";
			File file = new File(path);

			if (file.createNewFile()) // Creates a new TempStat File
			{
				roundPlayedNumber = 0 ;
				won = 0 ;
				lost = 0 ;
				draw = 0 ;
				score = 0 ;
				cumulScore = 0;
				totalScore = 0;
				elo = 0 ;
				insertTempStat();
			}
			else // Loading from file and parsing instance
			{
			   	TempStat current = loadLastTempStat(id,path);
			   	roundPlayedNumber = current.getNb();
			   	won = current.getWin();
			   	lost = current.getLose();
			   	draw = current.getDraw();
			   	score = current.getScore();
			   	cumulScore = current.getCScore();
			   	totalScore = current.getTScore();
			   	elo  = current.getElo();
			   	timeUsed = current.getTimeUsed();
			   	dateH = current.getDate();
			}

		}
		catch ( Exception e)
		{
			e.printStackTrace();
			System.out.println(" Error loading / creating TempStat Data");
		}
	}

	public static void quickSave(String uid , boolean win , boolean eq , int sc , int csc, int tsc,float e, float time, String date ) // Sauvegarde rapidement les données
	{
		TempStat s = new TempStat(uid);
	
		s.increaseNb();
		if ( win )
			s.increaseWin();
		else
			s.increaseLost();
		if ( eq )
			s.increaseDraw();
		s.setScore(sc);
		s.setCScore(csc);
		s.setTScore(tsc);
		s.setELO(e);
		s.setTimeUsed(time);
		s.setDate(date);
		s.insertTempStat();
	}

	public static void delete(String id) // supprime un fichier
	{
		File file = new File("../src/engine/data/temp_stat/"+id+".txt");
        if(file.delete())
        {
            System.out.println("Delete file : ../src/engine/data/temp_stat/"+id+".txt");
        }
	}

	public boolean writeFile(String content)									// Ré-écrit le fichier prof.txt
	{
		return writeFile(content,path);
	}

	public boolean insertTempStat() // Ecrit une instance de tempstat en fin de fichier
	{
		try 
		{
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(path, true)));
		    out.println(roundPlayedNumber+";"+won+";"+lost+";"+draw+";"+score+";"+cumulScore+";"+totalScore+";"+elo+";"+timeUsed+";"+dateH+";");
		    out.close();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		    return false;
		}
		return true;
	}

	public TempStat loadTempStat()
	{
		id =  id.replaceAll(";","");
		return loadLastTempStat(id,path);
	}

	public TempStat[] loadAllStat() // Charge toutes les stats dans un tableau 
	{
		id =  id.replaceAll(";","");
		return loadTempStatFromFile(GENERAL_PATH,id);
	}

	public static TempStat[] loadAllStat(String id) // IDEM mais static
	{
		id =  id.replaceAll(";","");
		return loadTempStatFromFile(GENERAL_PATH,id);
	}

	public static TempStat[] loadAllStatByGame(String id)
	{
		TempStat[] byRound = loadTempStatFromFile(GENERAL_PATH,id);
		Vector<TempStat> byGame = new Vector<TempStat>();
		
		float tmpElo = 0;
		int cnt = 0;
		for ( TempStat tmp : byRound )
		{
			if ( tmp.getElo() != tmpElo)
			{
				tmpElo = tmp.getElo();
				byGame.add(tmp);
				cnt+=1;
			}
		}
		TempStat[] returnedArr = new TempStat[cnt];
		returnedArr = byGame.toArray(returnedArr);
		return returnedArr;
	}

	public int getNbRoundRecorded() // Return le nombre de round enregistré ( Faire attention a bien construire le profile pour apeller)
	{
		return getNbLine(path);
	}

	public static int getNbLine(String path) // Same but static  ex : "../src/engine/data/temp_stat/ID.txt"
	{
		return getNbLine(path);
	}

	public void increaseNb()
	{
		roundPlayedNumber+=1;
	}
	public void increaseWin()
	{
		won+=1;
	}
	public void increaseLost()
	{
		lost+=1;
	}
	public void increaseDraw()
	{
		draw+=1;
	}

	public void setScore(int s)
	{
		score = s;
	}

	public void setCScore(int s)
	{
		cumulScore = s;
	}

	public void setTScore(int s)
	{
		totalScore = s;
	}

	public void setELO(float e)
	{
		elo = e;
	}
	
	public void setDate(String date)
	{
		dateH = date;
	}
	
	public void setTimeUsed(float time)
	{
		timeUsed = time;
	}


	public int getNb()
	{
		return roundPlayedNumber;
	}

	public int getWin()
	{
		return won;
	}
	public int getLose()
	{
		return lost;
	}
	public int getDraw()
	{
		return draw;
	}

	public int getScore()
	{
		return score;
	}

	public int getCScore()
	{
		return cumulScore;
	}

	public int getTScore()
	{
		return totalScore;
	}

	public float getElo()
	{
		return elo;
	}
	
	public String getDate()
	{
		return dateH;
	}
	
	public float getTimeUsed()
	{
		return timeUsed;
	}
	
	public String getId()
	{
		return id;
	}

	public String toString()
	{
		return roundPlayedNumber+";"+won+";"+lost+";"+draw+";"+score+";"+totalScore+";"+elo;
	}



};
