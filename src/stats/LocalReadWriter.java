package stats;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Quentin Raymondaud
 * @author Thomas Robine
 * @author Gaelle Laperriere
 */
public class LocalReadWriter
{
	// Class LocalReadWriter
	
	/**
	 * @param content : String content to append
	 * @param path : path to txt formated file
	 * @return True if writing has beed done correctly
	 */
	static public boolean writeFile(String content,String path)									// Ré-écrit le fichier prof.txt
	{

		PrintWriter fileToWrite ;
		try
		{
			fileToWrite =  new PrintWriter(new BufferedWriter(new FileWriter(path)));
			fileToWrite.append(content);
			fileToWrite.close();
			System.out.println(" -> Add : " + content + " in " + path +" <-");
			return true;
		}
		catch (NullPointerException a)
		{
			System.out.println("Erreur : pointeur null");
			return false;
		}
		catch (IOException a)
		{
			System.out.println("Problème d'IO");
			return false;
		}
	}

	/**
	 * @param profileUID : GUID of the profile to erase
	 * @param path	: Path to txt formated file 
	 * @return True if it erase it successfully
	 */
	static public boolean eraseID(String profileUID, String path)
	{
		String total = "";
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String ligne;
			
			while((ligne = reader.readLine()) != null )
			{
				if ( ligne.indexOf(profileUID) != -1 && ligne.charAt(profileUID.length()) == ';' )
				{
					System.out.println(" <- Delete : " + ligne  + " in " + path + "->");
				}
				else
				{
					total += ligne+'\n';
				}
			}
			reader.close();
			if ( writeFile(total,path) )
				return true;
			return false;
		} 
		catch (Exception ex)
		{
			System.out.print("Impossible de supprimer ce profil !");
		}
		return false;
	}

	public static boolean eraseNLines(int n, String path)
	{
		String total = "";
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String ligne;
			int cnt = 0;
			
			while((ligne = reader.readLine()) != null )
			{
				if ( cnt < n )
				{
					System.out.println(" <- Delete : " + ligne  + " in " + path + "->");
					cnt +=1;
				}
				else
				{
					total += ligne+'\n';
				}
				
			}
			reader.close();
			if ( writeFile(total,path) )
				return true;
			return false;
		} 
		catch (Exception ex)
		{
			System.out.print("Impossible de supprimer ces lignes !");
		}
		return false;
	}

	/**
	 * Return the Id of the Profile that is named name
	 * @param name Profile name
	 * @param path Path to txt formated file
	 * @param local Local profile or not 
	 * @return String name
	 */
	public static String getId(String name , String path, boolean local)													// return id du nom local
	{
		name = name.replaceAll(";","");
		String info="";
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String ligne;
			while((ligne = reader.readLine()) != null )
			{
				if ( ligne.indexOf(name) != -1 )
				{
					if ( local )
					{
						if ( readField(ligne,5).charAt(0)=='1')
							return readField(ligne,1);
					}
					else
					{
						if ( readField(ligne,5).charAt(0)=='0')
							return readField(ligne,1);
					}

				}
			}
			
		} 
		catch (Exception ex)
		{
			System.out.print("Impossible de supprimer ce profil !");
		}
		return info;
	}

	/**
	 * @param id of Stats
	 * @return String that correspond to statistics
	 */
	public static String[] getStat(String id)
	{
		String [] s = new String[8];
		try
		{
			Profile p = new Profile();
			p = p.loadProfile(id);
			s = p.getStat().getStrStat();
		}
		catch ( Exception e)
		{
			e.printStackTrace();
			return null;
		}

		return s;
	}


	/**
	 * @param ligne String composed of field separated by ";"
	 * @param numField requiered field ( number )
	 * @return String field content
	 */
	public static String readField(String ligne,int numField)						// Retourn un champ selectionné d'une ligne de la base de donnée
	{
		char index = ' ' ;
		String name="";
		int field = 1;
		for (int i = 0 ; i<ligne.length() ; i ++)
		{
			if ( ligne.charAt(i) != ';' && numField == field )
			{
				index = ligne.charAt(i);
				name += index;
			}
			else if ( ligne.charAt(i) == ';')
			{
				field +=1;
			}
			else if(numField < field)

				return name;

		}
		return name;
	}

	/**
	 * @param id that need to be verified
	 * @param path to txt formated file
	 * @return True if it exist False otherwise
	 */
	public static boolean alreadyExists( String id , String path)  										// return true si existe déjà
	{
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String ligne;
			
			while((ligne = reader.readLine()) != null )
			{
				if ( ligne.indexOf(id) != -1 && ligne.charAt(id.length()) == ';' )
				{
					reader.close();
					return true;
				}
			}
			reader.close();
			
		} 
		catch (Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * @param path to  formated txt file
	 * @return Integer : number of local profiles
	 */
	public static int nbLocalProfiles(String path)														// Retourne le nombre de profils locaux
	{
		int nb = 0;
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String ligne;
			while((ligne = reader.readLine()) != null )
			{
				nb+=1;
			}
			reader.close();
		} 
		catch (Exception ex)
		{
			nb = -1;
			System.out.print("Impossible de supprimer ce profil !");
		}
		return nb;
	}

	/**
	 * @param path to formated txt file
	 * @return String array of all Stats
	 */
	public static String [][] getStrStats (String path) // return id nom elo total des points
	{
		
		int nb = getNbLine(path) ;
		String [][] global = new String[getNbLine(path)][4];
		String [] p = new String[4];

		Profile [] profs = loadProfileFromFile(path);
		try
		{
			for ( int i = 0 ; i < nb ; i ++ )
			{
				p = profs[i].getStat().getStrEloScore(profs[i].getName());
				for ( int j = 0 ; j < 4 ; j ++)
				{
					global[i][j]=p[j];
				}			
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return global;
	}

	/**
	 * @param path to formated txt file
	 * @return Profile Array
	 */
	public static Profile[] loadProfileFromFile(String path)
	{
		int nb = getNbLine(path) ;
		String [] uuid = getIdFromFile(path);
		Profile [] p = new Profile[nb];
		for ( int i = 0 ; i < nb ; i ++)
		{
			p[i] = new Profile();
			p[i] = p[i].loadProfile(uuid[i]);
		}
		return p;
	}

	/**
	 * @param path to formated txt file
	 * @return Stat Array
	 */
	public static TempStat[] loadTempStatFromFile(String path , String id)
	{
		try 
		{
			int nb = getNbLine(path+"/"+id+".txt") ;
			TempStat [] tempStats = new TempStat[nb];
			BufferedReader reader = new BufferedReader(new FileReader(new File(path+"/"+id+".txt")));
			String ligne;
			for ( int i = 0 ; i < nb ; i ++)
			{
				if ( (ligne = reader.readLine()) != null )
					tempStats[i]=parseTempStat(ligne,id);
			}
			reader.close();
			return tempStats ;

		} 
		catch (Exception ex)
		{
			System.err.println("LOADTEMPSTAT ERROR "+ex.getMessage());
		}
		return null ;
	}

	/**
	 * @param path to formated txt file
	 * @return Stat Array
	 */
	public static Stat[] loadStatFromFile(String path)
	{
		int nb = getNbLine(path) ;
		String [] uuid = getIdFromFile(path);
		Stat [] p = new Stat[nb];
		for ( int i = 0 ; i < nb ; i ++)
		{
			p[i] = new Stat();
			p[i] = p[i].loadStat(uuid[i]);
		}
		return p;
	}

	/**
	 * @param path to formated txt file
	 * @return String Array ( GUID ) 
	 */
	public static String[] getIdFromFile (String path)
	{
		String [] ids = new String[getNbLine(path)];
		String ligne;
		int cpt = 0;
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			while((ligne = reader.readLine()) != null )
			{
				ids[cpt] = readField(ligne,1);
				cpt+=1;			
			}
			reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return ids;
	}

	/**
	 * @param path to formated txt file
	 * @return Integer : Line number into file
	 */
	public static int getNbLine (String path)
	{
		int cpt = 0 ;
		String ligne;
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			
			while((ligne = reader.readLine()) != null )
			{
				cpt += 1;			
			}
			reader.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return cpt;
	}

	/**
	 * @param id GUI that identify the profile
	 * @param path to formated txt file
	 * @return Profile
	 */
	public static Profile loadProfile(String id, String path)
	{
		
		try 
		{
			
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String ligne;
			while((ligne = reader.readLine()) != null )
			{
				if ( ligne.indexOf(id) != -1 && ligne.charAt(id.length()) == ';' )
				{
					//System.out.println("<-- Stat Loaded -->");
					reader.close();
					return new Profile(readField(ligne,1),readField(ligne,2),readField(ligne,3),Integer.valueOf(readField(ligne,4)),Integer.valueOf(readField(ligne,5)),readField(ligne,6));
				}
			}
			reader.close();
			return null ;

		} 
		catch (Exception ex)
		{
			System.err.println("LOADPROFILE ERROR "+ex.getMessage());
		}
		return null ;
	}

	
	/**
	 * @param id taht identify stats we wish to load
	 * @param path to formated txt file
	 * @return Stat
	 */
	public static Stat loadStat(String id, String path)
	{
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String ligne;
		
			while((ligne = reader.readLine()) != null )
			{
				if ( ligne.indexOf(id) != -1 && ligne.charAt(id.length()) == ';' )
				{
					//System.out.println("<-- Stat Loaded -->");
					return new Stat(readField(ligne,1),Integer.valueOf(readField(ligne,2)),Integer.valueOf(readField(ligne,3)),Integer.valueOf(readField(ligne,4)),Integer.valueOf(readField(ligne,5)), Integer.valueOf(readField(ligne,6)),Integer.valueOf(readField(ligne,7)),Float.valueOf(readField(ligne,8)),Integer.valueOf(readField(ligne,9)));
				}
			}
			return null ;

		} 
		catch (Exception ex)
		{
			System.err.println("Error loadStat : "+ex.getMessage());
			ex.printStackTrace();
		}
		return null ;
	}

	public static TempStat loadLastTempStat(String id, String path)
	{
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String ligne;
			TempStat tempStat = null ;
			int cnt = 0 ;
			while((ligne = reader.readLine()) != null )
			{
				if ( cnt > 1000 ) // Si il y a plus de 1000 lignes
					eraseNLines(100,path); // On supprime les 100 premières
				tempStat = new TempStat(id,Integer.valueOf(readField(ligne,1)),Integer.valueOf(readField(ligne,2)),Integer.valueOf(readField(ligne,3)),Integer.valueOf(readField(ligne,4)),Integer.valueOf(readField(ligne,5)),Integer.valueOf(readField(ligne,6)),Integer.valueOf(readField(ligne,7)), Float.valueOf(readField(ligne,8)),Float.valueOf(readField(ligne,9)),readField(ligne,10));
				cnt +=1;
			}
			return tempStat ;

		} 
		catch (Exception ex)
		{
			System.err.println("Error loadLastTempStat : "+ex.getMessage());
			ex.printStackTrace();
		}
		return null ;
	}

	public static TempStat parseTempStat(String line , String id)
	{
		TempStat tempStat = new TempStat(id,Integer.valueOf(readField(line,1)),Integer.valueOf(readField(line,2)),Integer.valueOf(readField(line,3)),Integer.valueOf(readField(line,4)),Integer.valueOf(readField(line,5)),Integer.valueOf(readField(line,6)),Integer.valueOf(readField(line,7)), Float.valueOf(readField(line,8)),Float.valueOf(readField(line,9)),readField(line,10));
		return tempStat ;
	}

	/**
	 * @param path to formated txt file
	 * @return String txt file content
	 */
	public static String readFile(String path)														// Lecture de la base de donnée txt
	{
		String total = "";
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String ligne;

			while((ligne = reader.readLine()) != null)
			{
				total += ligne+'\n';
			}
		} 
		catch (Exception ex)
		{
			System.err.println("Error. "+ex.getMessage());
		}
		return total;
	}
	
	/**
	 * @param path to formated txt file
	 * @return String[] txt file content by line
	 */
	public static String[] loadByLines(String path)
	{
		String[] lines=new String[getNbLine(path)];
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String line;
			int i=0;
			while((line=reader.readLine())!=null)
			{
				lines[i]=line;
				i+=1;
			}
		}
		catch (Exception e)
		{
			System.err.println("Error. "+e.getMessage());
		}
		return lines;
	}

	/**
	 * @param path to formated txt file
	 * @return Profile[] txt file content
	 */
	public static Profile[] loadProfiles(String path)
	{
		Profile[] profiles=new Profile[getNbLine(path)];
		Profile p = new Profile();
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String line;
			int i=0;
			while((line=reader.readLine())!=null)
			{
				profiles[i]=p.loadProfile(line);
				i+=1;
			}
		}
		catch (Exception e)
		{
			System.err.println("Error. "+e.getMessage());
		}
		return profiles;
	}

	/**
	 * @param fN fileName
	 * @param path to formated txt file
	 */
	public static void eraseFileName(String fN, String path)
	{
		String total = "";
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String line;
			while((line=reader.readLine())!=null)
			{
				if(!line.equals(fN))
				{
					total+=line+'\n';
				}
			}
			reader.close();
			writeFile(total,path);
		} 
		catch (Exception ex)
		{
			System.out.print("Impossible de supprimer ce profil !");
		}
	}
}


