package stats;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;

import network.dataobjects.StatData;

/**
 * @author Robine Thomas
 * @author Raymondaud Quentin
 */
public class Stat extends LocalReadWriter implements Serializable
{
	String id;
	int nbGame=1;
	int win=0;
	int lost=0;
	int draw=0;
	int leaved=0;
	int scoreTotal=0;
	float elo=100f;
	int time=0;


	private static String path = "../src/engine/data/stats.txt" ;

	/**
	 * @param name Str
	 * @param nbgame int
	 * @param wiN int
	 * @param losT int
	 * @param draW int
	 * @param leaveD int
	 * @param scoreTotal int
	 * @param ELO double
	 * @param timE int
	 */
	public Stat(String name, int nbgame , int wiN , int losT , int draW, int leaveD, int scoreTotal,float ELO,int timE)
	{
		id  = name;
		nbGame = nbgame;
		win = wiN;
		lost = losT;
		draw = draW;
		leaved = leaveD;
		this.scoreTotal = scoreTotal;
		this.elo=ELO;
		time=timE;
	}

	public Stat(String name)
	{
		id  = name;
		nbGame=0;
		win=0;
		lost=0;
		draw=0;
		leaved=0;
		scoreTotal = 0;
		elo=100f;				// arbitraire
	}

	public Stat()
	{
		id  = "";
	}
	
	public Stat(StatData newbie)
	{
		id = newbie.id;
		nbGame = newbie.gameWon+newbie.gameLost+newbie.gameDraw;
		win = newbie.gameWon;
		lost = newbie.gameLost;
		draw = newbie.gameDraw;
		leaved = 0;
		scoreTotal = newbie.totalScore;
		elo = newbie.elo;
		time = (int) newbie.totalTimePlayed;
	}



	public boolean writeFile(String content)									// Ré-écrit le fichier prof.txt
	{
		return writeFile(content,path);

	}

	public String readFile()														// Lecture de la base de donnée txt
	{
		return readFile(path);
	}

	/**
	 * Insert Stat into file
	 * @return true if inserted successfully
	 */
	public boolean insertStat()														// Insere un profil
	{
		PrintWriter fileToWrite ;

		try
		{
			fileToWrite =  new PrintWriter(new BufferedWriter(new FileWriter(path,true)));
			fileToWrite.append(id+";"+nbGame+";"+win+";"+lost+";"+draw+";"+leaved+";"+scoreTotal+";"+elo+";"+time+";");
			fileToWrite.append('\n');
			fileToWrite.close();
			return true;
		}
		catch (NullPointerException a)
		{
			System.out.println("Erreur : pointeur null");
			return false;
		}
		catch (IOException a)
		{
			System.out.println("Problème d'IO");
			return false;
		}
	}

	/**
	 * Delete Stat
	 * @return boolean
	 */
	public boolean deleteStat()															// Suppression d'un profil
	{
		return eraseID(id,path);
	}

	
	/**
	 * Load a Stat object thanks to id
	 * @param id that identify Stat
	 * @return Stat
	 */
	public Stat loadStat(String id)														// Chargement de stats
	{
		id =  id.replaceAll(";","");
		return loadStat(id,path);
	}
	
	/**
	 * @param s time second
	 * @return String time hms
	 */
	public String convert(int s) 
	{
		int heures = 0;
		int minutes = 0;
		int secondes = 0;
		boolean b = true;
		while(b) {
			if(s==0) {
				b=false;
			}
			else {
				secondes++;
				if (secondes==60) {
					minutes++;
					if (minutes==60) {
						heures++;
						minutes=0;
					}
					secondes=0;
				}
				s--;
			}
		}
		String rep=heures+"h"+minutes+"min"+secondes+"sec";
		return rep;
	}
	
	/**
	 *  Stat display
	 */
	public void printStat()
	{
		System.out.println(" Player : "+id);
		System.out.println(" Number of game : "+nbGame);
		System.out.println(" Won : "+win);
		System.out.println(" Lost : "+lost);
		System.out.println(" Equality : "+draw);
		System.out.println(" Leaved : "+draw);
		System.out.println(" Elo : "+elo);
		String timE=convert(time);
		System.out.println(" Temps joué : "+timE);
		System.out.println("--> Stat Displayed ! <--");

	}



	/**
	 * Edit stat into file
	 */
	public void editStat()
	{
		deleteStat();
		insertStat();
	}

	public void increaseTime(int s) 
	{
		time+=s;
	}

	public void increaseNb()
	{
		nbGame+=1;
	}
	public void increaseWin()
	{
		win+=1;
	}

	public void increaseLost()
	{
		lost+=1;
	}
	public void increaseDraw()
	{
		draw+=1;
	}
	public void increaseLeaved()
	{
		leaved+=1;
	}

	public void increaseTotalScore(int score)
	{
		scoreTotal+=score;
	}

	/**
	 * If a player wins
	 * @param clt Profile Array
	 * @param i Int index
	 * @param p String name useless
	 * @return boolean
	 */
	static public boolean StatWinner( Profile [] clt, int i, String p)
	{
		clt[i].getStat().increaseWin();
		clt[i].getStat().editStat();
		return true;
	}
	/**
	 * If a player loose
	 * @param clt Profile Array
	 * @param i Int index
	 * @param p String name useless
	 * @return boolean
	 */
	static public boolean StatLooser(Profile[] clt, int i,String p)
	{
		clt[i].getStat().increaseLost();
		clt[i].getStat().editStat();
		return true;
	}

	/**
	 * If a player leaves
	 * @param clt Profile Array
	 * @param i Int index
	 * @param p String name useless
	 * @return boolean
	 */
	static public boolean StatLeaver(Profile[] clt, int i,String p)
	{
		clt[i].getStat().increaseLeaved();
		clt[i].getStat().editStat();
		return true;
	}

	/**
	 * If a player do a draw
	 * @param clt Profile Array
	 * @param i Int index
	 * @param p String name useless
	 * @return boolean
	 */
	static public boolean StatDraw(Profile[] clt, int i,String p)
	{
		clt[i].getStat().increaseDraw();
		clt[i].getStat().editStat();
		return true;
	}

	/**
	 * If a player wins
	 * @param clt Profile Array
	 * @param i Int index
	 * @param p String name
	 * @return boolean
	 */
	static public void setPlayerStat(Stat[] clt,int i,String p)
	{
		clt[i] = new Stat();
		if ( null == (clt[i]=clt[i].loadStat(p)) )
		{
			clt[i] = new Stat(p);
		}
		clt[i].increaseNb();
		clt[i].editStat();
		clt[i].printStat();	
	}

	public static Stat[] loadStatFromFile()
	{
		return loadStatFromFile(path);
	}



	public Stat getStat()
	{
		return this;
	}

	/**
	 * @param name String player name
	 * @return String ELO info
	 */
	public String [] getStrEloScore(String name)
	{
		String [] statsStrA = new String[4];
		statsStrA[0]=id;
		statsStrA[1]=name;
		statsStrA[2]=String.valueOf(elo);
		statsStrA[3]=String.valueOf(scoreTotal);
		return statsStrA;
	}

	/**
	 * @return String array of stat info
	 */
	public String [] getStrStat( )
	{
		String [] statsStrA = new String[8];
		statsStrA[0]=String.valueOf(nbGame);
		statsStrA[1]=String.valueOf(win);
		statsStrA[2]=String.valueOf(lost);
		statsStrA[3]=String.valueOf(draw);
		statsStrA[4]=String.valueOf(leaved);
		statsStrA[5]=String.valueOf(scoreTotal);
		statsStrA[6]=String.valueOf(elo);
		statsStrA[7]=String.valueOf(time);
		return statsStrA;
	}

	
	/**
	 * @param J1 Float ELO 
	 * @param J2 Float ELO
	 * @return float Probability
	 */
	public float probWin(float J1, float J2) // probabilite de gagner de J1 sur J2, on prend leurs elos en parametre 
	{ 	
		float diff = J1 - J2;
		return (1/(float)(1+Math.pow(10,(-diff/400))));
	} 
	

	/**
	 * @param oldElo float 
	 * @param result float
	 * @param probGagne float
	 * @param nbParties int
	 * @return float new ELO
	 */
	public float calcElo (float oldElo, float result, float probGagne, int nbParties) 
	{
		if (nbParties<30) 
			return (oldElo + 40*(result - probGagne));

		else if (oldElo < 2400) 
			return (oldElo + 20*(result - probGagne));

		else 
			return (oldElo + 10*(result - probGagne));
	}

	public int getTotalScore() 
	{
		return this.scoreTotal;
	}
	
	public float getElo() 
	{
		return this.elo;
	}
	
	public void setElo(float e) 
	{
		this.elo=e;
	}

	public int getNbGame()
	{
		return nbGame;
	}

	public void setTime(int s) 
	{
		time=s;
	}
	
	public int getTime() 
	{
		return time;
	}
	
}
