package stats;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;

import java.text.SimpleDateFormat;

import java.util.Calendar;

import stats.Profile;
import engine.game.Move;

public class Replay extends LocalReadWriter
{	
	private int nbPlayers;
	private Profile[] profiles;
	private String fileName;
	private static String path="../src/engine/data/replays/";
	private int nbGamesMax;
	
	/*
		 	../src/engine/data/replays/......Game.txt 
				getAllActions
				addMove
				addQuit
				addPass

			../src/engine/data/replays/......Decks.txt
				getDecks

			../src/engine/data/replays/......Profiles.txt
				getProfiles

			../src/engine/data/replays/......Scores.txt
				setScores
				getScores
			
			../src/engine/data/replay.txt
				setNbMaxReplayFiles
				getNbMaxReplayFiles
				enableReplay
				disableReplay
				getReplayState

			../src/engine/data/replayFiles.txt	
				getFileNames
				addFileName

			++++++++++++++++++++++++++++++++++
				createReplayFiles
				deleteFiles
				eraseFileName
	*/

	////////////////////////////////////////////////////////////////

	/**
	 * Create a new replay from now
	 * @param p the profiles in order
	 */ 
	public Replay(Profile[] p)
	{
		profiles=p;
		nbPlayers=p.length;
		fileName= new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		nbGamesMax=getNbMaxReplayFiles();

		if(getNbLine("../src/engine/data/replayFiles.txt")==nbGamesMax)
		{
			deleteFiles(getFileNames()[0]);
			eraseNLines(1,"../src/engine/data/replayFiles.txt");
		}
		addFileName(fileName);
		createReplayFiles();
	}

	/**
	 * Load an old replay
	 * @param fN his date 
	 */ 
	public Replay(String fN)
	{
		fileName=fN;
		profiles=loadProfiles(path+fileName+"Profiles.txt");
		nbPlayers=profiles.length;
	}

	////////////////////////////////////////////////////////////////

	public static void setNbMaxReplayFiles(int n)
	{
		String path="../src/engine/data/replay.txt";
		writeFile(loadByLines(path)[0]+"\n"+Integer.toString(n),path);
	}

	public int getNbPlayers() {
		return nbPlayers;
	}

	public static int getNbMaxReplayFiles()
	{
		return Integer.parseInt(loadByLines("../src/engine/data/replay.txt")[1]);
	}

	public static void enableReplay()
	{
		String path="../src/engine/data/replay.txt";
		writeFile("yes\n"+loadByLines(path)[1],path);
	}

	public static void disableReplay()
	{
		String path="../src/engine/data/replay.txt";
		writeFile("no\n"+loadByLines(path)[1],path);
	}

	public static boolean getReplayState()
	{
		if(loadByLines("../src/engine/data/replay.txt")[0].equals("yes"))
		{
			return true;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////

	/**
	 * Get the names of the replays saved in the db
	 */ 
	public static String[] getFileNames()
	{		
		return loadByLines("../src/engine/data/replayFiles.txt");
	}

	/**
	 * Add a new replay name to the list
	 * @param fN his date
	 */ 
	public static void addFileName(String fN)
	{	
		PrintWriter fileToWrite ;
		try
		{
			fileToWrite =  new PrintWriter(new BufferedWriter(new FileWriter("../src/engine/data/replayFiles.txt",true)));
			fileToWrite.append(fN+"\n");
			fileToWrite.close();
		}
		catch (NullPointerException e)
		{
			System.out.println("Erreur : pointeur null");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Create the new replay files
	 */ 
	public void createReplayFiles()
	{
		try
		{
			//Profiles
			File file=new File(path+fileName+"Profiles.txt");
			file.createNewFile();
			String profilesContent="";
			for(int i=0;i<profiles.length;i++)
			{
				profilesContent+=profiles[i].getUID()+"\n";
			}			
			writeFile(profilesContent,path+fileName+"Profiles.txt");

			//Game
			file=new File(path+fileName+"Game.txt");
			file.createNewFile();

			//Scores
			file=new File(path+fileName+"Scores.txt");
			file.createNewFile();
			
			//Decks
			file=new File(path+fileName+"Decks.txt");
			file.createNewFile();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Error creating Files");
		}
	}

	/**
	 * Delete files
	 * @param fN fileName
	 */ 
	public static void deleteFiles(String fN)
	{
		try
		{
			File file=new File(path+fN+"Decks.txt");
			if(file.delete())
			{
			    System.out.println("Delete file :"+fN+"Decks.txt");
			}
			file=new File(path+fN+"Game.txt");
			if(file.delete())
			{
			    System.out.println("Delete file :"+fN+"Game.txt");
			}
			file=new File(path+fN+"Profiles.txt");
			if(file.delete())
			{
			    System.out.println("Delete file :"+fN+"Profiles.txt");
			}
			file=new File(path+fN+"Scores.txt");
			if(file.delete())
			{
			    System.out.println("Delete file :"+fN+"Scores.txt");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Error deleting Files");
		}
        }

	////////////////////////////////////////////////////////////////

	/**
	 * Get the profiles playing (all of them)
	 */ 
	public Profile[] getProfiles()
	{
		return this.profiles;
	}

	/**
	 * Get the decks saved
	 */ 
	public String[] getDecks()
	{
		return loadByLines(path+fileName+"Decks.txt");
	}

	/**
	 * Get the scores saved
	 */ 
	public String[] getScores()
	{
		return loadByLines(path+fileName+"Scores.txt");
	}

	/**
	 * Get all the actions (move, quit or pass)
	 */ 
	public String[][] getAllActions()
	{
		//Pos;Col;Blitz;UID;
		//"Quit";UID;"";"";
		//"Pass";Blitz;UID;"";
		int k;
		String[] line=loadByLines(path+fileName+"Game.txt");
		String[][] action= new String[getNbLine(path+fileName+"Game.txt")][4];
		for(int i=0;i<line.length;i++)
		{
			k=0;
			action[i]=new String[4];
			action[i][0]="";
			action[i][1]="";
			action[i][2]="";
			action[i][3]="";
			for(int j=0;j<line[i].length();j++)
			{
				if(line[i].charAt(j)!=';')
				{
					action[i][k]+=line[i].charAt(j);
				}
				else
				{
					k++;
				}
			}
		}
		return action;
	}

	////////////////////////////////////////////////////////////////

	/**
	 * Add a move to the db
	 * @param blitz the time passed for the move
	 * @param move the move
	 * @param p the profile who played
	 */ 
	public void addMove(String blitz, Move move, Profile p)
	{
		PrintWriter fileToWrite ;
		try
		{
			fileToWrite =  new PrintWriter(new BufferedWriter(new FileWriter(path+fileName+"Game.txt",true)));
			fileToWrite.append(move.getPosition()+";"+move.getColor()+";"+blitz+";"+p.getUID()+";\n");
			fileToWrite.close();
		}
		catch (NullPointerException e)
		{
			System.out.println("Erreur : pointeur null");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Add a quit to the db
	 * @param p the profile who quitted
	 */ 
	public void addQuit(Profile profile)
	{
		PrintWriter fileToWrite ;
		try
		{
			fileToWrite =  new PrintWriter(new BufferedWriter(new FileWriter(path+fileName+"Game.txt",true)));
			fileToWrite.append("Quit;;;"+profile.getUID()+";\n");
			fileToWrite.close();
		}
		catch (NullPointerException e)
		{
			System.out.println("Erreur : pointeur null");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Add a pass to the db
	 * @param blitz the time passed before the pass if it was his turn, if not = 0
	 * @param p the profile who passed
	 */ 
	public void addPass(String blitz, Profile profile)
	{
		PrintWriter fileToWrite ;
		try
		{
			fileToWrite =  new PrintWriter(new BufferedWriter(new FileWriter(path+fileName+"Game.txt",true)));
			fileToWrite.append("Pass;;"+blitz+";"+profile.getUID()+";\n");
			fileToWrite.close();
		}
		catch (NullPointerException e)
		{
			System.out.println("Erreur : pointeur null");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Add scores
	 * @param scores
	 */ 
	public void setScores(String[] scores)
	{
		try
		{
			File file=new File(path+fileName+"Scores.txt");
			file.createNewFile();
			String scoresContent="";
			for(int i=0;i<scores.length;i++)
			{
				scoresContent+=scores[i]+"\n";
			}
			writeFile(scoresContent,path+fileName+"Scores.txt");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Error creating Files");
		}
	}	

	/**
	 * Add decks
	 * @param deck
	 */ 
	public void addDecks(String[] deck)
	{
		PrintWriter fileToWrite ;
		try
		{
			fileToWrite =  new PrintWriter(new BufferedWriter(new FileWriter(path+fileName+"Decks.txt",true)));
			String allDecks="";
			for(int i=0;i<deck.length;i++)
			{
					allDecks+=deck[i]+"\n";
			}
			fileToWrite.append(allDecks);
			fileToWrite.close();
		}
		catch (NullPointerException e)
		{
			System.out.println("Erreur : pointeur null");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}	
}
