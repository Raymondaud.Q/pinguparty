package stats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ranking implements Serializable
{
	private int nbWinner;
	private int size=0;
	private List<Profile> winners;
	private List<PlayerRank> playerRank;

	public Ranking()
	{
		nbWinner = 0;
		winners = new ArrayList<Profile>() ;
		playerRank = new ArrayList<PlayerRank>();
	}
	
	public Ranking (ArrayList<PlayerRank> net, Profile winner)
	{
		size = net.size();
		nbWinner = 1; // Par défaut pour affichage client => Erreur dans les objets envoyé pas de signalisation clair sur les gagnants
		playerRank = net;
		winners = new ArrayList<Profile>();
		winners.add(winner);
	}
	
	public int getSize()
	{
		return size;
	}
	public void print()
	{
		System.out.println(winners);
		System.out.println(playerRank);
	}

	public void addWinner(Profile p)
	{
		nbWinner+=1;
		winners.add(p);
	}

	public void addRank(PlayerRank r)
	{
		size+=1;
		playerRank.add(r);
	}

	public List<PlayerRank> getPlayerRank()
	{
		return playerRank;
	}

	public PlayerRank getPlayerRankI(int i)
	{
		return playerRank.get(i);
	}
	public Profile getProfileWinnerI(int i)
	{
		return winners.get(i);
	}

	public List<Profile> getWinners()
	{
		return winners;
	}

	public int nbWinner()
	{
		return nbWinner;
	}
}

