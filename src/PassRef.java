import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import engine.agent.Player;
import gui.View;
import stats.Profile;
import stats.Stat;
import stats.TempStat;  
import gui.ActionAIEdition;
import gui.ActionImage;
import gui.ActionJoinGame;
import gui.ActionLocalGame;
import gui.ActionMoreStatistics;
import gui.ActionNetworkGame;
import gui.ActionNewAI;
import gui.ActionNewPlayer;
import gui.ActionOptions;
import gui.ActionPlayerEdition;
import gui.ActionProfiles;
import gui.ActionStatistics;

/**
 * @author Quentin Raymondaud
 */
public class PassRef
{
  //class PassRef or Main

	/**
	 * [main description]
	 * Launch Graphical User Interface
	 * @throws IOException 
	 */
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(() -> { createAndShowGui();});
	}

	/**
	 * @author Raymondaud Quentin
	 * [createAndShowGui description]
	 * Creates a View, a frame, etc ..
	 * and shows it
	 */
	private static void createAndShowGui()
	{
		View mainPanel = new View();
		JFrame frame = new JFrame("Pingu Party");
		frame.setDefaultCloseOperation(0);
		frame.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				int clickedButton = JOptionPane.showConfirmDialog(null, "Exit the Game ?", "Confirmation", JOptionPane.YES_NO_OPTION);
				if (clickedButton == JOptionPane.YES_OPTION)
				{
					System.exit(0);
				}
			}
		});
		frame.add(mainPanel);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}
}