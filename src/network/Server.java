package network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.SwingWorker;

import engine.agent.Player;
import engine.agent.Robot;
import engine.game.Color;
import engine.game.Game;
import gui.LocalGame;
import gui.NetworkGame;
import network.dataobjects.DataObjectParser;
import network.dataobjects.PlayerInformation;
import network.dataobjects.ProfileStatData;
import stats.Profile;
import stats.Replay;
import stats.Stat;

/**
 * @author Quentin Raymondaud
 */


public class Server
{
	// Class Server

	/**
	 * ServerSocket that accept client's Socket
	 */
	private ServerSocket  serverSocket ;                // Serveur

	/**
	 * Input from the clients
	 */
	private List<ObjectInputStream> input ;             // variable permettant la reception d'informations

	/**
	 * Output list to clients
	 */
	private List<ObjectOutputStream> output ;           // variable permettant l'envoie d'information

	/**
	 * Socket list that allow to share data with clients
	 */
	private List<Socket> client ;                       // Socket permettant de communiquer avec les clients

	/**
	 * Profile list of player before the creation of Game
	 */
	private List<Profile> prof ;                        // Liste des profiles

	/**
	 * Game that client will play into
	 */
	private Game pingu ;                                // Instance du jeu

	/**
	 *Number of player allowed [USELESS]
	 */
	private int nbPlayerMAX ;                           // Nombre maximun d'agent

	/**
	 * Number of player before a game start
	 */
	private int actualNb;                               // Nombre réel de joueur
	
	private SwingWorker shotListener;
	network.dataobjects.Move pld ;
	
	public void run(int i)
	{
		shotListener = new SwingWorker<Void, Void>()
		{
			@Override
			protected Void doInBackground() throws Exception
			{
				pld = getFromI(i); //Reception du moove
				return null;
			}
		};
		shotListener.execute();
	}

/**
 * return the number of player connected during the lobby
 * @return Integer : actualNb
 */
	public int getActualNb()
	{
		return actualNb;
	}


/**
 * Set actualNb at nb
 * @param Integer nb
 */
	public void setActualNb(int nb)
	{
		actualNb = nb;
	}

/**
 * return nbMax
 * @return Integer : nbPlayerMAX
 * USELESS AF
 */
	public int getMax()
	{
		return nbPlayerMAX;
	}

/**
 * Baguette
 * @param m Integer
 */
	public void setMax(int m)
	{
		nbPlayerMAX = m ;
	}

/**
 * Return the profile list generated during lobbying
 * @return prof : Profile Array
 */
	public List<ProfileStatData> getNetworkProfiles()
	{
		return DataObjectParser.parseProfiles((ArrayList)prof);
	}
	
	public List<Profile> getProfiles()
	{
		return prof;
	}

/**
 * Return the current Game
 * @return Game : pingu
 */
	public Game getGame()
	{
		return pingu;
	}

	/**
	 * KICK EVERY ONE OUT
	 */
	public void close()
	{
		try
		{
			serverSocket.close();
			for (int i = 0 ; i < actualNb ; i ++)
			{
				if( input.get(i) != null && output.get(i) != null && client.get(i)!=null)
				{
					input.get(i).close();
					output.get(i).close();
					client.get(i).close();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void closeLobby()
	{
		try
		{
			for ( int player = 0 ; player < actualNb ; player ++ )
			{
				kickLobby(player);
			}
			close();
		}
		catch(Exception e)
		{}
	}

	public void stop()
	{
		try 
		{
			serverSocket.close();
		}
		catch ( Exception e)
		{};
	}

	/**
	* Server Construction
	* @param nbp number of Player
	* @param p  Profile List
	*/
	public Server(int nbp, Profile [] p)
	{
		nbPlayerMAX = nbp;
		actualNb = p.length;

		output = new  ArrayList<ObjectOutputStream>();
		input = new  ArrayList<ObjectInputStream>();

		client = new ArrayList<Socket>();
		prof = new ArrayList<Profile>(Arrays.asList(p));
		
		for ( int i = 0 ; i < actualNb ; i ++)
		{
			client.add(null);
			output.add(null);
			input.add(null);
		}

		try
		{
			serverSocket = new ServerSocket(11111);
		}
		catch (IOException e)
		{
			System.out.println(" CRITICAL ERROR : Création du serveur ! ");
			System.exit(0);
		}
	}

	/**
	 * Accept a socket and put it at the index i in every list ( Object , Socket )
	 * @param i Integer : index
	 */
	public void acceptSocket()
	{
		try
		{
			Socket newS = serverSocket.accept();
			client.add(newS);
			output.add(new ObjectOutputStream(newS.getOutputStream()));
			input.add(new ObjectInputStream(newS.getInputStream()));
			
			Object p = input.get(input.size()-1).readObject();
			if ( p instanceof ProfileStatData && pingu == null)
			{
				ProfileStatData neutralPS = (ProfileStatData) p;
				Profile newProf = new Profile(neutralPS);
				newProf.setLocal(0);
				prof.add(newProf);
				actualNb+=1;
			}
			else //ATTENTE MODE SPEC
			{
				int nb = client.size()-1;
				client.get(nb).close();
				client.remove(nb);
				output.get(nb).close();
				output.remove(nb);
				input.get(nb).close();
				input.remove(nb);
			}
		}
		catch ( Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Finish the lobbying , send profile list and create the Game
	 */
	public void initGame(int time)
	{
		Profile[] stockArr = new Profile[prof.size()];
		stockArr = prof.toArray(stockArr);
		for (int i = 0 ; i < stockArr.length ; i ++)
			stockArr[i].printProf();
		System.out.println(stockArr.length);
		pingu = new Game(actualNb,stockArr,time,null);
		setRemote();
		pingu = pingu.shuffle();
		sendToPlayer(DataObjectParser.parseGameIntoRoundStart(pingu)); // Envoie un roundstart a chaque joueur
	}

	/**
	 * Console display
	 * @param pro Profile List
	 */
	public void displayLobby( List<Profile> pro)
	{
		Iterator<Profile> it = pro.iterator();
		while (it.hasNext())
		{
			Profile p = it.next();
			p.printProf();
		}
		System.out.println("______________________");
	}
	
	

	/**
	 * Kick the player at index i
	 * @param  i Index
	 * @return Boolean
	 */
	public boolean kickLobby(int i)
	{
		if ( i == -1 )
			return false;
		try
		{
			if ( i < client.size())
			{
				sendToAllLobby("kick:"+prof.get(i).getUID());
				System.out.println(prof.get(i).getName() + " Delete from lobby .");
				
				if( input.get(i) != null && output.get(i) != null && client.get(i)!=null)
				{
					client.get(i).close();
	
				}
				client.remove(i);
				output.remove(i);
				input.remove(i);
				prof.remove(i);
				
				actualNb -=1;
			}
			
		}
		catch(IOException e)
		{
			System.out.println(" Le joueur "+prof.get(i).getName()+" n'est plus disponnible ");
		}
		return true;
	}

	/**
	 * Send to client i the object o
	 * and check if the Client is already connected
	 * @param o Object to send
	 * @param i index
	 */
	public void sendToI(Object o,int i)
	{
		Player p = pingu.getPlayer(i,pingu.getNbPlayer());
		if ( p.getLeaver() )
		{
			pingu.setPlayerDead(i);
		}
		else if ( p.getRemote() && ! p.getLeaver() )
		{
			try
			{
				p.getOutput().reset();
				p.getOutput().writeObject(o);
				p.getOutput().flush();
			}
			catch(Exception e)
			{
				System.out.println("NO CONNECTION : BUG sendToI IMPOSSIBLE ");
				pingu.setPlayerDead(i);
				pingu.setPlayerLeaver(i);
			}
		}
	}

	/**
	 * Meaningfull name
	 * send using the PLayer inner output
	 * @param o THE object
	 */
	public void sendToAll(Object o)
	{
		for ( int i = 0  ; i < pingu.getNbPlayer() ; i ++ )
		{
			Player p = pingu.getPlayer(i,pingu.getNbPlayer());
			
			if ( p.getRemote() && ! p.getLeaver() )
			{
				ObjectOutputStream out =  p.getOutput();
				if ( out != null)
				{
					System.out.println("Envoie de l'objet a " + p.getProfile().getName());
					try
					{
						out.reset();
						out.writeObject(o);
						out.flush();
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
						System.out.println("NO CONNECTION : sendToAll");
					}
				}
			}
		}
	}

	/**
	 * send the object o during the lobby
	 * send using Server output
	 *  @param o Object
	 */
	public int sendToAllLobby(Object o)
	{
		int counter = 0 ;
		for ( ObjectOutputStream out : output )
		{
			if ( out != null )
			{
				try
				{
					out.reset();
					out.writeObject(o);
					out.flush();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					return counter;
				}
				
			}
			counter +=1;
		}
		return -1;
	}

	/**
	 * Send Object to all except to client k
	 * @param k index
	 * @param o Object
	 */
	public void sendToAllExceptI(int k, Object o)
	{
		for ( int i = 0  ; i < pingu.getNbPlayer() ; i ++ )
		{
			if ( i !=  k)
			{
				Player p = pingu.getPlayer(i,pingu.getNbPlayer());
				if( p.getRemote() && ! p.getLeaver() )
				{

					ObjectOutputStream out = p.getOutput();
					if ( out != null)
					{
						try
						{
							out.reset();
							out.writeObject(o);
							out.flush();
						}
						catch(Exception ex)
						{
							System.out.println("NO CONNECTION : Bug sendToAll");
						}
					}
				}
			}
		}
	}

	/**
	 * set the player as distant player and give him its output and input
	 */
	public void setRemote()
	{

		for ( int i = 0 ; i < pingu.getNbPlayer() ; i ++ )
		{
			Player pl = pingu.getPlayer(i,pingu.getNbPlayer());
			if (  client.get(i) != null )
			{
				try
				{
					pl.setRemote();
					pl.setOutput(output.get(i));
					pl.setInput(input.get(i));
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("NO CONNECTION : Bug setRemote");
				}
			}
		}
	}

	/**
	 * Send the object i at the client i ( for all client )
	 * @param o Object Array
	 */
	public void sendToPlayer(Object [] o)
	{

		for ( int i = 0 ; i < pingu.getNbPlayer() ; i ++ )
		{
			Player pl = pingu.getPlayer(i,pingu.getNbPlayer());
			if (  pl.getRemote() && ! pl.getLeaver() )
			{
				try
				{
					Object p = o[i];
					pl.getOutput().reset();
					pl.getOutput().writeObject(p);
					pl.getOutput().flush();
				}
				catch(Exception e)
				{
					System.out.println("NO CONNECTION : Bug sendToPlayer");
				}
			}
		}
	}

	/**
	 * Wait for receiving an Object String from the client i
	 * @param  i Index
	 * @return  String
	 */
	public network.dataobjects.Move getFromI(int i)
	{

		Player pl = pingu.getPlayer(i,pingu.getNbPlayer());
		if (  pl.getRemote() && ! pl.getLeaver()  )
		{
			try
			{
				Object o = pl.getInput().readObject();
				if ( o instanceof network.dataobjects.Move )
				{
					return (network.dataobjects.Move) o;
				}
			}
			catch ( Exception e)
			{
				System.out.println("NO CONNECTION : Bug getFromI");
			}
		}

		return null;
	}

	public void saveProfiles()
	{
		pingu.saveProfiles();
		sendToAll(getGame().convertToEndGame());
	}

	public boolean play(NetworkGame g)
	{
		try
		{
			Thread.sleep(100);
		}
		catch (Exception e)
		{}
		
		boolean replay = false;
		int i = getGame().getPlayerIndex();
		Player p = getGame().getPlayer(i,getGame().getNbPlayer()) ;
		if ( p.getGR() )
			System.out.println(p.getPseudo() + " est encore en vie !! ");
		else
		{
			System.out.println(p.getPseudo() + " passe son tour !! ");
			int a = getGame().endGame(i);
			if ( a == 1 )
			{
				//sendToAll(EndGame);
			}
			else if ( a == 2)
			{
				Game.startRound(getGame());
				getGame().initBoard();
				sendToPlayer(getGame().convertToRoundStart());
			}
			return true;
		}

		sendToAll(getGame().convertToNextTurn());                       // Envoie 1 - NextTurn;

		String played="";

		if ( p.getRemote() && ! p.getLeaver() && p.getGR() ) // Si c'est un joueur distant
		{
			p.startTimer();
			run(i);
			
			while ( ! shotListener.isDone() && !shotListener.isCancelled() )
			{
				try 
				{
					Thread.sleep(1);
				}
				catch(Exception e)
				{}
				
				if ( p.timerNull() == false )
				{
					p.updateTimer();
					if ( p.timeLeft() <= 0 )
						shotListener.cancel(true);
				}
			}
			if (  !shotListener.isCancelled() && getGame().putCard(pld.position,""+Color.getColorChar(pld.color)))
			{
				p.useCard(pld);
				getGame().lastPlayerInfo = new PlayerInformation();
				getGame().lastPlayerInfo.id = p.getProfile().getUID();
				getGame().lastPlayerInfo.remainingTime =  -1; 
				getGame().lastPlayerInfo.currentScore = p.getScore();
				getGame().lastPlayerInfo.lastMovePlayed = pld;
				System.out.println("Position : " + getGame().lastPlayerInfo.lastMovePlayed.position+" Couleur : "+Color.getColorChar(getGame().lastPlayerInfo.lastMovePlayed.color));
				if ( p.timerNull() == false)
					p.stopTimer();
			}
			else
			{
				getGame().setPlayerDead(i);
			}	
		}
		else
		{
			if ( p.getGR() && ! p.getLeaver() )
			{
				System.out.println(p.getClass());
				if ( p != g.user || p.getClass().toString().indexOf("Player") == -1 )
				{
					played = p.play(getGame());
				}
				else
				{
					played = "R";
					if ( p.timerNull() == false) 
						p.startTimer();
					
					while (played.charAt(0) == 'R')
					{
						NetworkGame.roundAc = "FINISHED";
						NetworkGame.roundPlace = "FINISHED";
						while (NetworkGame.roundAc.equals("FINISHED") || NetworkGame.roundPlace.equals("FINISHED"))
						{
							try
							{
								Thread.sleep(10);
								if ( p.timerNull() == false && p.updateTimer() )
								{
									NetworkGame.roundAc = "D";
									NetworkGame.roundPlace = "D";
								}
							}
							catch (Exception e)
							{}
						}
						played = g.user.play(getGame(),NetworkGame.roundPlace+NetworkGame.roundAc);
					}

					NetworkGame.roundAc = "FINISHED";
					NetworkGame.roundPlace = "FINISHED";
				}

				if ( played != null && played.length() != 0 )
				{
					getGame().lastPlayerInfo = new PlayerInformation();
					getGame().lastPlayerInfo.id = p.getProfile().getUID();
					getGame().lastPlayerInfo.remainingTime =  -1; 
					getGame().lastPlayerInfo.currentScore = p.getScore();
					
					if ( p.getGR() && ! played.equals("DDD")  && ! played.equals("QQQ"))
					{
						if ( p.timerNull() == false)
						{
							p.stopTimer();
							if ( p.timeLeft() <= 0 )
								getGame().setPlayerDead(i);
						}
						if ( ( p.timerNull() == false && p.timeLeft() > 0 )|| p.timerNull() == true)
							getGame().lastPlayerInfo.lastMovePlayed = p.useCard(played);
					}						

					else if ( p.getGR() && played.charAt(0) == 'D')
					{
						getGame().setPlayerDead(i);
						getGame().lastPlayerInfo.lastMovePlayed = new network.dataobjects.Move(40,network.dataobjects.Color.UNDEFINED);
					}

					else if ( p.getGR() && played.charAt(0) == 'Q')
					{
						getGame().lastPlayerInfo.lastMovePlayed = new network.dataobjects.Move(40,network.dataobjects.Color.UNDEFINED);
						getGame().setPlayerDead(i);
						getGame().setPlayerLeaver(i);
					}
					System.out.println("Position : " + getGame().lastPlayerInfo.lastMovePlayed.position+" Couleur : "+Color.getColorChar(getGame().lastPlayerInfo.lastMovePlayed.color));
					
					
				}
				else
				{
					getGame().setGame(false);
				}
			}
		}
		g.flag = true;

		
		replay=false;


		int a = getGame().endGame(i);
		if ( a == 1 )
		{
			sendToAll(getGame().convertToEndGame());
			return false;
		}
		else if ( a == 2)
		{
			Game.startRound(getGame());
			getGame().initBoard();
			sendToPlayer(getGame().convertToRoundStart());
		}
		return true;

	}
}
