package network.dataobjects;

import java.io.Serializable;

public class ProfileStatData implements Serializable
{
	private static final long serialVersionUID = 1L;
	public ProfileData profile;
	public StatData stat;
	
	public ProfileStatData ( ProfileData p , StatData s)
	{
		profile = p ;
		stat = s;
	}
}
