package network.dataobjects;

import java.io.Serializable;


/**
 * @author Jarod
 *
 */
public enum Color 
{
	/**
	 * Color red
	 */
	RED,
	/**
	 * Color green
	 */
	GREEN,
	/**
	 * Color Blue
	 */
	BLUE,
	/**
	 * Color Yellow
	 */
	YELLOW,
	/**
	 * Color Purple
	 */
	PURPLE,
	/**
	 * Color Undefined
	 */
	UNDEFINED, 
};

