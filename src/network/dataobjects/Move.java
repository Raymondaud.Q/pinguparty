package network.dataobjects;

import java.io.Serializable;

public class Move implements Serializable
{
	private static final long serialVersionUID = 1L;
	public int position;
	public Color color;
	
	public Move (int pos , Color c)
	{
		position = pos;
		color = c;
	}
}