package network.dataobjects;

import java.io.Serializable;

public class ProfileData implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String id;
	public boolean isHuman;
	public boolean isDistant;
	public String nickname;
	public String avatarUrl;
	
	public ProfileData ( String uid , boolean h , boolean d , String nick , String url )
	{
		id = uid;
		isHuman = h;
		isDistant = d;
		nickname = nick;
		avatarUrl = url;
	}
}
