package network.dataobjects;

import java.io.Serializable;

public class StatData implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String id;
	public int gameWon;
	public int gameDraw;
	public int gameLost;
	public int totalScore;
	public float totalTimePlayed;
	public float elo;
	
	public StatData ( String uid, int gW, int gD, int gL,int tS,float ttp, float e )
	{
		id = uid;
		gameWon = gW;
		gameDraw = gD;
		gameLost = gL;
		totalScore = tS;
		totalTimePlayed = ttp;
		elo = e;
	}
}
