package network.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class NextTurn implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String currentPlayerID ;
	public HashMap<Color,ArrayList<Integer>> availableMoves;
	public PlayerInformation lastPlayerInfo;
	
	public NextTurn ( String id , HashMap<Color,ArrayList<Integer>> moves , PlayerInformation pI )
	{
		currentPlayerID = id;
		availableMoves = moves;
		lastPlayerInfo = pI;
	}
	
}
