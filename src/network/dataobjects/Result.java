package network.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;

public class Result implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String id;
	public int rank;
	public int score;
	public int greenCardsPlayed;
	public ArrayList<Integer> roundWinnerId;
	
	public Result ( String uid , int r , int s , int gc , ArrayList<Integer> rwid)
	{
		id = uid ;
		rank = r;
		score = s;
		greenCardsPlayed = gc;
		roundWinnerId  = rwid;
	}
}
