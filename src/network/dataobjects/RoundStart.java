package network.dataobjects;

import java.io.Serializable;

public class RoundStart implements Serializable
{
	private static final long serialVersionUID = 1L;
	public int nbPlayers;
	public String hand;
	public Color excludedCard;
	public float blitzTimer;
	public String lastRoundWinnerID;
	public Object misc;
	
	public RoundStart(int nb , String hnd , Color eC, float blitz , String lri )
	{
		nbPlayers = nb;
		hand = hnd;
		excludedCard = eC;
		blitzTimer = blitz;
		lastRoundWinnerID = lri;
	}
}
