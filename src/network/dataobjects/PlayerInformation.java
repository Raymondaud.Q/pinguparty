package network.dataobjects;

import java.io.Serializable;

public class PlayerInformation implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String id;
	public float remainingTime;
	public Move lastMovePlayed;
	public int currentScore;
	
	public PlayerInformation( String UID , float rTime, Move m, int score)
	{
		this.id = UID ;
		this.remainingTime = rTime;
		this.lastMovePlayed = m ;
		this.currentScore = score;
	}

	public PlayerInformation() {
		// TODO Auto-generated constructor stub
	}
}
