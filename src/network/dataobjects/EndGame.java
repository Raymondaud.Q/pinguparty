package network.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;

public class EndGame implements Serializable
{
	private static final long serialVersionUID = 1L;
	public ArrayList<StatData> stats;
	public ArrayList<Result> ranking;
	
	public EndGame ( ArrayList<StatData> psd ,  ArrayList<Result> rk)
	{
		stats = psd;
		ranking = rk;
	}
}
