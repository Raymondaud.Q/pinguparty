package network.dataobjects;

import java.util.ArrayList;

import engine.agent.Player;
import engine.game.Game;
import stats.PlayerRank;
import stats.Profile;
import stats.Ranking;
import stats.Stat;

public class DataObjectParser 
{
	public static ArrayList<Profile> parseProfileStatData( ArrayList<ProfileStatData> newbies )
	{
		ArrayList<Profile> p = new ArrayList<Profile>();
		int counter = 0;
		for ( ProfileStatData psd : newbies)
		{
			p.add(new Profile(psd));
		}
		return p;
	}
	
	public static ArrayList<ProfileStatData> parseProfiles( ArrayList<Profile> pgm )
	{
		ArrayList<ProfileStatData> newbies = new ArrayList<ProfileStatData>();
		int counter = 0;
		for ( Profile p : pgm)
		{
			newbies.add(p.toProfileStatData());
		}
		return newbies;
	}

	public static RoundStart[] parseGameIntoRoundStart(Game pingu) 
	{
		return pingu.convertToRoundStart();
	}
	public static NextTurn parseGameIntoNextTurn(Game pingu)
	{
		return pingu.convertToNextTurn();
	}
	
	public PlayerRank parseResultIntoPlayerRank(Result data)
	{
		return null;
	}
	public static Ranking parseEndGameIntoRanking(EndGame datas,Profile[] gameProfiles)
	{
		ArrayList<Profile> profs = new ArrayList<Profile>();
		ArrayList<PlayerRank> ranks = new ArrayList<PlayerRank>();
		
		ArrayList<String> names = new ArrayList<String>();
		
		for ( int i = 0 ; i < datas.stats.size() ; i++)
		{
			for ( int j = 0 ; j < gameProfiles.length ; j++)
			{
				if ( datas.stats.get(i).id.equals(gameProfiles[j].getUID()))
				{
					names.add(gameProfiles[j].getName());
					profs.add(gameProfiles[j]);
				}
			
			}
		}
		
		for (int i = 0 ; i < gameProfiles.length ; i++)
		{
			ranks.add(new PlayerRank(datas.ranking.get(i),datas.stats.get(i),names.get(i)));
		}
		
		
		return new Ranking(ranks,profs.get(0));
	}

}
