package network;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import engine.agent.AgentFactory;
import engine.agent.Player;
import engine.agent.Robot;
import engine.game.Color;
import engine.game.Game;
import gui.NetworkGameCLI;
import network.dataobjects.DataObjectParser;
import network.dataobjects.EndGame;
import network.dataobjects.Move;
import network.dataobjects.NextTurn;
import network.dataobjects.ProfileStatData;
import network.dataobjects.RoundStart;
import stats.Profile;
import stats.Ranking;
import java.io.EOFException;
import java.io.IOException;

/**
 * @author Quentin Raymondaud
 */

public class Client
{
	//Class Client
	/**
	 * Client socket bind (or not) to ServerSocket
	 */
	private Socket cli;                                 // Socket connecté au serveur

	/**
	 * Stream that send object to Server
	 */
	private ObjectOutputStream output ; // variable d'envoie d'objets
	/**
	 * Stream that received object from Server
	 */
	private ObjectInputStream input ;       // variable de reception d'objets


	private static final int port =  11111 ;    // Port du serveur dédié a l'application

	/**
	 * Profile list used for save distant Profiles & Stat and for displaying during lobbying
	 */
	private List<Profile> prof ;        // Liste des profil

	/**
	 * Profile that the player will contain
	 */
	private Profile currentProfile ;

	/**
	 * Player
	 */
	private Player pinguPlayer ;

	/**
	 * Game received
	 */
	private static Game pinguGame ;

	private boolean play = false;
	
	private HashMap<network.dataobjects.Color, ArrayList<Integer>> possibleMoves;
	
	public String currentPlayerUID;
	
	public Ranking endRank ;

	private int roundNumber = 1 ;


	/**
	 * Client Constructor
	 * Bind, construct ObjectStreams and send profile to Server
	 * @param ip ServerSocketIP
	 * @param p  Profile selected
	 */
	public Client(String ip,Profile p)
	{
		pinguPlayer=new Player();
		currentProfile = p;
		prof = new ArrayList<Profile>();
		prof.add(p);
		try
		{
			cli = new Socket(ip,port);                                  // Création du socket
			output = new ObjectOutputStream(cli.getOutputStream());     // Création de la variable Envoie
			input = new ObjectInputStream(cli.getInputStream());        // Création de la variable de Reception
			sendToServ(p.toProfileStatData());
		}
		catch (Exception e)
		{
			System.out.println(" Hôte introuvable ");
		}
	}

	/**
	 * tell if the Client will play or not
	 * @return Boolean
	 */
	public boolean getPlay()
	{
		return play;
	}

	/**
	 * Return the Profile list of all the player in the distant game
	 * @return Profile List
	 */
	public List<Profile> getProfiles()
	{
		return prof;
	}

	/**
	 * Returns the Client Player
	 * @return Player
	 */
	public Player getCliPlayer()
	{
		return pinguPlayer;
	}
	


	/**
	 * Returns the last Game received by the Client
	 * @return Game
	 */
	public Game getCliGame()
	{
		return pinguGame;
	}

	/**
	 * Close connection
	 */
	public void close()
	{
		try
		{
			if ( ! cli.isClosed() )
				cli.close();
			output.close();
			input.close();
			pinguPlayer = null;
			pinguGame = null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Receive an ArrayList of profile,
	 * True if arrayList received false otherwise
	 * @return Boolean
	 */
	public boolean lobbyGUI()
	{
		try
		{
			Object answer = input.readObject();
			if ( answer != null)
			{
				if (answer instanceof ArrayList<?> )
				{
	
					if(((ArrayList<?>)answer).get(0) instanceof ProfileStatData )
					{
						prof = DataObjectParser.parseProfileStatData((ArrayList<ProfileStatData>) answer);
						displayLobby(prof);
					}
				}
				else if ( answer instanceof String)
				{
					String msg = (String) answer;
					
					if ( msg.indexOf("kick") != -1)
					{
						String UID = (String)((String) answer).subSequence(5, ((String) answer).length());
						if ( UID.equals(currentProfile.getUID()))
						{
							close();
							return false;
						}						
						deleteProf(UID);
					}
					
				}
					
				else if ( answer instanceof RoundStart)
				{
					RoundStart rs = (RoundStart) answer;
					System.out.println("Creation Player");
					this.pinguPlayer = AgentFactory.createCLIPlayer(rs,this.currentProfile);
					System.out.println("Creation Game");
					Profile[] arrProf = (Profile[]) this.prof.toArray(new Profile[this.prof.size()]);
					this.pinguGame = new Game(rs.nbPlayers,arrProf,0,null);
					System.out.println("Game crée");
					return false;
				}
			}
			
			return true;


		}
		catch(Exception e)
		{
			e.printStackTrace();
			
			return false;
		}
		

	}
	
	public boolean deleteProf(String UID)
	{
		for ( Profile p : prof )
		{
			if (p.getUID().equals(UID) == true )
			{
				prof.remove(p);
				return true;
			}
		}
		return false;
	}

	/**
	 * Console Display of profiles
	 * @param pro Profile List
	 */
	public void displayLobby( List<Profile> pro)
	{
		Iterator<Profile> it = pro.iterator();
		while (it.hasNext())
		{
			Profile p = it.next();
			p.printProf();
		}
		System.out.println("______________________");
	}

	/**
	 * Send an Object to the Server
	 * @param o An Object
	 */
	public void sendToServ(Object o)
	{
		try
		{
			output.reset();
			output.writeObject(o);
			output.flush();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			pinguPlayer = null;
		}
	}


	/**
	 * Wait for receive an Object NextTurn from Server
	 */
	public String getNextTurn()
	{
		NextTurn m;
		try
		{
			if ( pinguPlayer != null)
			{
				System.out.println("Attente de NextTurn");
				Object o = new Object();
				o = input.readObject();

				if ( o instanceof NextTurn )
				{
					m = (NextTurn) o;
					System.out.println("Recu : " + m);
					currentPlayerUID =  m.currentPlayerID;
					if (currentPlayerUID.equals(pinguPlayer.getProfile().getUID()))
					{
						play = true;
						possibleMoves = m.availableMoves;
					}
					else
						play = false;
					if ( m.lastPlayerInfo != null)
					{
						Move lastMove = m.lastPlayerInfo.lastMovePlayed;
						System.out.println("Position : " + lastMove.position+" Couleur : "+Color.getColorChar(lastMove.color));
						pinguGame.brutalPutCard(lastMove.position, ""+Color.getColorChar(lastMove.color));
					}
				}
				else if ( o instanceof RoundStart )
				{
					play=false;
					RoundStart rs = (RoundStart) o;
					roundNumber+=1;
					System.out.println("Creation Player");
					this.pinguPlayer = AgentFactory.createCLIPlayer(rs,this.currentProfile);
					System.out.println("Creation Game");
					Profile[] arrProf = (Profile[]) this.prof.toArray(new Profile[this.prof.size()]);
					this.pinguGame = new Game(rs.nbPlayers,arrProf,0,null);
					System.out.println("Game crée");
				}
				else if ( o instanceof EndGame)
				{

					System.out.println("Reception de classement ");
					endRank = DataObjectParser.parseEndGameIntoRanking((EndGame) o, pinguGame.getProfile()) ;
					pinguGame.setRanking(endRank);
					pinguGame.saveProfiles();
					close();
				}
				else
				{
					System.out.println(" ERREUR : EXEPTED NextTurn , " + o.getClass() + " provided" );
				}
			}
			return " NOPE ";

		}
		catch ( EOFException ee)
		{
			System.out.println("EOF");
			close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
			close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			close();
		}
		return null;
	}


	/**
	 * Translate an panel.user input into an Action understandable by Server
	 * @param  line Action
	 * @return      Action
	 */
	public String yourShotGUI(String line)
	{

		if ( pinguPlayer.getGR()  )                                                                             // Si le joueur est en vie on doit jouer un coup
		{
			boolean isValid = false;

			if (line.length() == 0)
				line = "40X";
			else
				line = line.toUpperCase();

			if ( pinguPlayer.isCard(line) || line.charAt(0) == 'D' ||  line.charAt(0) == 'Q'  || line.charAt(0) == 'H' )
			{
				isValid=true;
				if ( line.charAt(0) == 'D' || line.charAt(0) == 'Q'   )
					line+=pinguPlayer.getScore();
				else if (line.charAt(0) != 'H')
					line +='E';
			}
			else
				System.out.println(" !!  VOUS NE POSSEDEZ PAS CETTE CARTE !! ");
		}
		return line;

	}

	/**
	* Network Client Game Loop function
	*
	*/
	public boolean play(NetworkGameCLI panel )
	{
		NetworkGameCLI.roundAc = "FINISHED";
		NetworkGameCLI.roundPlace = "FINISHED";
		try {
			Thread.sleep(300);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//if ( !getPlay())
			getNextTurn();
		panel.flag=true;
		Move envoie;                     // INTERPR
		if ( getPlay() )
		{
			panel.flag=true;
			System.out.println(pinguPlayer.getProfile().getClassName());
			if ( pinguPlayer.getGR() && pinguPlayer.getProfile().getClassName().equals("Player") )
			{
				while (NetworkGameCLI.roundAc.equals("FINISHED") || NetworkGameCLI.roundPlace.equals("FINISHED"))
				{
					try
					{
						Thread.sleep(10);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					if ( getPlay() == false)
						return true;
				}
				if ( NetworkGameCLI.roundPlace.charAt(0) != 'D' && NetworkGameCLI.roundPlace.charAt(0) != 'Q')
				{
					envoie = new Move(Integer.valueOf(NetworkGameCLI.roundPlace),Color.getColorEnum(NetworkGameCLI.roundAc.charAt(0)));
					pinguPlayer.useCard(NetworkGameCLI.roundPlace+NetworkGameCLI.roundAc.charAt(0)+"E");
				}
				else
					envoie = pinguPlayer.useCard(NetworkGameCLI.roundPlace+NetworkGameCLI.roundAc.charAt(0)+"E");
			}
			else if (  pinguPlayer.getGR() && ! pinguPlayer.getProfile().getClassName().equals("Player"))
			{
				String played = pinguPlayer.play(pinguGame);
				System.out.println("Coup : " +  played + "!");
				envoie = pinguPlayer.useCard(played);
			}
			else 
				return true;
			
			NetworkGameCLI.roundAc = "FINISHED";
			NetworkGameCLI.roundPlace = "FINISHED";
			if ( validShot(envoie))
			{
				try
				{
					Thread.sleep(200);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				System.out.println("Position : " + envoie.position+" Couleur : "+Color.getColorChar(envoie.color));
				sendToServ(envoie);
				stopPlay();                             // FIN DE TOUR
			}
			panel.flag=true;
		}
		return true;

	}
	
	public boolean validShot(network.dataobjects.Move m)
	{
		if ( m.color == network.dataobjects.Color.UNDEFINED)
			return true;
		for ( int i : possibleMoves.get(m.color))
		{
			System.out.println(" possible : " + i);
			if ( i  == m.position)
				return true;
		}
		return false;
	}
	/**
	 * End the turn of a player
	 */
	public void stopPlay()
	{
		play = false;
	}


	public Profile [] getEndProfiles(String uid)
	{
		pinguGame.saveProfiles(uid);
		return pinguGame.getProfile();
	}

	public int getRoundNumber() 
	{
		return roundNumber;
	}	
	
}
