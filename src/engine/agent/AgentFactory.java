package engine.agent;

import stats.Profile;
import engine.game.Timer;
import network.dataobjects.RoundStart;

/**
 * @author Quentin Raymonadud
 */
public class AgentFactory
{
	// Class AgentFactory
	/**
	 * Create a Player ( Agent ) according to the profile (.className) given in paramenter
	 * @param  nbCards Integer : number of cards
	 * @param  p       Profile : p
	 * @return         Player
	 */
	public static Player createPlayer(  int nbCards, Profile p , Timer t)
	{
		try
		{
			String className = p.getClassName();
			Class concreateClass = Class.forName("engine.agent."+className);  																		// Récupération du type de la classe
			java.lang.reflect.Constructor agentConstructor = concreateClass.getDeclaredConstructor(int.class, Profile.class, Timer.class);
			return (Player) agentConstructor.newInstance(new Object[]{nbCards,p,t});					   								// Création de Player avec p et nbCards
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static Player createCLIPlayer(  RoundStart rs, Profile p )
	{
		try
		{
			String className = p.getClassName();
			Class concreateClass = Class.forName("engine.agent."+className);  																		// Récupération du type de la classe
			java.lang.reflect.Constructor agentConstructor = concreateClass.getDeclaredConstructor(RoundStart.class, Profile.class);
			return (Player) agentConstructor.newInstance(new Object[]{rs,p});					   								// Création de Player avec p et nbCards
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * create a Player ( Agent ) according to the className given in paramenter
	 * Usefull for testing new AI Profiles classname
	 * @param  className String : futur Agent's classname
	 * @return         Player
	 */
	public static Player testRobot(  String className )
	{
		try
		{
			if ( className.equals("Player") == false )
			{
				if ( className.equals(""))
					className = "Player";
				System.out.println(className);
				Class concreateClass = Class.forName("engine.agent."+className);  																		// Récupération du type de la classe
				return (Player) concreateClass.newInstance();					   								// Création de Player avec p et nbCards
			}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}

