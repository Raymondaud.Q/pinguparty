package engine.agent;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import engine.game.*;
import network.dataobjects.RoundStart;
import stats.PlayerRank;
import stats.Profile;

 /**
 * @author Quentin Raymondaud
 *
 */
public class Player implements Agent, Serializable
{
	//Class Player

	/**
	 * Player's pseudo
	 */
	protected String pseudo;			//Pseudonyme du joueur

	/**
	 * current card number
	 */
	protected int nbCards;				//Nombre de cartes en main

	/**
	 * Card number at the begining of each round
	 */
	protected int nbT; 					//Nombre de carte

	/**
	 * Cards
	 */
	protected String ownedCards;		//Les cartes en main pour le joueur ( couleurs )

	/**
	 * True if the player isn't out of the round
	 */
	protected boolean gameRound;		//Joue toujours dans la manche (true), (false) sinon

	/**
	 * Score = sum of cards left a the end of each Round
	 */
	protected int score;				//Nombre de carte qu'il reste au joueur dans sa main

	/**
	 * Number of Round win
	 */
	protected int roundScore;			//Nombre de manche gagnées

	/**
	 * Number of green cards played
	 */
	protected int greenCardsPlayed; 	//Nombre de cartes verte joué

	/**
	 * Player's Current profile
	 */
	protected Profile clt;				//Profil associé au joueur

	/**
	 * True if the player is not and instance of Player
	 */
	protected boolean bot;				//Robot ou non

	/**
	 * True if the player quit a game before the end
	 */
	protected boolean leaver;			//A quitté la partie;

	/**
	 * True if the player isn't local
	 */
	protected boolean remote=false;

	protected Timer blitzTimer = null;

	/**
	 * InputStream that receive data from the Server
	 */
	protected transient ObjectInputStream input = null ;

	/**
	 * OutputStream that send data to the Server
	 */
	protected transient ObjectOutputStream output = null ;

	public Player()
	{}

	/**
	 * Create a Player with specified cards profile and card number
	 *
	 * @param nbC 			integer : number of Card
	 * @param cards 		String  : Cards
	 * @param p   			Profile
	 */
	public Player( int nbC , String cards , Profile p , Timer t )
	{
		// Player Constructor
		clt = p;
		bot = false;
		pseudo = clt.getName();
		nbCards =  nbC;
		nbT = nbC;
		ownedCards =  cards ;
		score  = 0 ;
		roundScore = 0 ;
		gameRound = true;
		greenCardsPlayed = 0 ;
		blitzTimer = t;
	}
	
	public Player ( RoundStart rs , Profile p)
	{
		clt = p ;
		bot = false;
		pseudo = clt.getName();
		nbCards = rs.hand.length();
		nbT = rs.hand.length();
		ownedCards = rs.hand;
		score = 0;
		roundScore = 0;
		gameRound = true;
		greenCardsPlayed = 0;
		if ( rs.blitzTimer <= 0 );
			blitzTimer = new Timer(rs.blitzTimer);
	}

	/**
	 * Create a Player with specific Profile

	 * @param p   			Profile
	 */
	public Player(Profile p)
	{
		clt = p;
		bot = false;
		pseudo = clt.getName();
		nbCards = 0;
		nbT = 0;
		ownedCards = "";
		score = 0;
		roundScore = 0;
		gameRound = true;
		greenCardsPlayed = 0 ;
	}

	/**
	 * Create a Player with specific  card number and profile
	 *
	 * @param nbC 			integer : number of Card
	 * @param p   			Profile
	 */
	public Player(int nbC,Profile p, Timer t)
	{
		clt = p;
		bot = false;
		pseudo = clt.getName();
		nbCards = nbC;
		nbT = 0;
		ownedCards = "";
		score = 0;
		roundScore = 0;
		gameRound = true;
		greenCardsPlayed = 0 ;
		blitzTimer = t;
	}
	/**
	 * Create a Player with specified card number, cards , score , profile and number of handle win
	 *
	 *<p> Used for reConstruct the player for the new Round </p>
	 * @param nb 			Integer : number of Card
	 * @param cards 		String  : Cards
	 * @param score 		Integer : current Score
	 * @param cards 		String  : Cards
	 * @param p   			Profile
	 * @param handle 		Integer : number of round win
	 */
	public Player( int nb , String cards , int score , Profile p , int handle , Timer t)
	{
		clt = p;
		bot = false;
		pseudo = clt.getName();
		nbCards =  nb;
		nbT = nb;
		ownedCards =  cards ;
		this.score  = score ;
		this.roundScore = handle;
		gameRound = true;
		greenCardsPlayed = 0 ;
		blitzTimer = t;
	}

	/**
	 * Return the current ObjectOutputStream
	 *
	 * @return ObjectOutputStream : output
	 */
	public ObjectOutputStream getOutput()
	{
		return output;
	}

	/**
	 * Return the current ObjectInputStream
	 *
	 * @return ObjectInputStream : input
	 */
	public ObjectInputStream getInput()
	{
		return input;
	}

	public void setTimer(Timer t)
	{
		blitzTimer = t;
	}

	public void startTimer()
	{
		if ( blitzTimer != null )
			blitzTimer.start();
	}

	public void stopTimer()
	{
		if ( blitzTimer != null )
			blitzTimer.start();
	}

	public boolean updateTimer()
	{
		if ( blitzTimer != null )
			return blitzTimer.update();
		return false;
	}

	public int getTimeLeft()
	{
		if ( blitzTimer != null )
			return blitzTimer.getTimeLeft();
		return -1;
	}
	public float timeLeft()
	{
		if ( blitzTimer != null )
			return blitzTimer.timeLeft();
		return -1;
	}

	public boolean timerNull()
	{
		if ( blitzTimer == null)
			return true;
		return false;
	}

	/**
	 * Set the current ObjectInputStream
	 *
	 * @param ObjectInputStream : i
	 */
	public void setInput(ObjectInputStream i)
	{
		input = i;
	}

	/**
	 * Set the current ObjectOutputStream
	 *
	 * @param ObjectOutputStream : i
	 */
	public void setOutput(ObjectOutputStream o)
	{
		output = o;
	}

	/**
	 * Set the true to this.remote
	 */
	public void setRemote()
	{
		remote = true;
	}

	/**
	 * Return true if the player isn't a local player
	 *
	 * @return Boolean : remote
	 */
	public boolean getRemote()
	{
		return remote;
	}

	public void displayPlayer()
	{
		System.out.println(" | Pseudo : "+pseudo+" | Score : "+score+ " | roundScore : " + roundScore +" | Green Cards Played : "+ greenCardsPlayed + " | " + clt.getClassName());
	}

	public String getPlayerAttribute()
	{
		return (" | Pseudo : "+pseudo+" | Score : "+score+ " | roundScore : " + roundScore +" | Green Cards Played : "+ greenCardsPlayed + " | " + clt.getClassName() );
	}

	/**
	 * Return PlayerRank wich contains player's statistics
	 *
	 * @return PlayerRank
	 */
	public PlayerRank getRank()
	{
		return new PlayerRank(pseudo,this.getClass().getName(),score,roundScore,greenCardsPlayed,clt.getStat().getElo());
	}

	/**
	 * Increase greenCardsPlayed by one
	 */
	public void increaseGreen()
	{
		greenCardsPlayed +=1;
	}

	/**
	 * Return number of green cards played this game
	 *
	 * @return Integer : greenCardsPlayed
	 */
	public int getGreen()
	{
		return greenCardsPlayed;
	}

	/**
	 * Return the number of card set at the beginning of the Game
	 *
	 * @return Integer : nbT
	 */
	public int getNb()
	{
		return nbT;
	}

	/**
	 * Return true if the player isn't human
	 *
	 * @return Boolean : bot
	 */
	public boolean getBot()
	{
		return bot;
	}

	/**
	 * Set true to bot ( INHUMAN PLAYER )
	 *
	 */
	public void setBot()
	{
		bot = true;
	}

	/**
	 * Return true if the player has quit the Game
	 *
	 *	@return Boolean : leaver
	 */
	public boolean getLeaver()
	{
		return leaver;
	}

	/**
	 * Kill a player and indicate that he leaves the game
	 *
	 */
	public void setLeaver()
	{
		leaver = true;
		die();
	}

	/**
	 *  Return the number of round win
	 *
	 *	@return Integer : roundScore;
	 */
	public int getRoundScore()
	{
		return roundScore;
	}

	/**
	 *  Set roundScore to x
	 *
	 *	@param x Integer
	 */
	public void setRoundScore(int x)
	{
		roundScore = x;
	}

	/**
	 *  Return the score
	 *
	 *	@return Integer : score;
	 */
	public int getScore()
	{
		return score;
	}

	/**
	 *  Set Score to x
	 *
	 *	@param x Integer
	 */
	public void setScore(int i)
	{
		score = i ;
	}

	/**
	 *  Return the current cards
	 *
	 *	@return String : ownedCards
	 */
	public String getCards()
	{
		return ownedCards;
	}

	/**
	 *  Set cards
	 *
	 *	@param String : card
	 */
	public void setPlayerCards(String card)
	{
		ownedCards = card;
	}

	/**
	 * 	Add a/many card.s to ownedCards
	 *
	 *	@param String : card
	 */
	public void AddCard(String card)
	{
		ownedCards += card;
	}

	/**
	 *  Return the current Pseudo
	 *
	 *	@return String : pseudo
	 */
	public String getPseudo()
	{
		return pseudo;
	}


	/**
	 *  Set pseudo
	 *
	 *	@param String : p
	 */
	public void setPseudo(String p)
	{
		pseudo = p;
	}

	/**
	 *  Return the current profile
	 *
	 *	@return Profile : clt
	 */
	public Profile getProfile()
	{
		return clt;
	}

	/**
	 *  Set Profile
	 *
	 *	@param Profile : p
	 */
	public void setProfile(Profile p)
	{
		clt = p;
	}

	/**
	 *  Calculate and set the current Score
	 *
	 */
	public void setScore()
	{
		for ( int i = 0 ; i < ownedCards.length() ; i++)
		{
			if ( ownedCards.charAt(i) != ' ')
			{
				score+=1;
			}
		}
	}


	/**
	 *  Return the current card Number
	 *
	 *	@return Integer : nbCards
	 */
	public int getNbC()
	{
		return nbCards;
	}

	/**
	 *  Set current card number at x
	 *
	 *	@param Integer : x
	 */
	public void setNbC(int x)
	{
		nbCards = x;
	}

	/**
	 *  Kill a player and calculate his score
	 *
	 */
	public void die()
	{
		if ( getGR() )
			setScore();

		gameRound=false;
	}


	/**
	 *  Return true if the player is alive in this round
	 *
	 *	@return Boolean : gameRound
	 */
	public boolean getGR()
	{
		return gameRound;
	}

	/**
	 *  Set a player Alive for this round (gameRound = true ) and return this
	 *
	 *	@return Player : this
	 */
	public Player setGR()
	{
		gameRound = true;
		return this;
	}

	/**
	 *  Return true if the card in the action(String) is in ownedCards
	 *
	 *	@return Boolean
	 */
	public boolean isCard( String value)
	{
		if ( value.length() >= 3 )
		{
			for ( int i = 0 ; i < ownedCards.length() ; i++)
			{
				if ( ownedCards.charAt(i) == value.charAt(2) && value.charAt(2) != 'x')
				{
					return true;
				}
			}
		}
		else if (value.length() == 2)
		{
			for ( int i = 0 ; i < ownedCards.length() ; i++)
			{
				if ( ownedCards.charAt(i) == value.charAt(1) && value.charAt(1) != 'x')
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public Player useCard(network.dataobjects.Move m)
	{
		int index =  ownedCards.indexOf((""+Color.getColorChar(m.color)));
		if ( index == -1 )
				return this;
		else
		{
			if ( (""+Color.getColorChar(m.color)).toUpperCase().equals('G'))
				greenCardsPlayed+=1;
			nbCards -= 1;	
			ownedCards = replaceCharAt(ownedCards,index,' ');
		}
		//setScore();
		return this;
	}

	/**
	 *  Delete the card in the action(Str) from the currend deck, and increase greenCards if the card was green
	 *
	 *	@return Player : this
	 */
	public network.dataobjects.Move useCard( String value)
	{
		network.dataobjects.Move m=null ;
		System.out.println(value);
		if ( value.charAt(0) == 'D')
			return m = new network.dataobjects.Move(40,Color.getColorEnum(value.charAt(0)));
		else if ( value.charAt(0) == 'Q')
			return null;
		if ( value.length() > 3 )
		{
			for ( int i = 0 ; i < ownedCards.length() ; i++)
			{
				if ( ownedCards.charAt(i) == value.charAt(2) && value.charAt(2) != 'X')
				{
					m = new network.dataobjects.Move(Integer.parseInt(""+value.charAt(0)+value.charAt(1)),Color.getColorEnum(value.charAt(2)));
					if ( value.charAt(2) == 'G' )
						greenCardsPlayed+=1;

					nbCards -= 1;

					ownedCards = replaceCharAt(ownedCards,i,' ');
					return m;
				}
			}
		}
		else
		{
			for ( int i = 0 ; i < ownedCards.length() ; i++)
			{
				if ( ownedCards.charAt(i) == value.charAt(1) && value.charAt(1) != 'x')
				{
					m = new network.dataobjects.Move(Integer.parseInt(""+value.charAt(0)),Color.getColorEnum(value.charAt(1)));
					if ( value.charAt(1) == 'G' || value.charAt(1) == 'g')
						greenCardsPlayed+=1;

					nbCards -= 1;

					ownedCards = replaceCharAt(ownedCards,i,' ');
					//setScore();
					return m;
				}
			}
		}
		return m;
	}


	/**
	 *  return the cards.charAt(y) of cards
	 *
	 *	@return char
	 */
	public char getCardI(int y)
	{
		int x = 0 ;
		int index = 0 ;
		for ( int i = 0 ; i < ownedCards.length() ; i ++)
		{
			if (ownedCards.charAt(i) != ' ' )
			{
				x += 1 ;
				index = i;
			}
			if ( x == y )
			{
				System.out.println(ownedCards.charAt(i));
				return ownedCards.charAt(i);
			}
		}
		System.out.println(ownedCards.charAt(index));
		return ownedCards.charAt(index);

	}

	/**
	 *  Empty play method
	 */
	@Override
	public String play(Game g) // CONS
	{
		return "";
	}

	/**
	 *  Method Play for a human Player in GUI version

	 *	@param Game : g ( Game that the player is involved in)
	 *  @param String : roundAc ( Action given : ex : 23RE )
	 *	@return String : action
	 */
	public String play(Game g,String roundAc) // GUI
	{
		String line = roundAc;
		
		if ( getGR() )																				// Si le joueur est en vie on doit jouer un coup
		{
			boolean isValid = false;
			if( !isValid )																					// On s'assure que le joueur possède la carte qu'il veut jouer
			{
				if (line.length() != 0)
					line = line.toUpperCase();
				else
					line = "40X";
				if (  isCard(line) || line.charAt(0) == 'D' || line.charAt(0) == 'Q' || line.charAt(0) == 'H'  )
				{
					if ( line.charAt(0) == 'D' || line.charAt(0) == 'Q'  )
					{
						isValid=true;
						line+=getScore();
						if ( line.charAt(0) == 'Q'  )
							return "QQQ";
						else
							return "DDD";
					}
					else
					{
						int place = 0;
						String value ="";
						line += "E";
						try
						{
							if (line.length() > 3)
							{
								String p = String.valueOf(line.charAt(0)) + String.valueOf(line.charAt(1));
								place  = Integer.parseInt(p);
								value =  Character.toString(line.charAt(2));
							}
							else if ( line.length() <= 3)
							{
								String pl = String.valueOf(line.charAt(0));
								place  = Integer.parseInt(pl);
								value =  Character.toString(line.charAt(1));
							}
						}
						catch( NumberFormatException e)
						{
							place = 40;
							value = "X";
						}

						if ( g.putCard(place,value))
						{
							isValid=true;
						}
						else
						{
							return "RRR";															// Si le coup est invalide alors le joueur rejoue
						}
					}
				}
			}
			return line;
		}
		return line;
	}
	
	
	/**
	 *	Remoplace a char at a position pos in the string s by the char c
	 *
	 * @param  String s   The Str
	 * @param  Integer pos index of the replace char
	 * @param  char c   the new char
	 * @return  String : the new Chain
	 */
	public static String replaceCharAt(String s, int pos, char c)
	{
   		return s.substring(0,pos) + c + s.substring(pos+1);
	}
	/**
	 * Call SetLeaver() on the player[i] in player
	 *
	 * @param  player   Array of player
	 * @param  i        index of the player concerned
	 * @param  nbPlayer size of player
	 * @return          Boolean
	 */
	public static  boolean setPlayerLeaver(Player [] player , int i , int nbPlayer)
	{
		if ( i < nbPlayer && i > -1  && player[i] != null  )
		{
			player[i].setLeaver();
			return true;
		}
		return false;
	}

	/**
	 * Call die() on the player[i] in player
	 *
	 * @param  player   Array of player
	 * @param  i        index of the player concerned
	 * @param  nbPlayer size of player
	 * @return          Boolean
	 */
	public static boolean setPlayerDead(Player [] player , int i , int nbPlayer)
	{
		if ( i < nbPlayer && i > -1  && player[i] != null && player[i].getGR() )
		{
			player[i].die();
			return true;
		}
		return false;
	}

	/**
	 *
	 * Set pseudo of Player[] p by String[] ps ( pseudo )
	 * @param p   Array of player
	 * @param ps   Array of pseudo
	 * @param size nbPlayer
	 */
	public static void setPseudo(Player [] p , String [] ps , int size )
	{
		for ( int i = 0 ; i < size ; i ++ )
		{
			p[i].setPseudo(ps[i]);
		}
	}

	/**
	 *
	 * Set roundScore of Player[] p by Integer[] ps ( Roundscore )
	 * @param p   Array of player
	 * @param ps   Array of score
	 * @param size nbPlayer
	 */
	public static void setRound(Player [] p , int [] ps , int size )
	{
		for ( int i = 0 ; i < size ; i ++ )
		{
			p[i].setRoundScore(ps[i]);
		}
	}

	/**
	 *
	 * Set Score of Player[] p by Integer[] ps ( score )
	 * @param p   Array of player
	 * @param ps   Array of score
	 * @param size nbPlayer
	 */
	public static void setScore(Player [] p , int [] ps , int size )
	{
		for ( int i = 0 ; i < size ; i ++ )
		{
			p[i].setScore(ps[i]);
		}
	}

/**
 *	Return an Array of Profile thanks to an Array of Player
 *
 * @param  p    Array Player
 * @param  size size of array
 * @return      Array of profiles
 */
	public static Profile[] getProfile(Player [] p ,int size )
	{
		Profile [] pP = new Profile[size];
		for ( int i = 0 ; i < size ; i ++ )
		{
			pP[i] = p[i].getProfile();
		}
		return pP;
	}

/**
 * Return Player[i]
 * @param  player   Array of Player
 * @param  i        Index of the desired Player
 * @param  nbPlayer Size of player
 * @return          Player
 */
	public static Player getPlayer(Player [] player, int i, int nbPlayer)						// Gestion de deux pseudo identiques a prévoir !
	{
		if ( i < nbPlayer && player[i] != null )
			return player[i];
		else
			return null;
	}


/**
 * set the player[i] reference to the Player p
 * @param  player   Player array
 * @param  p        Player p
 * @param  i        Index : Integer
 * @param  nbPlayer Size of player
 * @return          Player array
 */
	public static Player[] setPlayer(Player [] player, Player p , int i, int nbPlayer)			// Gestion de deux pseudo identiques a prévoir !
	{
		if ( i < nbPlayer && player[i] != null )
		{
			player[i] = p;
			return player;
		}
		else
			return null;

	}

 /**
  * Set the score of the Player i at p
  * @param  player   Player array
  * @param  i        index of selectedplayer
  * @param  p        score : Integer
  * @param  nbPlayer size of array
  * @return          Boolean
  */
	public static boolean setPlayerScore(Player[] player,int i, int p, int nbPlayer)			// Gestion de deux pseudo identiques a prévoir !
	{
		if ( i < nbPlayer && player[i] != null)
		{
			player[i].setScore(p);
			return true;
		}
		else
			return false;
	}

/**
 * Get the pseudo of the Player i
 * @param  player   Player array
 * @param  i        Index of desired player pseudo
 * @param  nbPlayer Size of array
 * @return          String : Pseudo
 */
	public static String getPlayerPseudo(Player [] player, int i, int nbPlayer)
	{
		if ( i < nbPlayer && player[i] != null )
			return player[i].getPseudo();
		else
			return null;

	}
/**
 * Set all player alive if they had not left the Game
 * @param  player   Player array
 * @param  nbPlayer Size of array
 * @return          Array  of Player
 */
	public static Player[] setAlive(Player [] player, int nbPlayer)
	{
		Player [] newPlayer = new Player[nbPlayer] ;
		for ( int i  = 0 ; i < nbPlayer ; i ++ )
		{
			if ( ! player[i].getLeaver()  )
				newPlayer[i] = player[i].setGR();
			else
				newPlayer[i] = player[i];
		}
		return newPlayer;

	}

	/**
	 *  Array of  String of each Player pseudo
	 * @param  player   Player array
	 * @param  nbPlayer Size of array
	 * @return          Array of String
	 */
	public static String[] getPlayerPseudo(Player [] player, int nbPlayer)
	{
		String [] pP = new String[nbPlayer];

		for (int i = 0 ; i < nbPlayer ; i ++)
		{
			pP[i]="";
			pP[i]+=player[i].getPseudo();
		}
		return pP;
	}


	/**
	 *  Array of  score of  each Player
	 * @param  player   Player array
	 * @param  nbPlayer Size of array
	 * @return          Array of Integer
	 */
	public static int [] getPlayerScore(Player[] player, int nbPlayer)
	{
		int [] pS = new int[nbPlayer];

		for (int i = 0 ; i < nbPlayer ; i ++)
		{
			pS[i]=0;
			pS[i]+=player[i].getScore();
		}
		return pS;
	}

/**
 *  Array of  number of cards owned by each Player
 * @param  player   Player array
 * @param  nbPlayer Size of array
 * @return          Array of Integer
 */
	public static int [] getPlayerNbC(Player[] player, int nbPlayer)
	{
		int [] pN = new int[nbPlayer];

		for (int i = 0 ; i < nbPlayer ; i ++)
		{
			pN[i]=player[i].getNbC();
		}
		return pN;
	}

/**
 * Array of number of green cards played by each Player
 * @param  player   Player array
 * @param  nbPlayer size of Array
 * @return          Array of Integer
 */
	public static int [] getPlayerNbGreen(Player[] player, int nbPlayer)
	{
		int [] pG = new int[nbPlayer];

		for (int i = 0 ; i < nbPlayer ; i ++)
		{
			pG[i]=0;
			pG[i]=player[i].greenCardsPlayed;
		}
		return pG;
	}


	public static String[] getAllCards(Player[] player,int nbPlayer,Profile[] p) {
		String res[] = new String[nbPlayer];
		for (int i = 0; i < nbPlayer; i++) 
		{
			for(int j=0;j<p.length;j++)
			{
				if(player[i].getProfile().getUID().equals(p[j].getUID()))
				{
					res[j]=player[i].getCards();
				}
			}
			System.out.println("pseudo joueur : "+player[i].getPseudo());
		}
		return res;
	}
	
	/**
	 *Return the list of Profile of the Players
	 * @param  player  	Player array
	 * @param  nbPlayer size of array
	 * @return          Array of Profile
	 */
	public static Profile [] getPlayerProfile(Player [] player, int nbPlayer )
	{
		Profile [] pP = new Profile[nbPlayer];

		for (int i = 0 ; i < nbPlayer ; i ++)
		{
			pP[i]=new Profile();
			pP[i]=player[i].getProfile();
		}
		return pP;
	}

/**
 * Return an array of number of round wins by each player
 *
 * @param  player   Player array
 * @param  nbPlayer size of arry
 * @return          Array of Integer
 */
	public static int [] getPlayerHandle (Player [] player, int nbPlayer)
	{
		int [] pH = new int[nbPlayer];

		for (int i = 0 ; i < nbPlayer ; i ++)
		{
			pH[i]=0;
			pH[i]+=player[i].getRoundScore();
		}
		return pH;
	}

/**
 * Return a String, cards of a player i
 *
 * @param  player   Player array
 * @param  i        index
 * @param  nbPlayer size of array
 * @return          String : cards
 */
	public static  String getCard(Player[] player , int i , int nbPlayer)								// Accesseur, recupère toutes les cartes de la partie
	{
		if (   i < nbPlayer && player[i] != null )
			return player[i].getCards();
		else
			return null;
	}

/**
 * Increase by one the roundScore of a Player
 * @param  player   Player array
 * @param  i        Index of player
 * @param  nbPlayer size of array
 * @return          Boolean
 */
	public static  boolean setPlayerHandle (Player [] player, int i , int nbPlayer )			// Gestion de deux pseudo identiques a prévoir !
	{
		if (  i < nbPlayer && player[i] != null)
		{
			player[i].setRoundScore(player[i].getRoundScore()+1);
			return true;
		}
		else
		{
			System.out.println(" Action Impossible");
			return false;
		}
	}

}
