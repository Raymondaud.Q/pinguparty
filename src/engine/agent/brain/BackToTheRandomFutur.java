package engine.agent.brain;

import engine.game.*;
import engine.agent.brain.logic.*;
import java.util.ArrayList;

/**
 * @author Quentin Raymondaud
 * @author Imad Iguichi
 * Heuristical Logic
 */

public class BackToTheRandomFutur 
{
	private Game g;
	private String ownedCards;
	private Simulator simulation;
	private int difficulty = 1;
	
	public BackToTheRandomFutur(int dif)
	{
		difficulty = dif;
		simulation = new Simulator(dif);
	}

	public void initSimulation(Game g,String oCards, int greenCardsPlayed)
	{
		float time = 6000;
		this.g = g;
		if ( g.getPlayer(g.getPlayerIndex(),g.getNbPlayer()).timerNull() == false)
			time = g.getPlayer(g.getPlayerIndex(),g.getNbPlayer()).timeLeft();
		ownedCards = oCards;
		simulation.setState(time,g.getPartyBoard(),generateOthersCard(),ownedCards,greenCardsPlayed,g.getNbPlayer());
	}
	public Move getResult()
	{
		return simulation.simulate();
	}
	
	public ArrayList<String> generateOthersCard()
	{
		return PinguLogic.generateOthersCard(g,ownedCards);
	}

	public Move[] getAvailableMoves()
	{
		return simulation.availableMoves();
	}

}