package engine.agent.brain.logic;
import engine.game.*;

import java.util.ArrayList;
import java.util.Collections;


public class Simulator implements Cloneable
{
	private String [][] fakeParty;
	private int nbPlayer = 0 ;
	private String fakeMonoHand = "";
	private String fakeHand = "";
	private int fakeGreenCardsPlayed = 0;
	private int nbCardsPerPlayer = 0;
	private int nbCards = 0;
	private int othersNbCards = 0;
	private RankedMove [] toRank;
	int nb = 20;
	
	public Simulator( int dif )
	{
		nbPlayer = 0 ;
		fakeMonoHand = "";
		fakeHand = "";
		fakeGreenCardsPlayed = 0;
		nbCardsPerPlayer = 0;
		nbCards = 0;
		othersNbCards = 0;
		nb = dif*10;
	}
	

	public Object clone() throws CloneNotSupportedException 
    { 
        return super.clone(); 
    } 

    public void initRankedMove()
    {
    	Move [] possies = availableMoves();
		toRank = new RankedMove[possies.length];
    	int counter = 0;
		for ( Move m : possies)
		{
			if ( ! isPresent(m,toRank) )
			{
				toRank[counter] = new RankedMove(m.getPosition(),m.getColor(),0f);
				counter+=1;
			}
		}
    }
    
    public boolean isPresent(Move m , RankedMove [] a)
    {
    	for ( RankedMove t : a )
    	{
    		if ( t != null)
	    		if ( t.getColor() == m.getColor() && t.getPosition() == m.getPosition() )
	    			return true;
    	}
    	return false;
    }



	public void setState( float timer , String[][] party, ArrayList<String> hand , String userHand , int greenCardsPlayed , int nb)
	{
		fakeParty = new String[party[0].length][party[0].length];
		for ( int i = 0 ; i < party[0].length ; i ++)
		{

		 	for ( int j = 0 ; j < party[0].length ; j ++)
			{
					fakeParty[i][j] = party[i][j];		 
			}
		}
		Collections.shuffle(hand);
		for ( String c  : hand )
			fakeMonoHand+=c;
		
		if ( timer <= 5100)
			this.nb=10*(14-userHand.length()-1)+20; // Ici on gère si il  a plus de temps pour faire beaucoup de simulations
		// Gestion dynamique de la profondeur critique en fonction du nombre de carte en main
		fakeHand = userHand;
		fakeGreenCardsPlayed = greenCardsPlayed;
		nbPlayer = nb;
	}

	public Move simulate()
	{
		Simulator randomDev = new Simulator(nb);
		calcTotalCards();
		calcNbCards();
		calcOthersNbCards();
		initRankedMove();		
		try
		{
			for ( int  i = 0 ; i < nb  ; i ++ )
			{
				for ( RankedMove m : toRank )
				{
					if ( m != null )
					{
						randomDev = (Simulator) this.clone();
						randomDev.putCard(m.getPosition(),""+m.getColor(),false);
						for ( int o = 0 ; o < PinguLogic.random(1,nbPlayer) ; o ++ )
						{
							Move [] othersPossies = randomDev.availableMoves(true);
							Move oM = null ;
							if ( othersPossies.length-1 > 0 )
							{
								oM = othersPossies[PinguLogic.random(0,othersPossies.length-1)];
								if ( randomDev.canPutCard(oM.getPosition(),""+oM.getColor(),true))
								{
									randomDev.putCard(oM.getPosition(),""+oM.getColor(),true);
									randomDev.simulate();
								}
							}
						}
						m.increaseValue(randomDev.estimate(m).getValue()/nb);
					}

				}
			}
			for ( RankedMove rM : toRank)
				System.out.println("Value : " + rM );
			return bestMove();
		}
		catch (Exception e)
		{e.printStackTrace();}

		return null;
	}

	public RankedMove estimate( RankedMove toSetRank )
	{
		calcNbCards();
		calcOthersNbCards();
		toSetRank.increaseValue(  availableMoves().length*availableMoves().length + PinguLogic.countHand(fakeHand)[PinguLogic.getNumFunction(toSetRank.getColor())]  + othersNbCards ) ;
		toSetRank.increaseValue(-(fakeGreenCardsPlayed/nb));
		toSetRank.increaseValue(-toSetRank.getPosition()/nb);
		return toSetRank;
	}

	public Move bestMove()
	{
		double max = -10000 ;
		Move best = null;
		double cValue = 0;
		for ( RankedMove m : toRank )
		{
			if ( m != null)
			{

				cValue = m.getValue();
				if ( cValue >= max )
				{
					max = cValue;
					best = m;
				}
			}
		}
		return best;
	}

		
	/**
	 * Try to put the card value to place
	 * Return true if it's possible , false otherwise
	 * @param  place A Specific place
	 * @param  value String : a single card ( char )
	 * @return       Boolean
	 */
	public boolean putCard(int place, String value, boolean others)			// Pose une carte en fonction des regles du jeu
	{

		if ( (others && fakeMonoHand.indexOf(value) == -1 )|| (others == false  && fakeHand.indexOf(value) == -1))
			return false; 
		
		int cmp = 0;
		int x = 0;
		int y = 0;

		for (int i = 0 ; i < 8 ; i++ )
		{
			for (int j = 0 ; j < 8 ; j++ )
			{
				String pb = fakeParty[i][j];
				if ( pb.charAt(0) != 'X')
				{
					if ( cmp == place)
					{
						x = i;
						y = j;
					}
					cmp += 1;
				}
			}
		}

		if ( fakeParty[x][y] == "E" && ( value.charAt(0) == 'R' || value.charAt(0) == 'G' || value.charAt(0) == 'P' || value.charAt(0) == 'Y' || value.charAt(0) == 'B') )
		{
			if ( nbPlayer == 2 && x == 6 )
			{
				fakeParty[x][y] = value;
				useCard(value,others);
				return true;
			}
			else if ( nbPlayer > 2 && x == 7 )
			{
				fakeParty[x][y] = value;
				useCard(value,others);
				return true;
			}

			else if (  x < 7 )
			{
				if ( ( fakeParty[x+1][y].charAt(0) == value.charAt(0) && fakeParty[x+1][y+1].charAt(0) != 'E') || ( fakeParty[x+1][y+1].charAt(0) == value.charAt(0) &&  fakeParty[x+1][y].charAt(0) != 'E'  ) )
				{
					fakeParty[x][y] = value;
					useCard(value,others);
					return true;
				}
			}
			return false ;
		}
		return false;
	}

	/**
	 * Indicate if we can put a card
	 * @param  place A Specific place
	 * @param  value String : a single card ( char )
	 * @return       Boolean
	 */
	public boolean canPutCard(int place, String value, boolean others )			// Pose une carte en fonction des regles du jeu
	{
		if ( others )
			return PinguLogic.canPutCard(fakeParty,fakeMonoHand,nbPlayer,place,value);
		else
		{
			return PinguLogic.canPutCard(fakeParty,fakeHand,nbPlayer,place,value);
			
		}
	}

	public Move[] availableMoves()
	{
		return PinguLogic.getPossibleMoves(fakeParty , fakeHand , nbPlayer );
	}

	public Move[] availableMoves( boolean others)
	{
		if ( others )
			return PinguLogic.getPossibleMoves(fakeParty , fakeMonoHand , nbPlayer );
		else
			return PinguLogic.getPossibleMoves(fakeParty , fakeHand , nbPlayer );
	}

	public String useCard(String c ,boolean others)
	{
		if ( others)
		{
			fakeMonoHand = fakeMonoHand.substring(0,fakeMonoHand.indexOf(c)) + ' ' + fakeMonoHand.substring(fakeMonoHand.indexOf(c)+1);
			return fakeMonoHand;
		}
		if ( c.charAt(0) == 'G')
		{
			fakeGreenCardsPlayed +=1;
		}
		fakeHand = fakeHand.substring(0,fakeHand.indexOf(c)) + ' ' + fakeHand.substring(fakeHand.indexOf(c)+1);
		calcNbCards();
		return fakeHand;
	}

	public void calcTotalCards()
	{
		if ( nbPlayer ==  2)
			nbCardsPerPlayer = 14;
		else if (nbPlayer == 5)
		{
			nbCardsPerPlayer = 7;
		}
		else
			nbCardsPerPlayer = 36/nbPlayer;
		if ( nbPlayer ==  2)
			nbCardsPerPlayer = 14;
		else if (nbPlayer == 5)
		{
			nbCardsPerPlayer = 7;
		}
		else
			nbCardsPerPlayer = 36/nbPlayer;
	}

	public int calcNbCards()
	{
		nbCards = 0 ;
		for ( int i = 0 ; i < fakeHand.length() ; i++)
		{
			if ( fakeHand.charAt(i) != ' ')
				nbCards +=1;
		}
		return nbCards;
	}

	public int calcOthersNbCards()
	{
		othersNbCards = 0 ;
		for ( int i = 0 ; i < fakeMonoHand.length() ; i++)
		{
			if ( fakeMonoHand.charAt(i) != ' ')
				nbCards +=1;
		}
		return othersNbCards;
	}
}
