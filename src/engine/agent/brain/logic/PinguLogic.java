package engine.agent.brain.logic;

import engine.game.*;
import java.util.ArrayList;
import java.util.Random;


public class PinguLogic
{

	public static int[] countHand(String ownedCards) // Compte le nombre de carte de chaques couleurs
	{
		int [] cnt ={0,0,0,0,0};
		//String ownedCard = g.getCards()
		for ( int a = 0 ; a < ownedCards.length() ; a ++)
		{
			if (ownedCards.charAt(a) == 'B')
				cnt[0]+=1;
			else if (ownedCards.charAt(a) == 'Y')
				cnt[1]+=1;
			else if (ownedCards.charAt(a) == 'P')
				cnt[2]+=1;
			else if (ownedCards.charAt(a) == 'G')
				cnt[3]+=1;
			else if (ownedCards.charAt(a) == 'R')
				cnt[4]+=1;
		}
		return cnt;
	}

	

	public static int[] countBoard(Game g) // Compte l'effectif des couleurs sur le plateau
	{
		int [] cnt ={0,0,0,0,0};
		String [][] board = g.getPartyBoard();
		for ( int i = 0 ; i < board[0].length ; i ++)
		{
			for ( int j = 0 ; j < board[0].length ; j ++ )
			{
				if (board[i][j].charAt(0) == 'B')
					cnt[0]+=1;
				else if (board[i][j].charAt(0) == 'Y')
					cnt[1]+=1;
				else if (board[i][j].charAt(0) == 'P')
					cnt[2]+=1;
				else if (board[i][j].charAt(0) == 'G')
					cnt[3]+=1;
				else if (board[i][j].charAt(0) == 'R')
					cnt[4]+=1;
			}
		}
		return cnt;
	}

	public static int random(int min, int max) // Return number between min max
	{

		if (min >= max) 
		{
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	public static int[] cardInOthersHand(Game g , String ownedCards) // Deduce left cards
	{
		int [] cnt ={0,0,0,0,0};
		int [] myCount = countHand(ownedCards);
		int [] boardCount = countBoard(g);

		for (int i  = 0 ; i < cnt.length ; i++)
		{
			if ( i != 3)
				cnt[i] = 7 - ( myCount[i] + boardCount[i] );
			else 
				cnt[i] = 8 - ( myCount[i] + boardCount[i] );
		}

		return cnt;
	}

	public static ArrayList<String> generateOthersCard(Game g , String ownedCards) // generate others cards
	{
		int [] cnt = cardInOthersHand (g,ownedCards);

		ArrayList<String> othersCards = new ArrayList<String>();	
		int counter = 0 ;
		for ( int i : cnt )
		{
			for ( int j = 0 ; j < i ; j ++ )
			{
				othersCards.add(enumFunction(counter));
				counter+=1;
			}
		}

		return othersCards;
	}

	public static String getPatronus(String cards) // genere un resumé des cartes
	{
		int [] occ = countHand(cards);
		String patronus ="";
		int enumCnt = 0;
		for ( int i : occ)
		{
			if ( i > 0)
				patronus+=enumFunction(enumCnt);
			enumCnt +=1;
		}
		return patronus;
	}


	public static Move[] getPossibleMoves(String[][]board , String cards , int nbPlayer ) // Find possible moves according to a board , a hand a player number
	{
		ArrayList<Move> possibilities = new ArrayList<Move>();
		String patronus = PinguLogic.getPatronus(cards);		
		int counter = 0 ;
		for ( int i = 0 ; i < board[0].length ; i ++)
		{
			for ( int j = 0 ; j < board[0].length ; j ++ )
			{
				for ( int c = 0 ; c<patronus.length() ; c++ )
				{
					if (PinguLogic.canPutCard(board,cards,nbPlayer,counter,""+patronus.charAt(c)))
						possibilities.add(new Move(counter,patronus.charAt(c)));	
				}
				if ( board[i][j].charAt(0) != 'X')
				{
					counter+=1;
				}
			}
		}

		Move[] moves = new Move[possibilities.size()];
		counter = 0 ;
		for ( Move m : possibilities)
		{
			moves[counter]=m;
			counter+=1;
		}
		return moves;

	}

	public static boolean canPutCard( String [][] fakeParty, String  fakeHand , int nbPlayer , int place, String value)			// Indique si on peux pose une carte en fonction des regles du jeu
	{
		if ( fakeHand.indexOf(value) == -1)
			return false; 
		 
		int cmp = 0;
		int x = 0;
		int y = 0;

		for (int i = 0 ; i < 8 ; i++ )
		{
			for (int j = 0 ; j < 8 ; j++ )
			{
				String pb = fakeParty[i][j];
				if ( pb.charAt(0) != 'X')
				{
					if ( cmp == place)
					{
						x = i;
						y = j;
					}
					cmp += 1;
				}
			}
		}
		//System.out.println(place+" "+value+" "+nbPlayer+" "+x+" "+y+ " "+ fakeParty[x][y]);
		if ( fakeParty[x][y].equals("E") && ( value.charAt(0) == 'R' || value.charAt(0) == 'G' || value.charAt(0) == 'P' || value.charAt(0) == 'Y' || value.charAt(0) == 'B') )
		{
			//System.out.println(nbPlayer+" "+x);
			if ( nbPlayer == 2 && x == 6 )
			{
				return true;
			}
			else if ( nbPlayer > 2 && x == 7 )
			{
				return true;
			}

			else if (  x < 7 )
			{
				if ( ( fakeParty[x+1][y].charAt(0) == value.charAt(0) && fakeParty[x+1][y+1].charAt(0) != 'E') || ( fakeParty[x+1][y+1].charAt(0) == value.charAt(0) &&  fakeParty[x+1][y].charAt(0) != 'E'  ) )
				{
					return true;
				}
			}
			return false ;
		}
		return false;
	}

	public static String enumFunction(int max) // Return the color string for each value
	{
		if ( max == 0 )
			return "B";
		else if ( max == 1 )
			return "Y";
		else if ( max == 2 )
			return "P";
		else if ( max == 3 )
			return "G";
		else if ( max == 4 )
			return "R";
		return "DDD";
	}

	public static int getNumFunction(char l) // Reverse enum
	{
		if ( l == 'B' )
			return 0;
		else if ( l == 'Y' )
			return 1;
		else if ( l == 'P' )
			return 2;
		else if ( l == 'G')
			return 3;
		else if ( l == 'R' )
			return 4;
		return -1;
	}


	public static int maxOf(int[] list) // MAXIMUM from list
	{
		int max = 0;
		int indexMax = 0;
		int index = 0;
		for ( int i : list)
		{
			if ( i > max )
			{
				max = i;
				indexMax = index;
			}

			index +=1;
		}
		return indexMax;
	}

}