package engine.agent.brain;

import engine.game.*;
import engine.agent.brain.logic.*;

import java.util.ArrayList;

/**
 * @author Quentin Raymondaud
 * @author Imad Iguichi
 * Heuristical Logic
 */

public class ZombieBlock 
{
	private Game g;
	private String ownedCards;

	public void setGame(Game g,String oCards)
	{
		this.g = g;
		ownedCards = oCards;
	}


	public int[] countHand()
	{
		return PinguLogic.countHand(ownedCards);
	}

	public int[] countBoard()
	{
		return PinguLogic.countBoard(g);
	}

	public int[] cardInOthersHand()
	{
		return PinguLogic.cardInOthersHand(g,ownedCards);
	}

	public String cardToPlay()
	{
		int max = PinguLogic.maxOf(countHand());
		return PinguLogic.enumFunction(max);
	}

	public String cardToBlock()
	{
		int max = PinguLogic.maxOf(cardInOthersHand());
		return PinguLogic.enumFunction(max);
	}

	public BlockMove[] findLateralBlockPlace()
	{
		String [][] board = g.getPartyBoard();

		ArrayList<String> placesCards = new ArrayList<String>();
		ArrayList<Integer> places =  new ArrayList<Integer>();
		ArrayList<String> targets =  new ArrayList<String>();

		int cnt = 0;
		for ( int i = 0 ; i < board[0].length ; i ++ )
		{
			for ( int j = 0 ; j < board[0].length ; j ++ )
			{
				if (board[i][j].charAt(0) != 'X' )
				{
					if ( j == i && ( board[i][j].charAt(0) == 'E' && ( i < board.length -1 && board[i+1][j].charAt(0) != 'E' ) && (j < board.length -1 && board[i+1][j+1].charAt(0) != 'E' 
							&& board[i+1][j+1].charAt(0) != board[i+1][j].charAt(0) && ownedCards.indexOf(board[i+1][j].charAt(0)) != -1 )))
					{
						places.add(cnt);
						placesCards.add(""+board[i+1][j].charAt(0));
						targets.add( board[i+1][j+1]);
					}
					else if ( j == 0 && ( board[i][j].charAt(0) == 'E' && ( i < board.length -1 && board[i+1][j].charAt(0) != 'E' ) && (j < board.length -1 && board[i+1][j+1].charAt(0) != 'E' 
							&& board[i+1][j+1].charAt(0) != board[i+1][j].charAt(0) && ownedCards.indexOf(board[i+1][j+1].charAt(0)) != -1 )))
					{
						places.add(cnt);
						placesCards.add(""+board[i+1][j+1].charAt(0));
						targets.add( board[i+1][j]);
					}
					cnt +=1;
				}
			}
		}
		BlockMove [] place = new BlockMove[places.size()];
		cnt= 0 ;
		for ( int j = 0 ; j < places.size() ; j ++ )
		{
			place[cnt] = new BlockMove( Integer.parseInt(places.get(j).toString()) , placesCards.get(j).charAt(0), targets.get(j).charAt(0) );
			cnt+=1;
		}
		return  place;
	}


	public BlockMove[] findHorizontalBlockPlace()
	{
		String [][] board = g.getPartyBoard();

		ArrayList<String> placesCards = new ArrayList<String>();
		ArrayList<Integer> places =  new ArrayList<Integer>();
		ArrayList<String> targets =  new ArrayList<String>();

		int cnt = 0;
		for ( int i = 0 ; i < board[0].length ; i ++ )
		{
			for ( int j = 0 ; j < board[0].length ; j ++ )
			{
				if (board[i][j].charAt(0) != 'X' &&  i < board[0].length-1 && j < board[0].length-1)
				{
					if ( board[i][j].charAt(0) == 'E' && board[i+1][j].charAt(0) != 'E' && i < board[0].length && j < board[0].length && j > 0 && board[i+1][j].equals(board[i][j+1] ) == false && board[i+1][j].equals(board[i][j-1] ) )
					{
						if ( ownedCards.indexOf(board[i+1][j+1]) != -1)
						{
							placesCards.add(""+board[i+1][j+1].charAt(0));
							targets.add( board[i+1][j]);
							places.add(cnt);
						}
						if ( ownedCards.indexOf(board[i+1][j-1] ) != -1)
						{
							placesCards.add(""+board[i+1][j-1].charAt(0));
							targets.add( board[i+1][j]);
							places.add(cnt);
						}
					}
					cnt+=1;
				}
			}
		}
		BlockMove [] place = new BlockMove[places.size()];
		cnt= 0 ;
		for ( int j = 0 ; j < places.size() ; j ++ )
		{
			place[cnt] = new BlockMove( Integer.parseInt(places.get(j).toString()) , placesCards.get(j).charAt(0) , targets.get(j).charAt(0));
			cnt+=1;
		}
		return  place;
	}


	public BlockMove lateralBlockMove()
	{
		char mustNotBlock = cardToPlay().charAt(0);
		char mustBlock = cardToBlock().charAt(0);
		BlockMove [] availableBlock = findLateralBlockPlace();
		BlockMove blockMove = null;
		for ( BlockMove i : availableBlock )
		{
			if ( i.getTarget() != mustNotBlock )
			{
				if ( i.getColor() == mustNotBlock )
					return  i;
				if ( i.getTarget() == mustBlock )
					return i;
				else
					blockMove = i;

			}
		}
		return blockMove;
	}

	public BlockMove horizontalBlockMove()
	{
		char mustNotBlock = cardToPlay().charAt(0);
		char mustBlock = cardToBlock().charAt(0);
		BlockMove [] availableBlock = findHorizontalBlockPlace();
		BlockMove blockMove = null;
		for ( BlockMove i : availableBlock )
		{
			if ( i.getTarget() != mustNotBlock )
			{
				if ( i.getColor() == mustNotBlock )
					return  i;
				if ( i.getTarget() == mustBlock )
					return i;

				else
					blockMove = i;

			}
		}
		return blockMove;
	}
}