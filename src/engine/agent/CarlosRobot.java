package engine.agent;

import engine.game.*;
import network.dataobjects.RoundStart;
import engine.agent.brain.*;
import engine.agent.brain.logic.*;
import stats.Profile;

/**
 * @author Quentin Raymondaud
 * @author Imad Iguichi
 * Heuristical Logic Robot
 */

public class CarlosRobot extends Player
{
	private BackToTheRandomFutur brain ;

	public CarlosRobot()
	{
		super();
		setBot();
	}
	
	public CarlosRobot ( RoundStart rs , Profile p)
	{
		super(rs,p);
		brain = new BackToTheRandomFutur(p.getLevel());
		setBot();
	}
	 /**
	 * Construct a Robot with a Profile p
	 * @param p Profile
	 */
	public CarlosRobot(Profile p)
	{
		super(p);
		brain = new BackToTheRandomFutur(p.getLevel());
		setBot();
	}

	 /**
	 * Construct a Robot with a Profile p and number of cards
	 * @param nbCards [description]
	 * @param p       [description]
	 */
	public CarlosRobot(int nbCards,Profile p, Timer t)
	{
		super(nbCards,p,t);
		brain = new BackToTheRandomFutur(p.getLevel());
		setBot();
	}
	/*
	 * Generate an Action
	 * @param  g Game
	 * @return   String ( Action )
	 */
	@Override
	public String play(Game g)
	{
		if ( getGR() != true )
			return "DDD";
		brain.initSimulation(g,ownedCards,greenCardsPlayed);
		Move bestMove = brain.getResult();
		System.out.println( bestMove );
		if ( bestMove != null && g.canPutCard(bestMove.getPosition(),""+bestMove.getColor() ))
		{
			g.putCard(bestMove.getPosition(),""+bestMove.getColor());
			return String.valueOf(bestMove.getPosition())+bestMove.getColor()+"E";
		}
		if ( getGR() != true )
			return "DDD";
		String value ="";
		for ( int i = 35 ; i >= 0 ; i-- )
		{		
			for ( int a = 0 ; a < ownedCards.length() ; a ++)
			{
				value = "";
				value += ownedCards.charAt(a);
				if ( g.canPutCard(i,value))
				{
					g.putCard(i,value);
					return String.valueOf(i)+value+"E";
				}
			}
		}
		return "DDD";
	}
}


