package engine.agent;

import engine.game.*;
import network.dataobjects.RoundStart;
import stats.Profile;

/**
 * @author Quentin Raymonadud
 */

public class Robot extends Player
{
	//Class Robot

	/**
	 * Construct a Robot 
	 */
	public Robot()
	{
		super();
		setBot();
	}
	
	public Robot ( RoundStart rs , Profile p)
	{
		super(rs,p);
		setBot();
	}
	/**
	 * Construct a Robot with a Profile p
	 * @param p Profile
	 */
	public Robot(Profile p)
	{
		super(p);
		setBot();
	}

/**
 * Construct a Robot with a Profile p and number of cards
 * @param nbCards [description]
 * @param p       [description]
 */
	public Robot(int nbCards,Profile p, Timer t)
	{
		super(nbCards,p,t);
		setBot();
	}
	/**
	 * Generate an Action
	 * @param  g Game
	 * @return   String ( Action )
	 */
	@Override
	public String play(Game g)
	{
		for ( int i = 35 ; i >= 0 ; i-- )
		{
			for ( int a = 0 ; a < ownedCards.length() ; a ++)
			{
				String value = "";
				value += ownedCards.charAt(a);
				if ( g.canPutCard(i,value))
				{
					g.putCard(i,value);
					return String.valueOf(i)+value+"E";
				}
			}
		}
		return "DDD";
	}
}
