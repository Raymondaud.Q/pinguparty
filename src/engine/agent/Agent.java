package engine.agent;

import engine.game.Game;

/**
 * @author Quentin Raymondaud
 *
 */
public interface Agent 
{
	// Interface Agent
	/**
	 * Method that each agent need to implement for playing in a Game
	 * @param  Game g : contains the entire pinguparty universe
	 * @return Return the action played as String
	 */
	String play(Game g);
}