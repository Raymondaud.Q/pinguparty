package engine.agent;

import engine.game.*;
import network.dataobjects.RoundStart;
import engine.agent.brain.*;
import stats.Profile;

/**
 * @author Quentin Raymondaud
 * @author Imad Iguichi
 * Heuristical Logic Robot
 */

public class HBasedRobot extends Player
{
	//Class Robot

	ZombieBlock brain = new ZombieBlock();
	/**
	* Construct a Robot 
	*/
	public HBasedRobot()
	{
		super();
		setBot();
	}
	
	public HBasedRobot( RoundStart rs , Profile p)
	{
		super(rs,p);
		setBot();
	}
	 /**
	 * Construct a Robot with a Profile p
	 * @param p Profile
	 */
	public HBasedRobot(Profile p)
	{
		super(p);
		setBot();
	}

	 /**
	 * Construct a Robot with a Profile p and number of cards
	 * @param nbCards [description]
	 * @param p       [description]
	 */
	public HBasedRobot(int nbCards,Profile p, Timer t)
	{
		super(nbCards,p,t);
		setBot();
	}


	/*
	 * Generate an Action
	 * @param  g Game
	 * @return   String ( Action )
	 */
	@Override
	public String play(Game g)
	{
		brain.setGame(g,ownedCards);

		String value = "";
		Move toTry = brain.lateralBlockMove();
		Move toTryH = brain.horizontalBlockMove();
		String  card = brain.cardToPlay();

		if ( toTry != null && g.canPutCard( toTry.getPosition(),""+toTry.getColor() ) )
		{
			g.putCard( toTry.getPosition(),""+toTry.getColor() );
			return String.valueOf(toTry.getPosition())+toTry.getColor()+"E";
		}
		else if (  toTryH != null && g.canPutCard( toTryH.getPosition(),""+toTryH.getColor() ) )
		{
			g.putCard( toTryH.getPosition(),""+toTryH.getColor() );
			return String.valueOf(toTryH.getPosition())+toTryH.getColor()+"E";
		}

		for ( int i = 35 ; i >= 0 ; i-- )
		{		
			if ( g.canPutCard(i,card))
			{
				g.putCard(i,card);
				return String.valueOf(i)+card+"E";
			}
			for ( int a = 0 ; a < ownedCards.length() ; a ++)
			{
				value = "";
				value += ownedCards.charAt(a);
				if ( g.canPutCard(i,value))
				{
					g.putCard(i,value);
					return String.valueOf(i)+value+"E";
				}
			}
		}
		return "DDD";
	}
}
