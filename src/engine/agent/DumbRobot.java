package engine.agent;

import engine.game.*;
import network.dataobjects.RoundStart;
import stats.Profile;

/**
 * @author Quentin Raymonadud
 */
public class DumbRobot extends Robot
{
	//Class DumbRobot
	public DumbRobot()
	{
		super();
	}

	/**
	 * Construct a DumbRobot with a Profile p
	 * @param p Profile
	 */
	public DumbRobot(Profile p)
	{
		super(p);
	}
	
	public DumbRobot( RoundStart rs , Profile p)
	{
		super(rs,p);
		setBot();
	}

	/**
	 *  Construct a DumbRobot with a Profile p and a number of Cards
	 * @param nbCards Integer
	 * @param p       Profile
	 */
	public DumbRobot(int nbCards,Profile p, Timer t)
	{
		super(nbCards,p,t);
	}

	/**
	 * Generate a Dumb action
	 * @param  g Game
	 * @return   String action
	 */
	@Override
	public String play(Game g)
	{
		for ( int i = 0 ; i < 35 ; i++ )
		{
			for ( int a = 0 ; a < ownedCards.length() ; a ++)
			{
				String value = "";
				value += ownedCards.charAt(a);
				if ( g.canPutCard(i,value))
				{
					g.putCard(i,value);
					System.out.println("Je suis un robot pas très intelligent mais voici mon coup ! ");
					return String.valueOf(i)+value+"E";
				}
			}
		}
		return "DDD";
	}
}
