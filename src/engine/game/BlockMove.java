package engine.game;
public class BlockMove extends Move
{
	char target;
	public BlockMove(int pos, char col,char tar)
	{
		super(pos,col);
		target = tar;
	}

	public char getTarget()
	{
		return target;
	}


	@Override
	public String toString()
	{
		return ( " Case : " + position + ", Color : " + color + ", Target : " + target);
	}
}