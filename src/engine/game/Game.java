package engine.game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import engine.agent.AgentFactory;
import engine.agent.brain.logic.*;
import engine.agent.Player;
import gui.LocalGame;
import network.dataobjects.*;
import stats.Profile;
import stats.Ranking;
import stats.Stat;
import stats.TempStat;
import stats.Replay;

import java.text.DateFormat;
import java.util.Date;

public class Game implements Serializable
{
	/**
	 * Number of player
	 */
	private int nbPlayer;					// Nombre de joueurs
	/**
	 * Number of cards per Player
	 */
	private int nbCards;					// Nombre de cartes par joueur
	/**
	 * Number of the current round
	 */
	private int nbRound; 					// Numero de manche
	/**
	 * Index of Player wich play this gameloop
	 */
	private int playerIndex;
	/**
	 * Indicate if the game is looping or not
	 */
	private boolean game;					// variable de partie
	/**
	 * Game board
	 */
	private String [][] partyBoard ;		// Plateau de jeu
	/**
	 * Player array
	 */
	private Player [] player;				// Tableau de joueurs
	/**
	 * Indicate if the game is local or accept distant players
	 */
	/**
	 * List of cards generated at each round
	 */
	private List<String> partyCards;
	/**
	 * Chronometer
	 */
	private Chrono chrono;
	/**
	 * Ranking of end of game
	 */
	private Ranking rank;

	private int blitzTime ;
	
	public PlayerInformation lastPlayerInfo = null;


	private static ArrayList<Replay> replay = new ArrayList<Replay>();
	private static int nbGame = -1;
	private int gameNumber;
	
	public static int getNbGame()
	{
		return nbGame;
	}



	//7 cartes rouges, violettes, bleues et jaunes et 8 cartes vertes, pour un total de 36 cartes.

/**
 * Construct a new Game for a new round
 *
 * @param nb    Integer number of player
 * @param p    	Player array
 * @param nbH   Integer number of round
 * @param locaL Boolean local game or not
 * @param iP    Integer : PlayerIndex
 * @param c     Chrono : Chronometer
 */
	public Game(int nb, Player [] p,int nbH, int iP, Chrono c, int time, int gameNumber) // Nouvelle manche
	{
		this.gameNumber=gameNumber;
		chrono = c;
		playerIndex = iP ;
		game = true;
		nbPlayer = nb ;
		nbRound = nbH;
		player = p ;
		partyCards = new ArrayList<String>();
		partyBoard = new String[8][8];								//Allocation du plateau de jeu
		blitzTime = time;


		if ( nbPlayer > 6 )
			nbPlayer = 6;
		if ( nbPlayer <= 1 )
			nbPlayer = 2;

		initDeck();
		initBoard();
		calcNbCards();

		for (int i = 0 ; i < nbPlayer ; i ++)
		{

			player[i].setPlayerCards("");
			player[i].setGR();
			player[i].setNbC(nbCards);
			if ( blitzTime >= 5 )
			{
				player[i].setTimer(new Timer(time));
			}
		}

		initPlayerDeck();
		System.out.println(partyCards);
	}




	public Game(int nb, Profile [] p , int time,Replay replay)
	{
		nbGame+=1;
		gameNumber = nbGame;
		chrono = new Chrono();
		playerIndex = 0 ;
		game = true;
		nbPlayer = nb ;
		nbRound = 1;
		player = new Player[nb];
		blitzTime = time;


		partyCards = new ArrayList<String>();
		partyBoard = new String[8][8];			//Allocation du plateau de jeu


		if ( nbPlayer > 6 )
			nbPlayer = 6;
		if ( nbPlayer <= 1 )
			nbPlayer = 2;

		if (Replay.getReplayState() )
			this.replay.add(replay);
		initDeck();
		initBoard();
		calcNbCards();
		constructPlayer(p);
		initPlayerDeck();
		chrono.start();
		System.out.println(partyCards);

	}


	public Game(int nb, Profile [] p , int time, boolean cli)
	{
		System.out.println(" - FUCKING CREATION");
		
		chrono = new Chrono();
		playerIndex = 0 ;
		game = true;
		nbPlayer = nb ;
		nbRound = 1;
		player = new Player[nb];
		blitzTime = time;
		
		if ( nbPlayer > 6 )
			nbPlayer = 6;
		if ( nbPlayer <= 1 )
			nbPlayer = 2;
		
		partyBoard = new String[8][8];			//Allocation du plateau de jeu
		initBoard();
		calcNbCards();
		constructPlayer(p);
		chrono.start();

	}

	/**
	 * Random initialization of the Game deck
	 */
	public void initDeck()
	{
		for (int x = 0 ;  x < 36 ; x ++)
		{
			if ( x < 7)
				partyCards.add("R"); 			// COLORS.RED
			else if ( x >= 7 && x < 7*2)
				partyCards.add("P");			// COLORS.PURPLE
			else if ( x >= 7*2 && x < 7*3)
				partyCards.add("B");			// COLORS.BLUE
			else if ( x >= 7*3 && x < 7*4)
				partyCards.add("Y");			// COLORS.YELLOW
			else
				partyCards.add("G");			// GREEN
		}

		Collections.shuffle(partyCards);
	}
	
	/**
	 * initialization of Gameboard
	 */
	public void initBoard()
	{

		if ( nbPlayer == 2)										//Initialisation du plateau pour 2 joueurs (Pas d'étage 8)
		{
			for (int i = 0 ; i < 8 ; i++ )
			{
				for (int y = 0 ; y < 8 ; y++ )
				{
					if ( i < 7 && y < i+1 )
						partyBoard[i][y]="E";
					else
						partyBoard[i][y]="X";
				}
			}
		}

		else 													//Initialisation du plateau pour les autres nombre de joueurs
		{
			for (int i = 0 ; i < 8 ; i++ )
			{
				for (int y = 0 ; y < 8 ; y++ )
				{
					if ( y < i+1 )
						partyBoard[i][y]="E";
					else
						partyBoard[i][y]="X";
				}
			}
		}
	}
/**
 * Creation of players deck
 */
	public void initPlayerDeck()
	{
		int i = 0 ;
		int y = 0 ;
		System.out.println(nbPlayer);
		for ( int m = 0 ; m < nbCards*nbPlayer ; m ++ )
		{
			if ( y < nbCards )
			{
				player[i].AddCard(partyCards.get(m));
				y+=1;
			}
			else
			{
				i +=1 ;
				y = 0 ;
				m -= 1;
			}
		}
		if (Replay.getReplayState())
		{
			String[] decks = Player.getAllCards(player,nbPlayer,getProfile());
			if ( replay != null )
			{
				if ( replay.get(gameNumber) != null )
					replay.get(gameNumber).addDecks(decks);
			}
		}

	}
	
	public Replay getReplay()
	{
		if ( replay != null )
		{
			if ( replay.get(gameNumber) != null )
				return replay.get(gameNumber);
		}
		return null;
	}

/**
 * Calculate the number of cards per Player according to the number of player
 */
	public void calcNbCards()
	{
		if ( nbPlayer ==  2)
			nbCards = 14;
		else if (nbPlayer == 5)
		{
			nbCards = 7;
			System.out.println("Carte restante : " + partyCards.get(35));
		}
		else
			nbCards = 36/nbPlayer;
		if ( nbPlayer ==  2)
			nbCards = 14;
		else if (nbPlayer == 5)
		{
			nbCards = 7;
			System.out.println("Carte restante : " + partyCards.get(35));
		}
		else
			nbCards = 36/nbPlayer;
	}

	/**
	 * Call the method createPlayer in AgentFactory for each player
	 * generate the Game's Player array
	 * @param p Profile array
	 */
	public void constructPlayer(Profile [] p )
	{
		for (int pl = 0 ; pl < nbPlayer ; pl ++)
		{
			if ( blitzTime >= 5 )
			{
				player[pl]=AgentFactory.createPlayer(nbCards,p[pl],new Timer(blitzTime));
			}
			else
				player[pl]=AgentFactory.createPlayer(nbCards,p[pl],null);
		}
	}

/**
 * Shuffle the player array
 * @return Game : this
 */
	public Game shuffle()
	{
		List<Player> playerl = new ArrayList<Player>(Arrays.asList(player));
		Collections.shuffle(playerl);
		player = playerl.toArray(player);

		return this;
	}

	/**
	 * Return the ranking rank ( end of game)
	 * @return Ranking : rank of end of game
	 */
	public Ranking getRanking()
	{
		return rank;
	}

	/**
	 * Display the gameBoard console
	 */
	public void printParty()
	{

		//System.out.println( GREEN + "This text has a green background but default text!" + COLORS.RESET);
		int cmp = 0;
		for (int i = 0 ; i < 8 ; i++ )
		{
			for (int g = 7-i ; g > 0 ; g--)
			{
				System.out.print("   ");
			}

			for (int y = 0 ; y < 8 ; y++ )
			{
				String pb = partyBoard[i][y];
				String color = "";
				if ( pb.charAt(0) == 'R')
					color = COLORS.RED;
				else if ( pb.charAt(0) == 'P')
					color = COLORS.PURPLE;
				else if ( pb.charAt(0) == 'Y')
					color = COLORS.YELLOW;
				else if ( pb.charAt(0) == 'B')
					color = COLORS.BLUE;
				else if ( pb.charAt(0) == 'G')
					color = COLORS.GREEN;
				if ( pb.charAt(0) == 'E')
				{
					pb = String.valueOf(cmp);
					cmp += 1;
					if ( cmp >= 10 )
						System.out.print("  |"+color+ pb + COLORS.RESET+"|");
					else if ( cmp < 10 )
						System.out.print("  |"+color+ " "+ pb + COLORS.RESET+"|");
				}
				else if ( pb.charAt(0) != 'X')
				{
					cmp += 1;
					System.out.print("  |"+color+" "+ pb + COLORS.RESET+"|");
				}
			}
			System.out.println(" ");
		}

	}

	
	

	/**
	 * Indicate if we can put a card
	 * @param  place A Specific place
	 * @param  value String : a single card ( char )
	 * @return       Boolean
	 */
	public boolean canPutCard(int place, String value)			// Pose une carte en fonction des regles du jeu
	{
		if ( player[playerIndex].getCards().indexOf(value) == -1 )
			return false; 
		int cmp = 0;
		int x = 0;
		int y = 0;

		for (int i = 0 ; i < 8 ; i++ )
		{
			for (int j = 0 ; j < 8 ; j++ )
			{
				String pb = partyBoard[i][j];
				if ( pb.charAt(0) != 'X')
				{
					if ( cmp == place)
					{
						x = i;
						y = j;
					}
					cmp += 1;
				}
			}
		}
		//System.out.println(place+" "+value+" "+nbPlayer+" "+x+" "+y+ " "+ partyBoard[x][y]);
		if ( partyBoard[x][y].equals("E") && ( value.charAt(0) == 'R' || value.charAt(0) == 'G' || value.charAt(0) == 'P' || value.charAt(0) == 'Y' || value.charAt(0) == 'B') )
		{
			//System.out.println(nbPlayer+" "+x);
			if ( nbPlayer == 2 && x == 6 )
			{
				return true;
			}
			else if ( nbPlayer > 2 && x == 7 )
			{
				return true;
			}

			else if (  x < 7 )
			{
				if ( ( partyBoard[x+1][y].charAt(0) == value.charAt(0) && partyBoard[x+1][y+1].charAt(0) != 'E') || ( partyBoard[x+1][y+1].charAt(0) == value.charAt(0) &&  partyBoard[x+1][y].charAt(0) != 'E'  ) )
				{
					return true;
				}
			}
			return false ;
		}
		return false;
	}
	
	/**
	 * Try to put the card value to place
	 * Return true if it's possible , false otherwise
	 * @param  place A Specific place
	 * @param  value String : a single card ( char )
	 * @return       Boolean
	 */
	public boolean putCard(int place, String value)			// Pose une carte en fonction des regles du jeu
	{

		if ( player[playerIndex].getCards().indexOf(value) == -1 )
			return false; 
		
		int cmp = 0;
		int x = 0;
		int y = 0;

		for (int i = 0 ; i < 8 ; i++ )
		{
			for (int j = 0 ; j < 8 ; j++ )
			{
				String pb = partyBoard[i][j];
				if ( pb.charAt(0) != 'X')
				{
					if ( cmp == place)
					{
						x = i;
						y = j;
					}
					cmp += 1;
				}
			}
		}

		if ( partyBoard[x][y] == "E" && ( value.charAt(0) == 'R' || value.charAt(0) == 'G' || value.charAt(0) == 'P' || value.charAt(0) == 'Y' || value.charAt(0) == 'B') )
		{
			if ( nbPlayer == 2 && x == 6 )
			{
				partyBoard[x][y] = value;
				return true;
			}
			else if ( nbPlayer > 2 && x == 7 )
			{
				partyBoard[x][y] = value;
				return true;
			}

			else if (  x < 7 )
			{
				if ( ( partyBoard[x+1][y].charAt(0) == value.charAt(0) && partyBoard[x+1][y+1].charAt(0) != 'E') || ( partyBoard[x+1][y+1].charAt(0) == value.charAt(0) &&  partyBoard[x+1][y].charAt(0) != 'E'  ) )
				{
					partyBoard[x][y] = value;
					return true;
				}
			}
			return false ;
		}
		return false;
	}
	
	public boolean putCard(String hit)
	{
		if( hit.length() > 3)
		{
			String strPlace = ""+hit.charAt(0)+hit.charAt(1);
			int place = Integer.valueOf(strPlace);
			return putCard(place,""+hit.charAt(3));
		}
		String strPlace = ""+hit.charAt(0);
		int place = Integer.valueOf(strPlace);
		return putCard(place,""+hit.charAt(1));
	}

	/**
	 * Indicate if we can put a card
	 * @param  place A Specific place
	 * @param  value String : a single card ( char )
	 * @return       Boolean
	 */
	public void brutalPutCard(int place, String value)			// Pose une carte en fonction des regles du jeu
	{
		int cmp = 0;
		int x = 0;
		int y = 0;

		for (int i = 0 ; i < 8 ; i++ )
		{
			for (int j = 0 ; j < 8 ; j++ )
			{
				String pb = partyBoard[i][j];
				if ( pb.charAt(0) != 'X')
				{
					if ( cmp == place)
					{
						x = i;
						y = j;
						partyBoard[x][y]=value;
						return;
					}
					cmp += 1;
				}
			}
		}
	}
	

	/**
	 * Indicate a list of place in wich we can put a card value
	 * @param  value a single card ( char )
	 * @return       Integer list
	 */
	public List<Integer>  wherePutCard(String value)			// renvoie une liste de position ou l'on peut placer la carte de valeur value
	{
		List<Integer> res = new ArrayList<Integer>();
		int i=0;

		for (int place = 35 ; place >= 0 ; place-- )
		{
			if ( canPutCard(place,value) )
			{
				res.add(place);
			}
		}
		return res;
	}

	/**
	 * Call the new round constructor
	 * @param  g Game : g
	 * @return   Game
	 */
	public static Game startRound(Game g)
	{
		g = new Game(g.getNbPlayer(),Player.setAlive(g.getPlayer(),g.getNbPlayer()),g.getNbRound(),g.getPlayerIndex(),g.getChrono(),g.getBlitzTime(),g.getGameNumber());
		return g;
	}

	public int getGameNumber()
	{
		return gameNumber;
	}

	public int getBlitzTime()
	{
		return blitzTime;
	}

	/**
	 * Return the double array of string wich represents the gameboard
	 * @return String double array
	 */
	public String[][] getBoard()
	{
		return partyBoard;
	}

	/**
	 * Return the card i of the game deck
	 * @param  i Integer : index
	 * @return   String ( char )
	 */
	public String getPartyCards(int i)
	{
		if (partyCards.get(i) != null)
			return partyCards.get(i);
		return null;
	}

	/**
	 * Return the index of the current player
	 * @return  Integer : playerIndex
	 */
	public int getPlayerIndex()
	{
		return playerIndex;
	}

	/**
	 * Needed for replaying in case of wrong user input
	 */
	public void decreasePlayerIndex()
	{
		playerIndex-=1;
	}

	/**
	 * Return the Game Chrono
	 * @return [description]
	 */
	public Chrono getChrono()
	{
		return chrono;
	}

	/**
	 * Return the Player array
	 * @return Player Array
	 */
	public Player[] getPlayer()
	{
		return player;
	}

	/**
	 * Set player reference to p
	 * @param Player[]p A Player array
	 */
	public void setPlayers(Player[]p)
	{
		player = p;
	}

	/**
	 * True if the game isn't distant
	 * @return Boolean
	 */


	/**
	 * Return the number of cards per Player
	 * @return Integer
	 */
	public int getNbCards()
	{
		return nbCards;
	}

	/**
	 * Return the number of Round ( useless but used)
	 * @return Integer
	 */
	public int getNbRound()
	{
		return nbRound;
	}

	/**
	 * Return the number of player
	 * @return Integer
	 */
	public int getNbPlayer()
	{
		return nbPlayer;
	}

	/**
	 * Return true if the game is running , false otherwise
	 * @return Boolean
	 */
	public boolean getGame()
	{
		return game;
	}

	/**
	 * Add 1 to the round number
	 */
	public void increaseNbRound()
	{
		nbRound += 1;
	}

	/**
	 * Add 1 to the number of player
	 */
	public void increaseNbPlayer()
	{
		nbPlayer += 1;
	}

	/**
	 * Set game
	 * true if the game need to loop
	 * @param b Boolean
	 */
	public void setGame(boolean b)
	{
		game = b;
	}

	/**
	 * Return the player i
	 * @param  i  Integer index
	 * @param  nb limit number
	 * @return    [description]
	 */
	public Player getPlayer(int i, int nb)
	{
		return Player.getPlayer(player,i,nb);
	}

	/**
	 * Return the player who had the Profile p
	 * @param  p Profile
	 * @return   Player
	 */
	public Player getPlayerByProfile(Profile p)
	{
		for ( int i = 0 ; i < nbPlayer ; i ++)
		{
			if (player[i].getProfile() == p)
				return player[i];
		}
		return null;
	}

	/**
	 * Set the player i reference to Player p
	 * @param  p  Player p
	 * @param  i  Integer : index
	 * @param  nb limit number
	 * @return    Player array
	 */
	public Player[] setPlayer(Player p, int i, int nb )
	{
		return Player.setPlayer(player,p,i,nb);
	}

	public boolean setPlayerHandle (int i )			// Gestion de deux pseudo identiques a prévoir !
	{
		return Player.setPlayerHandle(player,i,nbPlayer);
	}

	/**
	 * set Score of the player i to p
	 * @param  i Integer : index
	 * @param  p Integer : p
	 * @return   Boolean
	 */
	public boolean setPlayerScore (int i, int p)			// Gestion de deux pseudo identiques a prévoir !
	{
		return Player.setPlayerScore(player,i,p,nbPlayer);
	}

	/**
	 * Return the pseudo of the player i
	 * @param  i Integer : index
	 * @return   String : pseudo
	 */
	public String getPlayerPseudo(int i)
	{
		return Player.getPlayerPseudo(player,i,nbPlayer);
	}

	/**
	 * Return the player pseudo list
	 * @return String array
	 */
	public String[] getPlayerPseudo()
	{
		return Player.getPlayerPseudo(player,nbPlayer);
	}

	/**
	 * Return the player Score array
	 * @return Integer array
	 */
	public int [] getPlayerScore()
	{
		return Player.getPlayerScore(player,nbPlayer);
	}

	/**
	 * Return player number of card array
	 * @return Integer array
	 */
	public int [] getPlayerNbC()
	{
		return Player.getPlayerNbC(player,nbPlayer);
	}


	/**
	 * Return the Player greenCardsplayed number list
	 * @return Integer array
	 */
	public int [] getPlayerNbGreen()
	{
		return Player.getPlayerNbGreen(player,nbPlayer);
	}

	/**
	 * Return the Player Number of Round win
	 * @return  Intger array
	 */
	public int [] getPlayerHandle ()
	{
		return Player.getPlayerHandle(player, nbPlayer);
	}

	/**
	 * Return the Player Profile list
	 * @return Profile Array
	 */
	public Profile [] getProfile()
	{
		return Player.getPlayerProfile(player,nbPlayer);
	}

	/**
	 * Returns the cards of the player i
	 * @param  i Integer : index
	 * @return   String
	 */
	public String getCard(int i)					// Accesseur, recupère toutes les cartes de la partie
	{
		return Player.getCard(player,i,nbPlayer);
	}

	/**
	 * set game = false
	 */
	public void finishGame()
	{
		game=false;
	}

	/**
	 * Set the player i leaver
	 * @param  i  Integer : Index
	 * @return   Boolean ( Already quit = false )
	 */
	public boolean setPlayerLeaver(int i)
	{
		boolean res = Player.setPlayerLeaver(player,i,nbPlayer);
		if ( res )
		{
			return true;
		}
		return  false;
	}

	/**
	 * Set the player i dead
	 * @param  i  Integer : Index
	 * @return   Boolean ( Already dead = false )
	 */
	public boolean setPlayerDead(int i)
	{
		return Player.setPlayerDead(player,i,nbPlayer);
	}
	/**
	 * Return the number of dead player
	 * @return Integer
	 */
	public int getNbDead()
	{
		int nbDead = 0 ;
		for ( int i = 0 ; i < nbPlayer ; i ++)
		{
			if ( ! player[i].getGR() )
				nbDead+=1;
		}
		return nbDead;
	}
	/**
	 * Return the number of leaver
	 * @return Integer
	 */
	public int getNbLeaver()
	{
		int nbAfk = 0 ;
		for ( int i = 0 ; i < nbPlayer ; i ++)
		{
			if ( player[i].getLeaver() )
				nbAfk+=1;
		}
		return nbAfk;
	}

	public String[][]getPartyBoard()
	{
		return partyBoard;
	}

	/**
	 * Returns pseudo of equals Player
	 * @return String array : Pseudos
	 */
	public String [] getPlayerDraw()
	{
		int [] pScore = getPlayerScore();
		int [] pGreen = getPlayerNbGreen();
		int [] pHandle = getPlayerHandle();
		String [] playerDraw = new String[nbPlayer];
		String [] playerPseudo = getPlayerPseudo();

		int minG = 1000;
		int min = 1000;
		int max = 0 ;

		for ( int  i = 0 ;  i < nbPlayer ; i++)
		{
			if ( pScore[i] < min )
				min = pScore[i];
		}

		for ( int  i = 0 ;  i < nbPlayer ; i++)
		{
			if ( pGreen[i] < minG && pScore[i]==min )
				minG = pGreen[i];
		}

		for ( int  i = 0 ;  i < nbPlayer ; i++)
		{
			if ( pHandle[i] > max && pGreen[i] == minG)
				max = pHandle[i];
		}

		for ( int  i = 0 ;  i < nbPlayer ; i++)
		{
			if ( pScore[i] == min && pGreen[i]==minG && pHandle[i]==max)
			{
				playerDraw[i] = playerPseudo[i];
			}
		}
		return playerDraw;
	}

	/**
	 * Determine wich player wins the game
	 * @return Integer : Winner index
	 */
	public int winGame()																//  Detection de l'indice du joueur ayant le plus petit score a la fin de la  partie
	{
		winRound();
		int [] pScore = getPlayerScore();
		int min = 1000;
		int indexMin=-1;
		for ( int  i = 0 ;  i < nbPlayer ; i++)
		{
			if ( pScore[i] < min )
			{
				min = pScore[i];
				indexMin = i ;
			}
			else if ( pScore[i] == min )							// Si il y a egalité on met l'index a -1
			{
				indexMin = -1;
			}
		}

		if ( indexMin != -1)										// Si il y a un joueur qui se demarque avec son score on retourne son index
			return indexMin ;
		else
		{
			indexMin = winGameE(min);									// Sinon on regarde les manches puis le nombre de cartes vertes
		}

		return indexMin ;

	}

	/**
	 * Additional treatment in case of equality
	 * @param  score Integer : Score trigger
	 * @return       Integer : index of player
	 */
	public int winGameE(int score )									// Gestion egalité au nombre de manche ( Le gagnant étant celui qui a gagné le même nombre de manche )
	{
		int [] pHandle = getPlayerHandle();
		int max = 0;
		int indexMax = 0 ;
		for ( int  i = 0 ;  i < nbPlayer ; i++)
		{
			if ( pHandle[i] > max && player[i].getScore() == score )
			{
				max = pHandle[i];
				indexMax = i ;
			}
			else if (pHandle[i] == max && player[i].getScore() == score )
			{
				indexMax = -1;
			}
		}
		if ( indexMax != -1 && player[indexMax].getScore() == score)
			return indexMax;

		int [] pGreen = getPlayerNbGreen();
		int min = 1000;
		int indexMinG = 0 ;
		for ( int  i = 0 ;  i < nbPlayer ; i++)
		{
			if ( pGreen[i] < min  && player[i].getScore() == score && player[i].getRoundScore() == max )		// Il faut que le joueur ayant le plus petit taux de carte verte jouées ai le score possédé par plusieurs joueurs
			{
				min = pGreen[i];
				indexMinG = i ;
			}
			else if ( pGreen[i] == min  && player[i].getScore() == score && player[i].getRoundScore() == max  )												// Si il y a egalité on met l'index a -1
			{
				indexMinG = -1;
			}
		}
		System.out.println(indexMinG);
		return indexMinG;
	}

	/**
	 * Determine winners into a round
	 */
	public void winRound()									// Gagnant d'une manche
	{

		int [] score = getPlayerNbC();
		int min = 100;

		for ( int  i = 0 ;  i < nbPlayer ; i++)
		{
			if ( score[i] < min )
				min = score[i];
		}

		Date now = new Date();
		DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT);
		String date = shortDateFormat.format(now) ;
		
		float timeLeft = timeLeft = chrono.getTour();
		for (int i = 0 ; i < nbPlayer ; i ++ )
		{

			if ( score[i] == min ) // Pas encore gestion égalité par manche
			{
				TempStat.quickSave(player[i].getProfile().getUID(),true,false,score[i],player[i].getScore(),player[i].getProfile().getStat().getTotalScore(),player[i].getProfile().getStat().getElo(),timeLeft,date);
				setPlayerHandle(i);
			}
			else
				TempStat.quickSave(player[i].getProfile().getUID(),false,false,score[i],player[i].getScore(),player[i].getProfile().getStat().getTotalScore(),player[i].getProfile().getStat().getElo(),timeLeft,date);
		}
	}

	/**
	 * Set winners and add them to the end of game rank
	 * @param  nbWinner Integer
	 * @param  s        Boolean ( Modify files or not)
	 * @return          [description]
	 */
	public String setWinner(int nbWinner , boolean s)
	{
		String winner =  getPlayerPseudo(nbWinner);
		if (s)
		{
			Stat.StatWinner(Player.getPlayerProfile(player,nbPlayer),nbWinner,winner);
			rank.addWinner(player[nbWinner].getProfile());
		}
		for ( int lost = 0 ; lost < getNbPlayer() ; lost ++)
		{
			if ( lost != nbWinner && s )
			{
				Stat.StatLooser(Player.getPlayerProfile(player,nbPlayer),lost,getPlayerPseudo(lost));
			}
		}
		game = false;
		return "Partie terminée : Le gagnant est : "+ winner;
	}

	/**
	 * Set draw between players
	 * @param  s Boolean ( Modify files or not)
	 * @return   String : winners pseudo
	 */
	public String setDraw(boolean s)
	{
		String [] winners = getPlayerDraw();
		String winnersName = "";
		for ( int i = 0 ; i < nbPlayer ; i ++)
		{
			if ( winners[i] != null)
			{
				if(s)
					Stat.StatDraw(Player.getPlayerProfile(player,nbPlayer),i,winners[i]);
				winnersName+=winners[i]+" ";
				rank.addWinner(player[i].getProfile());
			}
			else
			{
				if (s)
					Stat.StatLooser(Player.getPlayerProfile(player,nbPlayer),i,getPlayerPseudo(i));
			}
		}
		game = false;
		return " Partie terminée : Les joueurs gagnants a égalité sont " + winnersName ;
	}

	/**
	 * Rank players according to their perfomance
	 * @param  set Boolean ( Modify file or not)
	 * @return     String : Winners
	 */
	public String ranking(boolean set)
	{
		rank = new Ranking();
		String res = "@,";
		int nbWinner =  winGame();
		if ( nbWinner != -1)
		{
			res += setWinner(nbWinner,set)+",";
		}
		else
		{
			res += setDraw(set)+",";
		}

		for (int i = 0 ; i < nbPlayer ; i ++)
		{
			if ( ! player[i].getLeaver() )
			{
				player[i].getProfile().getStat().increaseTime(chrono.getDuration());
			}
			player[i].getProfile().getStat().increaseNb();
			player[i].getProfile().getStat().increaseTotalScore(player[i].getScore());
			player[i].getProfile().getStat().editStat();
			res += getPlayer(i,nbPlayer).getPlayerAttribute()+",";
		}
		eloDuals();

		for (int i = 0 ; i < nbPlayer ; i ++)
			rank.addRank(player[i].getRank());

		rank.print();
		return res;
	}

	/**
	 * Calculate the elo of each player against each player
	 */
	public void eloDuals() //calcElo (float oldElo, float result, float probGagne, int nbParties)
	{
		for ( int i = 0 ; i < nbPlayer ; i ++ )
		{
			for ( int y = i+1 ; y < nbPlayer ; y ++)
			{
				if ( player[i].getScore() > player[y].getScore() ) // Player i wins
				{
					player[i].getProfile().getStat().setElo( player[i].getProfile().getStat().calcElo( player[i].getProfile().getStat().getElo() , 0f , player[i].getProfile().getStat().probWin( player[i].getProfile().getStat().getElo() , player[y].getProfile().getStat().getElo() ) , player[i].getProfile().getStat().getNbGame()  ) );
					player[y].getProfile().getStat().setElo( player[y].getProfile().getStat().calcElo( player[y].getProfile().getStat().getElo() , 1f , player[y].getProfile().getStat().probWin( player[y].getProfile().getStat().getElo() , player[i].getProfile().getStat().getElo() ) , player[y].getProfile().getStat().getNbGame()  ) );
				}
				else if ( player[i].getScore() == player[y].getScore() )  // draw
				{
					player[i].getProfile().getStat().setElo( player[i].getProfile().getStat().calcElo( player[i].getProfile().getStat().getElo() , 0.5f , player[i].getProfile().getStat().probWin( player[i].getProfile().getStat().getElo() , player[y].getProfile().getStat().getElo() ) , player[i].getProfile().getStat().getNbGame()  ) );
					player[y].getProfile().getStat().setElo( player[y].getProfile().getStat().calcElo( player[y].getProfile().getStat().getElo() , 0.5f , player[y].getProfile().getStat().probWin( player[y].getProfile().getStat().getElo() , player[i].getProfile().getStat().getElo() ) , player[y].getProfile().getStat().getNbGame()  ) );
				}
				else // Player i loss
				{
					player[i].getProfile().getStat().setElo( player[i].getProfile().getStat().calcElo( player[i].getProfile().getStat().getElo() , 1f , player[i].getProfile().getStat().probWin( player[i].getProfile().getStat().getElo() , player[y].getProfile().getStat().getElo() ) , player[i].getProfile().getStat().getNbGame()  ) );
					player[y].getProfile().getStat().setElo( player[y].getProfile().getStat().calcElo( player[y].getProfile().getStat().getElo() , 0f , player[y].getProfile().getStat().probWin( player[y].getProfile().getStat().getElo() , player[i].getProfile().getStat().getElo() ) , player[y].getProfile().getStat().getNbGame()  ) );
				}
				player[i].getProfile().getStat().editStat();
				player[y].getProfile().getStat().editStat();
			}
		}
	}
	
	/**
	 * Return 1 if the game is finish , 2 if the round is finished 0 otherwise
	 * @param  i Integer;
	 * @return   Integer
	 */
	public int endGame(int i)
	{
		int nbDead = getNbDead();
		int nbLeaver = getNbLeaver();
		if ( nbDead >= nbPlayer || nbLeaver >= nbPlayer  )																// Si tous les joueurs de la manche sont morts alors on recommence une nouvelle manche
		{
			if( nbRound == nbPlayer || nbLeaver >= nbPlayer )
			{
				
				chrono.stop();
				System.out.println(ranking(true).replaceAll(",","\n"));
				nbGame -=1;
				if ( nbGame == -1)
				{
					replay = new ArrayList<Replay>();
					LocalGame.startAll=false;
				}
				
				return 1;
			}
			else
			{
				System.out.println( "\n ###################### \n #                   # \n #  Nouvelle manche  # \n #                   # \n ###################### ");
				if ( nbRound < nbPlayer )
				{
					increaseNbRound();
					winRound();
					if ( i+1 < nbPlayer )
						playerIndex=i+1;
					else
						playerIndex=0;

					return 2;
				}
			}
		}
		if ( i+1 < nbPlayer )
			playerIndex=i+1;
		else
			playerIndex=0;
		return 0;
	}

	/**
	* Local Game loop function ( looped in panel)
	*
	*/
	public void play( LocalGame panel )
	{
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block lol
			e1.printStackTrace();
		}
		int time; 
		int i = getPlayerIndex();
		Player currentPlayer=getPlayer(i,getNbPlayer());
		panel.flag = true;
		if ( currentPlayer.getGR() && ! currentPlayer.getLeaver() )
		{
			String played="";
			if ( currentPlayer != panel.user || panel.user.getBot())
			{
				try 
				{
					if ( currentPlayer.timerNull() == false) 
					{
						Thread timerVerif = new Thread()
						{
						    public void run()
						    {
						      while (true)
						      {
						    	  currentPlayer.updateTimer();
						    	  if ( currentPlayer.timeLeft() <= 0 )
						    	  {
						    		  setPlayerDead(i);
						    		  break;
						    	  }
						      }
						    	 
						    }
						 };

						
						currentPlayer.startTimer();
						timerVerif.start();
						played = currentPlayer.play(this);
						currentPlayer.stopTimer();
						timerVerif.stop();
						
					}
					else
						played = currentPlayer.play(this);
					
				}
				catch(Exception e) {}
			}
			else
			{
				played = "R";
				if ( currentPlayer.timerNull() == false) {
					currentPlayer.startTimer();
				}
				while (played.charAt(0) == 'R')
				{
					panel.roundAc = "FINISHED";
					panel.roundPlace = "FINISHED";
					while (panel.roundAc.equals("FINISHED") || panel.roundPlace.equals("FINISHED"))
					{
						try
						{
							Thread.sleep(10);
							if ( currentPlayer.timerNull() == false && currentPlayer.updateTimer() )
							{
								panel.roundAc = "D";
								panel.roundPlace = "D";
							}
						}
						catch (Exception e)
						{}
						
					}
					played = panel.user.play(this,panel.roundPlace+panel.roundAc);
				}

			}
			panel.roundAc = "FINISHED";
			panel.roundPlace = "FINISHED";
			panel.flag=true;
			if ( played != null && played.length() != 0 )
			{
				if ( currentPlayer.getGR() && played != "DDD"  && played != "QQQ")
				{
					if ( currentPlayer.timerNull() == false) {
						currentPlayer.stopTimer();
						time = currentPlayer.getTimeLeft();
						if (Replay.getReplayState()) {
							replay.get(gameNumber).addMove(Integer.toString(time),Move.toMove(played),currentPlayer.getProfile());
						}	
					}
					else if (Replay.getReplayState()) {
						replay.get(gameNumber).addMove("0",Move.toMove(played),currentPlayer.getProfile());
					}
					currentPlayer.useCard(played);
				}

				else if ( currentPlayer.getGR() && played.charAt(0) == 'D') 
				{
					setPlayerDead(i);
					if ( currentPlayer.timerNull() == false) {
						currentPlayer.stopTimer();
						time = currentPlayer.getTimeLeft();
						if (Replay.getReplayState()) 
						{
							replay.get(gameNumber).addPass(Integer.toString(time),currentPlayer.getProfile());
						}
					}
					else if (Replay.getReplayState()) 
					{
						replay.get(gameNumber).addPass("0",currentPlayer.getProfile());
					}
				}
				else if ( currentPlayer.getGR() && played.charAt(0) == 'Q')
				{
					setPlayerDead(i);
					setPlayerLeaver(i);
					if (Replay.getReplayState()) {
						replay.get(gameNumber).addQuit(currentPlayer.getProfile());
					}
				}
			}
			else
			{
				setGame(false);
			}
		}
		int a = endGame(i);
		if ( a == 2)
		{
			Game.startRound(this);
			initBoard();
		}
		
	}



	public void saveProfiles()
	{
		for (int i = 0 ; i < nbPlayer ; i ++ )
		{
			player[i].getProfile().editProfile();
		}
	}

	public void saveProfiles(String uuid )
	{
		for (int i = 0 ; i < nbPlayer ; i ++ )
		{
			if ( player[i].getProfile().getUID().equals(uuid))
				player[i].getProfile().setLocal(1);
				
			else
				player[i].getProfile().setLocal(0);
			
			player[i].getProfile().editProfile();
		}
	}

	public RoundStart[] convertToRoundStart() 
	{
		RoundStart [] rs = new RoundStart[player.length];
		int counter = 0;
		
		for ( Player p : player )
		{
			
			rs[counter] = new RoundStart(nbPlayer,p.getCards(),getColor().UNDEFINED,p.getTimeLeft(),"");
			counter +=1;
		}
		return rs;
	}
	
	public EndGame convertToEndGame()
	{
		ArrayList<Result> m = new ArrayList<Result>();
		ArrayList<StatData> ps = new ArrayList<StatData>();
		
		for ( Player p : player )
		{
			ps.add(p.getProfile().toProfileStatData().stat);
			m.add(new Result(p.getProfile().getUID(),0,p.getScore(),p.getGreen(),null));
		}
		
		
		EndGame eg = new EndGame(ps,m);
		return eg;
	}
	
	public void setRanking( Ranking n)
	{
		rank = n;
	}


	public network.dataobjects.Color getColor()
	{
		if ( nbPlayer == 5 )
		{
			return Color.getColorEnum(partyCards.get(36).charAt(0));
		}
		else
			return network.dataobjects.Color.UNDEFINED;
			
	}

	public NextTurn convertToNextTurn() 
	{
		Player currentPlayer = player[playerIndex];
		String uid = currentPlayer.getProfile().getUID();
		return new NextTurn(uid,generatePossibleMoves(currentPlayer.getCards()),lastPlayerInfo);
	}
	
	public HashMap<network.dataobjects.Color,ArrayList<Integer>> generatePossibleMoves(String cards)
	{
		HashMap<network.dataobjects.Color,ArrayList<Integer>> moves = new HashMap<network.dataobjects.Color,ArrayList<Integer>>();
		ArrayList<Integer> movesForColor =new  ArrayList<Integer>();
		for ( int i = 0 ; i < cards.length() ; i ++ )
		{
			movesForColor = (ArrayList) wherePutCard(""+cards.charAt(i));
			moves.put(Color.getColorEnum(cards.charAt(i)),movesForColor);
		}
		return moves;
	}
};

class COLORS 
{
	public static final String RESET = "\u001B[00;01;37m";
	public static final String BLACK = "\u001B[40m";
	public static final String RED = "\u001B[41m";
	public static final String GREEN = "\u001B[42m";
	public static final String YELLOW = "\u001B[43m";
	public static final String BLUE = "\u001B[44m";
	public static final String PURPLE = "\u001B[45m";
	public static final String CYAN = "\u001B[46m";
	public static final String WHITE = "\u001B[47m";
};
