package engine.game;

public class Color 
{
	public static final char RED = 'R';
	public static final char GREEN = 'G';
	public static final char BLUE = 'B';
	public static final char YELLOW = 'Y';
	public static final char PURPLE = 'P';
	public static final char UNDEFINED = 'U';

	public static char getColor(char c)
	{
		if ( c == 'R')
			return RED;
		else if ( c == 'G')
			return GREEN;
		else if ( c == 'B')
			return BLUE;
		else if ( c == 'Y')
			return YELLOW;
		else if ( c == 'P')
			return PURPLE;
		return UNDEFINED;
	}
	
	public static network.dataobjects.Color getColorEnum(char c)
	{
		if ( c == RED)
			return network.dataobjects.Color.RED;
		else if ( c == GREEN)
			return network.dataobjects.Color.GREEN;
		else if ( c ==	BLUE )
			return network.dataobjects.Color.BLUE;
		else if ( c == YELLOW)
			return network.dataobjects.Color.YELLOW;
		else if ( c == PURPLE)
			return network.dataobjects.Color.PURPLE;
		return network.dataobjects.Color.UNDEFINED;
	}

	public static char getColorChar(network.dataobjects.Color c) 
	{
		if ( c == network.dataobjects.Color.RED)
			return RED;
		else if ( c == network.dataobjects.Color.GREEN)
			return GREEN;
		else if ( c ==	network.dataobjects.Color.BLUE )
			return BLUE;
		else if ( c == network.dataobjects.Color.YELLOW)
			return YELLOW;
		else if ( c == network.dataobjects.Color.PURPLE)
			return PURPLE;
		return UNDEFINED;
	}
}