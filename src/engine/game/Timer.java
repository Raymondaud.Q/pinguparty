package engine.game;
import java.io.Serializable;

public class Timer extends Chrono implements Serializable
{

	float timeLeft = 0 ; // in ms

	
	public Timer ( float blitzTimer )
	{
		super();
		timeLeft = blitzTimer*1_000; // Avant c'etait 1M non pas parce que je suis débile mais parce que des sleep pertubaient "les temps"
	}

	@Override
	public void stop()
    {
        endT = System.currentTimeMillis();
        duration = endT - startT;
        timeLeft -= duration;
    }

	public boolean update()
	{
        duration = System.currentTimeMillis() - startT;
        timeLeft -= duration;
        startT = System.currentTimeMillis(); 			// pour pouvoir faire multiple update sans tout cassé
        if ( timeLeft <= 0 ) 
            return true;
        return false;
    }

    public int getTimeLeft()
    {
    	return Math.round(timeLeft/1_000);
    }
    public float timeLeft()
    {
    	return timeLeft;
    }


}
