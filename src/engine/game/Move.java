package engine.game;

public class Move 
{
	int position;
	char color;

	public Move(int pos, char col)
	{
		position = pos;
		color = col;
	}

	public char getColor()
	{
		return color;
	}

	public int getPosition()
	{
		return position;
	}

	@Override
	public String toString()
	{
		return ( " Case : " + position + ", Color : " + color );
	}
	
	public static Move toMove(String s) {
		String place = "";
		char value = ' ';
		for(int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c != 'E') {
				if (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9') {
					place += c;
				}
				else {
					value = c;
				}
			}
		}
		int plc = Integer.parseInt(place);
		Move move = new Move(plc,value);
		return move;
	}
}
