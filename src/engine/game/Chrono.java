package engine.game;
import java.io.Serializable;

/**
 * @author Quentin Raymonadud
 */
public class Chrono implements Serializable
{
    /**
    * Time a start
    */
    protected long startT;
    /**
    * Time at the end
    */
    protected long endT;
    /**
    * Delta startT endT
    */
    protected long duration;   // in second

    /**
     * Construct a Chrono
     */
    public Chrono()
    {
        startT = 0;
        endT = 0;
        duration =0;
    }

    /**
     * Start a Chrono
     */
    public void start()
    {
        startT = System.currentTimeMillis();
        endT = 0;
    }

    /**
     * [getTour description]
     * Get the delta between the start and now in second
     * @return seconds Integer
     */
    public int getTour()
    {
        long end = System.currentTimeMillis();
        return Math.round( ( (end - startT) / 1_000)) ;
    }
    /**
     * set the value to endT and calculate the duration in second
     */
    public void stop()
    {
        endT = System.currentTimeMillis();
        duration = endT - startT;
    }

    /**
     * return duration ( in second )
     * @return second Integer
     */
    public int getDuration() // in second
    {
        return Math.round(duration / 1_000);
    }
}
