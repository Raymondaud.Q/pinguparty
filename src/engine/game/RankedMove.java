package engine.game;
public class RankedMove  extends Move
{
	double value;
	public RankedMove (int pos, char col,double est)
	{
		super(pos,col);
		value = est;
	}

	public double getValue()
	{
		return value;
	}

	public void setValue(double x)
	{
		value = x;
	}
	public void increaseValue(double x)
	{
		value += x;
	}


	@Override
	public String toString()
	{
		return ( " Case : " + position + ", Color : " + color + ", Estimation : " + value);
	}
}